<?xml version="1.0" encoding="UTF-8"?>
<wsdl:definitions name="TiersRib" targetNamespace="http://gfi.astre.webservices/gf/tiers/tiersrib" xmlns:impl="http://gfi.astre.webservices/gf/tiers/tiersrib" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:apachesoap="http://xml.apache.org/xml-soap" xmlns:intf="http://gfi.astre.webservices/gf/tiers/tiersrib" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:wsdlsoap="http://schemas.xmlsoap.org/wsdl/soap/">
  <wsdl:types>
    <schema elementFormDefault="qualified" targetNamespace="http://gfi.astre.webservices/gf/tiers/tiersrib" xmlns="http://www.w3.org/2001/XMLSchema">
				<!--  request et response -->	
			
	<complexType name="Authentification">
		<sequence>
			<element name="USERNOM" type="string"/>
			<element name="USERPWD" type="string"/>
		</sequence>
	</complexType>
	
	<complexType name="Contexte">
		<sequence>
			<element name="Organisme" type="string"/>
			<element name="Budget" type="string"/>
			<element name="Exercice" type="string"/>
		</sequence>
	</complexType>
	
	<complexType name="TiersRibRequest">
				<sequence>
			<element name="Authentification" type="impl:Authentification"/>
			<element name="Contexte" type="impl:Contexte"/>
		<element name="TiersRib" type="impl:TiersRib"/>
				</sequence>
			</complexType>
		<complexType name="TiersRibResponse">
			<sequence>		
				<element name="TiersRibReturn" type="impl:TiersRib"/>
			</sequence>
		</complexType>
		
		<complexType name="TiersRib">
		    <sequence>
         <element name="CodeTiers" nillable="true" type="xsd:string"/>
         <element name="IdRib" nillable="true" type="xsd:string"/>
         <element name="CodeDomiciliation" nillable="true" type="xsd:string"/>
         <element name="CodePaiement" nillable="true" type="xsd:string"/>
         <element name="LibelleCourt" nillable="true" type="xsd:string"/>
         <element name="IndicateurRibDefaut" nillable="true" type="xsd:string"/>
         <element name="CodeStatut" nillable="true" type="xsd:string"/>
         <element name="CodeBanque" nillable="true" type="xsd:string"/>
         <element name="CodeGuichet" nillable="true" type="xsd:string"/>
         <element name="Comptefrancais" nillable="true" type="xsd:string"/>
         <element name="CleRib" nillable="true" type="xsd:string"/>
         <element name="LibelleBanque" nillable="true" type="xsd:string"/>
         <element name="LibelleGuichet" nillable="true" type="xsd:string"/>
         <element name="CodeDevise" nillable="true" type="xsd:string"/>
         <element name="LibelleDevise" nillable="true" type="xsd:string"/>
         <element name="LibellePays" nillable="true" type="xsd:string"/>
         <element name="CodeIso2Pays" nillable="true" type="xsd:string"/>
         <element name="CompteEtranger" nillable="true" type="xsd:string"/>
         <element name="CodeBic" nillable="true" type="xsd:string"/>
         <element name="CleIban" nillable="true" type="xsd:string"/>
         <element name="NumeroIban" nillable="true" type="xsd:string"/>
         <element name="LibelleCompteEtranger" nillable="true" type="xsd:string"/>
         <element name="Libelle2CompteEtranger" nillable="true" type="xsd:string"/>
         <element name="NumeroTiersSubrogatoire" nillable="true" type="xsd:string"/>
         <element name="IdRibSubrogatoire" nillable="true" type="xsd:string"/>
         <element name="IntituleRibSub" nillable="true" type="xsd:string"/>
         <element name="IndicateurRibEtranger" nillable="true" type="xsd:string"/>
         <element name="TiersIdSubrogatoire" nillable="true" type="xsd:string"/>
         <element name="VilleTiersSub" nillable="true" type="xsd:string"/>
         <element name="NomTiersSub" nillable="true" type="xsd:string"/>
         <element name="NumRibSubrogatoire" nillable="true" type="xsd:string"/>
         <element name="CodePostalTiersSub" nillable="true" type="xsd:string"/>
         <element name="Commentaire" nillable="true" type="xsd:string"/>
</sequence>
	</complexType>
	
	<complexType name="TiersRib_Cle">
		<sequence>
			<element name="CodeTiers" nillable="true" type="xsd:string"/>
			<element name="IdRib" nillable="true" type="xsd:string"/>
		</sequence>
	</complexType>
	
	<complexType name="TiersRibRequest_Cles">
	   	<sequence>
			<element name="Authentification" type="impl:Authentification"/>
			<element name="Contexte" type="impl:Contexte"/>
		<element name="TiersRibCle" type="impl:TiersRib_Cle"/>
		
				</sequence>
			</complexType>
			
		<element name="creation">
			<complexType>
				<sequence>
					<element name="request" type="impl:TiersRibRequest"/>
				</sequence>
			</complexType>
		</element>
		
		<element name="creationResponse">
			<complexType>
				<sequence>
					<element name="response" type="impl:TiersRibResponse"/>
				</sequence>
			</complexType>
		</element>
		
		<element name="modification">
			<complexType>
				<sequence>
					<element name="request" type="impl:TiersRibRequest"/>
				</sequence>
			</complexType>
		</element>
		
		<element name="modificationResponse">
			<complexType>
				<sequence>
					<element name="response" type="impl:TiersRibResponse"/>
				</sequence>
			</complexType>
		</element>
		
		<element name="suppression">
			<complexType>
				<sequence>
					<element name="request" type="impl:TiersRibRequest_Cles"/>
				</sequence>
			</complexType>
		</element>
		
		<element name="suppressionResponse">
			<complexType>
				<sequence>
					<element name="response" type="impl:TiersRibResponse"/>
				</sequence>
			</complexType>
		</element>
		
		<element name="chargement">
			<complexType>
				<sequence>
					<element name="request" type="impl:TiersRibRequest_Cles"/>
				</sequence>
			</complexType>
		</element>
		
		<element name="chargementResponse">
			<complexType>
				<sequence>
					<element name="response" type="impl:TiersRibResponse"/>
				</sequence>
			</complexType>
		</element>
		
		
		</schema>
  </wsdl:types>
  <wsdl:message name="modificationRequest">
    <wsdl:part name="request" element="impl:modification">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="modificationResponse">
    <wsdl:part name="response" element="impl:modificationResponse">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="creationRequest">
    <wsdl:part name="request" element="impl:creation">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="chargementRequest">
    <wsdl:part name="request" element="impl:chargement">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="chargementResponse">
    <wsdl:part name="response" element="impl:chargementResponse">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="creationResponse">
    <wsdl:part name="response" element="impl:creationResponse">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="suppressionRequest">
    <wsdl:part name="request" element="impl:suppression">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="suppressionResponse">
    <wsdl:part name="response" element="impl:suppressionResponse">
    </wsdl:part>
  </wsdl:message>
  <wsdl:portType name="TiersRib">
<wsdl:documentation>Web service des TiersRib Astre </wsdl:documentation>
    <wsdl:operation name="Modification">
<wsdl:documentation>
				le champ encodeKey est obligatoire ainsi que le champ à modifier
			</wsdl:documentation>
      <wsdl:input name="modificationRequest" message="impl:modificationRequest">
    </wsdl:input>
      <wsdl:output name="modificationResponse" message="impl:modificationResponse">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="Chargement">
<wsdl:documentation>
				le champ encodeKey est obligatoire
			</wsdl:documentation>
      <wsdl:input name="chargementRequest" message="impl:chargementRequest">
    </wsdl:input>
      <wsdl:output name="chargementResponse" message="impl:chargementResponse">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="Creation">
<wsdl:documentation>
				tous les champs sont obligatoires
			</wsdl:documentation>
      <wsdl:input name="creationRequest" message="impl:creationRequest">
    </wsdl:input>
      <wsdl:output name="creationResponse" message="impl:creationResponse">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="Suppression">
<wsdl:documentation>
				tous les champs sont obligatoires
			</wsdl:documentation>
      <wsdl:input name="suppressionRequest" message="impl:suppressionRequest">
    </wsdl:input>
      <wsdl:output name="suppressionResponse" message="impl:suppressionResponse">
    </wsdl:output>
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="TiersRibSoapBinding" type="impl:TiersRib">
    <wsdlsoap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
    <wsdl:operation name="Modification">
      <wsdlsoap:operation soapAction=""/>
      <wsdl:input name="modificationRequest">
        <wsdlsoap:body use="literal"/>
      </wsdl:input>
      <wsdl:output name="modificationResponse">
        <wsdlsoap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="Suppression">
      <wsdlsoap:operation soapAction=""/>
      <wsdl:input name="suppressionRequest">
        <wsdlsoap:body use="literal"/>
      </wsdl:input>
      <wsdl:output name="suppressionResponse">
        <wsdlsoap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="Creation">
      <wsdlsoap:operation soapAction=""/>
      <wsdl:input name="creationRequest">
        <wsdlsoap:body use="literal"/>
      </wsdl:input>
      <wsdl:output name="creationResponse">
        <wsdlsoap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="Chargement">
      <wsdlsoap:operation soapAction=""/>
      <wsdl:input name="chargementRequest">
        <wsdlsoap:body use="literal"/>
      </wsdl:input>
      <wsdl:output name="chargementResponse">
        <wsdlsoap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="TiersRib">
    <wsdl:port name="TiersRib" binding="impl:TiersRibSoapBinding">
      <wsdlsoap:address location="http://172.25.0.200:8090/axis2/services/TiersRib/"/>
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>





https://github.com/riversun/xml-beautify

            <tier:Authentification>
               <tier:USERNOM>WBSGF</tier:USERNOM>
               <tier:USERPWD>WBSGF</tier:USERPWD>
            </tier:Authentification>
            <tier:Contexte>
               <tier:Organisme>DPT25</tier:Organisme>
               <tier:Budget>01</tier:Budget>
               <tier:Exercice>2021</tier:Exercice>

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:rec="http://gfi.astre.webservices/gf/recherchetiers">
   <soapenv:Header/>
   <soapenv:Body>
      <rec:request>
         <rec:Authentification>
            <rec:USERNOM>WBSGF</rec:USERNOM>
            <rec:USERPWD>WBSGF</rec:USERPWD>
         </rec:Authentification>
         <rec:Contexte>
            <rec:Organisme>DPT25</rec:Organisme>
            <rec:Budget>01</rec:Budget>
            <rec:Exercice>2021</rec:Exercice>
         </rec:Contexte>
         <rec:Criteres>
            <rec:NbEnreg></rec:NbEnreg>
            <rec:ListeResultat></rec:ListeResultat>
            <rec:casseCodeTiers></rec:casseCodeTiers>
            <rec:NbEnregPage></rec:NbEnregPage>
            <rec:isFinancier></rec:isFinancier>
            <rec:numeroTiers></rec:numeroTiers>
            <rec:nom></rec:nom>
            <rec:nomSigleRaison></rec:nomSigleRaison>
            <rec:nomContact></rec:nomContact>
            <rec:indicateurAssociation></rec:indicateurAssociation>
            <rec:codeFamille></rec:codeFamille>
            <rec:libelleFamille></rec:libelleFamille>
            <rec:siren>37809438700016</rec:siren>
            <rec:agence></rec:agence>
            <rec:codeApe></rec:codeApe>
            <rec:libelleCodeApe></rec:libelleCodeApe>
            <rec:banque></rec:banque>
            <rec:guichet></rec:guichet>
            <rec:numeroCompte></rec:numeroCompte>
            <rec:cleRIB></rec:cleRIB>
            <rec:AdrEmail></rec:AdrEmail>
            <rec:codePostal></rec:codePostal>
            <rec:ville></rec:ville>
            <rec:encodeKeyStatut></rec:encodeKeyStatut>
            <rec:typeTiers></rec:typeTiers>
            <rec:isPermanent></rec:isPermanent>
            <rec:ancienNumero></rec:ancienNumero>
            <rec:encodeKeyNomana></rec:encodeKeyNomana>
            <rec:codeElement></rec:codeElement>
            <rec:codeService></rec:codeService>
            <rec:encodeKeyIdCompl></rec:encodeKeyIdCompl>
            <rec:valeurIdCompl></rec:valeurIdCompl>
         </rec:Criteres>
      </rec:request>
   </soapenv:Body>
</soapenv:Envelope>


<?php

require_once 'AstreService.php';

// Recherche tiers par siret
$siret = '';
$config = (object) [
    'url' => '',
    'identifiant' => '',
    'password' => '',
    'organisme' => '',
    'budget' => '',
    'exercice' => '',
    'notification' => false,
];

$astre_service = new AstreService($config);
$tiers = $astre_service->getTiersBySiret($siret);

/*
 // On vérifie si on a bien une domiciliation sur le dossier 
        $this->isDomiciliationExist(); 

        // On récupere les domiciliations du tiers sur ASTRE
        $this->getDomiciliationsTiers();


        $domiciliation_astre_bloquee = [];
        $domiciliation_astre_non_bloquee = [];
        if ( count($domiciliation_astre_non_bloquee) > 0 ) {
            if ( $this->dossier->domiciliation === false ) {

                $this->updateTiersDomiciliation();
                if( count($domiciliation_astre_non_bloquee) === 1 ){
                    $this->updateDossierDomiciliation();
                } 

            } else {

                if ( $this->isDomiciliationExistInArray($domiciliation_astre_non_bloquee) ) {
                    $this->updateDomiciliation();
                    $this->updateAppairageDomiciliation();
                } else {

                }
                
                $exist = true;
                if ( $exist === true ) {
                    // updateAppairage();
                } else {
                    // Créer les domiciliations
                    // Récupération nouvelle domiciliation
                }
                if( count($domiciliation_astre_non_bloquee) === 1 ){
                    // On affecte la domiciliation au dossier
                } 
            }
        } 

        // Si le tiers dispose de domiciliation sur Astre
        $this->updateTiersDomiciliations();
        // On appaire les domiciliations du tiers
        $this->updateAppairageDomiciliations();
*/