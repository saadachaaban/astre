<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once 'modules/OPS_exercice/controller.php';
require_once 'custom/include/Ode/Helper/OdeArrayHelper.php';

class CustomOPS_exerciceController extends OPS_exerciceController
{


    /**
     * @access public
     * action_getExercicesName()
     * Fonction qui retoune le nom de l'exercice par id
     * 
     * @return json     $data
     */
    public function action_getExercicesName()
    {

        $libelle_erreur = "";
        $exercices = [];

        // Vérification des données recu en Ajax , on passe en parametre Request et la liste des champs à vérifier
        $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);
        if ($data_verified['statut'] == "ok") {
            // On récupére les id's des exercices séléctionnés
            $ids = $data_verified["data"]["json_array"]["ids"];
            if (is_array($ids) && count($ids) > 0) {
                foreach ($ids as $exercice_id) {
                    $obj_exercice = BeanFactory::getBean("OPS_exercice", $exercice_id);
                    if (!empty($obj_exercice->name)) {
                        $exercices[$exercice_id] = $obj_exercice->name;
                    }
                }
            }
        } else {
            $libelle_erreur = $data_verified['data'];
        }

        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => $exercices) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }
}
