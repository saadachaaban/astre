<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once 'modules/Users/controller.php';
require_once 'custom/include/Ode/Helper/OdeArrayHelper.php';

class CustomUsersController extends UsersController
{


    /**
     * @access public
     * action_getUsersInfos()
     * Fonction qui retoune le nom, prénom et email des utilisateurs par id
     * 
     * @return json     $data
     */
    public function action_getUsersInfos()
    {

        $libelle_erreur = "";
        $users = [];

        // Vérification des données recu en Ajax , on passe en parametre Request et la liste des champs à vérifier
        $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);
        if ($data_verified['statut'] == "ok") {
            // On récupére les id's des users séléctionnés
            $ids = $data_verified["data"]["json_array"]["ids"];
            if (is_array($ids) && count($ids) > 0) {
                foreach ($ids as $user_id) {
                    $obj_user = BeanFactory::getBean("Users", $user_id);
                    $user_name = (!empty($obj_user->id)) ? $obj_user->full_name : "";
                    if (!empty($user_name)) {
                        $user = [
                            'name' => $user_name,
                            'email' => (!empty($obj_user->email1)) ? $obj_user->email1 : ""
                        ];
                        $users[$user_id] = $user;
                    }
                }
            }
        } else {
            $libelle_erreur = $data_verified['data'];
        }

        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => $users) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }
}
