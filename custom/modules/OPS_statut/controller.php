<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once 'modules/OPS_statut/controller.php';
require_once 'custom/include/Ode/Helper/OdeArrayHelper.php';
require_once 'custom/include/Ode/Model/StatutModel.php';

class CustomOPS_statutController extends OPS_statutController
{


    /**
     * @access public
     * action_getStatutsInfos()
     * Fonction qui retoune le nom de l'statut par id
     * 
     * @return json     $data
     */
    public function action_getStatutsInfos()
    {

        $libelle_erreur = "";
        $statuts = [];

        // Vérification des données recu en Ajax , on passe en parametre Request et la liste des champs à vérifier
        $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);
        if ($data_verified['statut'] == "ok") {
            // On récupére les id's des statuts séléctionnés
            $ids = $data_verified["data"]["json_array"]["ids"];
            if (is_array($ids) && count($ids) > 0) {
                foreach ($ids as $statut_id) {
                    $obj_statut = BeanFactory::getBean("OPS_statut", $statut_id);
                    if (!empty($obj_statut->name)) {
                        $statuts[$statut_id] = [
                            'name' => $obj_statut->name,
                            'guide_name' =>  StatutModel::getGuideInstructionName($obj_statut->id),
                        ];
                    }
                }
            }
        } else {
            $libelle_erreur = $data_verified['data'];
        }

        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => $statuts) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }

    public function getGuideInstructionName($statut_id)
    {
        return "TEST";
    }
}
