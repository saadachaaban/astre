<?php

foreach ($admin_group_header as $index => $groupe) {
    if (is_array($groupe) && !empty($groupe[0]) && $groupe[0] === "LBL_ODE_COMMUNICATION") {
        if (is_array($groupe[3]["ODE_admin"])) {
            $admin_group_header[$index][3]["ODE_admin"]["04_connecteur_astre"] = [
                'Administration',
                'LBL_ASTRE',
                'LBL_ASTRE_PARAMS',
                './index.php?module=Administration&action=admin_astre'
            ];
        }
    }
}
