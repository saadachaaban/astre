Object.getLength = function(obj) {
    var size = 0,key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

if (!Object.prototype.forEach) {
    Object.defineProperty(Object.prototype, 'forEach', {
        value: function (callback, thisArg) {
            if (this == null) {
                throw new TypeError('Not an object');
            }
            thisArg = thisArg || window;
            for (var key in this) {
                if (this.hasOwnProperty(key)) {
                    callback.call(thisArg, this[key], key, this);
                }
            }
        }
    });
}

/********************** Polyfill EI11 *********************/
/**********************************************************/

if (!String.prototype.includes) {
    String.prototype.includes = function() {
        'use strict';
        return String.prototype.indexOf.apply(this, arguments) !== -1;
    };
}

/**********************************************************/
/**********************************************************/



$(document).ready(function() {

    ModeleMailHelper.init();
   
    HtmlHelper.getListe().init("user_list");
    ModulePopup.setClickOpenPopup("btn_add_user","Users",true,"callBackAddUser");
    ModulePopup.addCallBackFunction("callBackAddUser", AstreAdmin.callBackAddUser);

    HtmlHelper.getListe().init("exercice_list");
    ModulePopup.setClickOpenPopup("btn_add_exercice","OPS_exercice",true,"callBackAddExercice");
    ModulePopup.addCallBackFunction("callBackAddExercice", AstreAdmin.callBackAddExercice);

    HtmlHelper.getListe().init("dispositif_list");
    ModulePopup.setClickOpenPopup("btn_add_dispositif","OPS_dispositif",true,"callBackAddDispositif");
    ModulePopup.addCallBackFunction("callBackAddDispositif", AstreAdmin.callBackAddDispositif);

    HtmlHelper.getListe().init("statut_list");
    ModulePopup.setClickOpenPopup("btn_add_statut","OPS_statut",true,"callBackAddStatut");
    ModulePopup.addCallBackFunction("callBackAddStatut", AstreAdmin.callBackAddStatut);

});



var HtmlHelper = (function($) {

    const Liste = (function() {

        var liste_id = "";
        var liste_dom = false;
        var liste_dom_hidden = false;
        var liste_hidden = "";
        var liste = [];

        return {

            init: function(id) {
                liste_id = id;
                liste_dom = ( $(`#${liste_id}`).length > 0 || $(`#${liste_id}_hidden`).length > 0 ) ? $(`#${liste_id}`) : false;
                liste_dom_hidden = ( $(`#${liste_id}_hidden`).length > 0 ) ? $(`#${liste_id}_hidden`) : false;
                liste_hidden = ( liste_dom_hidden !== false ) ? liste_dom_hidden.val() : "";
                liste = (  liste_hidden !== "" && liste_hidden.split('|').length > 0 ) ?  liste_hidden.split('|'): [];
                if ( liste.length > 0 ) Liste.setClickDeleteElement();
            },

            addLigne: function(ligne_id='', ligne_libelle='', ligne_detail = '') {
                const ligne = Liste.newLigne(ligne_id,ligne_libelle,ligne_detail);
                if ( liste_dom !== false || ligne === false ){
                    if ( $.inArray( ligne.element_id, liste) === -1 ) {
                        var ligne_html = `<li class="list-group-item" id="${ligne.id}" >`;
                        ligne_html    +=     `<span class="element-info" > <strong>${ligne.libelle}</strong> ${ligne.detail} </span>`;
                        ligne_html    +=     `<span role="delete-${liste_id}-element" class="badge badge-danger" title="Retirer de la liste" data-id="${ligne.element_id}" data-list-id="${liste_id}" > Retirer </span>`;
                        ligne_html    += `</li>`;
                        if ( liste.length === 0 ) liste_dom.empty();
                        liste_dom.append(ligne_html);
                        liste.push(ligne.element_id);
                        liste_dom_hidden.val( liste.join('|') );
                        Liste.setClickDeleteElement();
                    }
                }
            },

            newLigne: function( id = '', libelle = '' , detail = '' ) { 
                if ( id !== '' ) {
                    return {
                        element_id: id,
                        id: `${liste_id}_${id}`,
                        libelle: ( libelle !== '' ) ? libelle : '',
                        detail: ( detail !== '' ) ? ` ( ${detail} )` : '',
                    };
                }
                return false;
            },

            setClickDeleteElement: function(){
                $(`[role="delete-${liste_id}-element"]`).each(function() {
                    $(this).off().on('click', function () {
                        const empty_html = '<li class="list-group-item"><span class="element-info" > Aucun élément </span></li>';
                        var element_id = $(this).attr("data-id");
                        var list_id = $(this).attr("data-list-id");
                        HtmlHelper.getListe().init(list_id);
                        liste = liste.filter(function(item) {
                            return item !== element_id
                        });
                        if ( liste.length === 0 ){
                            liste_dom.empty().append(empty_html);
                            liste_dom_hidden.val("");
                        } else {
                            $(`#${list_id}_${element_id}`).remove(); 
                            liste_dom_hidden.val(liste.join('|'));
                        }
                    });
                });
            },
        }
        
    })();
    return {
        getListe: function() {
            return Object.create(Liste);
        }
    }
})(jQuery);

var ModulePopup = (function($) {

    return {

        /**
         * Fonction qui 
         * 
         * @return {void} 
        */
        setClickOpenPopup: function( bouton_id, module_name, multi = false, callback ){
            var type = ( multi === true ) ? "MultiSelect" : "";
            if ( $(`#${bouton_id}`).length > 0 && module_name !== "" ){
                window['callBackModulePopup'] = ModulePopup.callback;
                $(`#${bouton_id}`).off().on('click', function () {
                    var json_data = {
                        "call_back_function": 'callBackModulePopup',
                        "form_name": "DetailView",
                        "field_to_name_array": {
                            "id": "subpanel_id"
                        },
                        "passthru_data": {
                            'callback': callback
                        }
                    }
                    open_popup(module_name, 600, 400, "", true, true, json_data, type, true);
                });
            }
        },
        
        addCallBackFunction: function( callback_name, callback_function ){ 
            window[callback_name] = callback_function;
        },

        callback: function( data ){ 
            var data_formated = {};
            do {
                if ( data.selection_list !== undefined && Object.getLength(data.selection_list) !== 0 ){
                    data_formated = data.selection_list;
                    break;
                } 

                if ( data.name_to_value_array !== undefined && data.name_to_value_array.subpanel_id !== "" ){
                    data_formated = { 0: data.name_to_value_array.subpanel_id};
                }
                
            } while (0);
            window[data.passthru_data.callback](data_formated);
        },
        
    }

})(jQuery);

var AstreAdmin = (function($) {

    return {

        /**
         * Fonction qui initialise le click sur le bouton "+" pour ajouter des utilisateurs 
         * 
         * @return {void} 
        */
        callBackAddUser: function(data){
            if ( Object.getLength(data) > 0 ){
                var loading = function(){};
                var callBack = function(){
                    if ( typeof this.result === "string" ){
                        console.log(`Erreur callBack Users::getUsersInfos : `, this.result);
                    } else {
                        for (const [user_id, user] of Object.entries(this.result)) {
                            var Liste = HtmlHelper.getListe();
                            Liste.init("user_list");
                            Liste.addLigne(user_id,user.name,user.email);
                        }
                    }
                };
                var query = OdeQueries.getAjaxActionQuery();
                query.setModule('Users');
                query.setAction("getUsersInfos");
                query.setPostData( { ids: data} );
                OdeAjax.getByAction( query, loading, callBack );
            } 
        },

        /**
         * Fonction qui initialise le click sur le bouton "+" pour ajouter des exercices 
         * 
         * @return {void} 
        */
        callBackAddExercice: function(data){
            if ( Object.getLength(data) > 0 ){
                var loading = function(){};
                var callBack = function(){
                    if ( typeof this.result === "string" ){
                        console.log(`Erreur callBack OPS_exercice::getExercicesName : `, this.result);
                    } else {
                        for (const [exercice_id, exercice_name] of Object.entries(this.result)) {
                            var Liste = HtmlHelper.getListe();
                            Liste.init("exercice_list");
                            Liste.addLigne(exercice_id,exercice_name,'');
                        }
                    }
                };
                var query = OdeQueries.getAjaxActionQuery();
                query.setModule('OPS_exercice');
                query.setAction("getExercicesName");
                query.setPostData( { ids: data} );
                OdeAjax.getByAction( query, loading, callBack );
            }   
        },

        /**
         * Fonction qui initialise le click sur le bouton "+" pour ajouter des dispositifs 
         * 
         * @return {void} 
        */
        callBackAddDispositif: function(data){
            if ( Object.getLength(data) > 0 ){
                var loading = function(){};
                var callBack = function(){
                    if ( typeof this.result === "string" ){
                        console.log(`Erreur callBack OPS_dispositif::getDispositifsName : `, this.result);
                    } else {
                        for (const [dispositif_id, dispositif_name] of Object.entries(this.result)) {
                            var Liste = HtmlHelper.getListe();
                            Liste.init("dispositif_list");
                            Liste.addLigne(dispositif_id,dispositif_name,'');
                        }
                    }
                };
                var query = OdeQueries.getAjaxActionQuery();
                query.setModule('OPS_dispositif');
                query.setAction("getDispositifsName");
                query.setPostData( { ids: data} );
                OdeAjax.getByAction( query, loading, callBack );
            }   
        },

        /**
         * Fonction qui initialise le click sur le bouton "+" pour ajouter des statuts 
         * 
         * @return {void} 
        */
        callBackAddStatut: function(data){
            if ( Object.getLength(data) > 0 ){
                var loading = function(){};
                var callBack = function(){
                    if ( typeof this.result === "string" ){
                        console.log(`Erreur callBack OPS_statut::getStatutsInfos : `, this.result);
                    } else {
                        for (const [statut_id, statut] of Object.entries(this.result)) {
                            var Liste = HtmlHelper.getListe();
                            Liste.init("statut_list");
                            Liste.addLigne(statut_id,statut.name,`Guide d'instruction : ${statut.guide_name}`);
                        }
                    }
                };
                var query = OdeQueries.getAjaxActionQuery();
                query.setModule('OPS_statut');
                query.setAction("getStatutsInfos");
                query.setPostData( { ids: data} );
                OdeAjax.getByAction( query, loading, callBack );
            }   
        },

    }

})(jQuery);

var ModeleMailHelper = (function($) {

    return {  

        /**
         * Fonction qui 
         * 
         * @param 
         * @return {void} 
        */
        createModele: function( fieldToSet ){ 
            fieldToSetValue = fieldToSet;
            URL="index.php?module=EmailTemplates&action=EditView&inboundEmail=true&show_js=1";
            windowName = 'email_template';
            windowFeatures = 'width=800' + ',height=600'    + ',resizable=1,scrollbars=1';
        
            win = window.open(URL, windowName, windowFeatures);
            if(window.focus)
            {
                // put the focus on the popup if the browser supports the focus() method
                win.focus();
            }
        },

        /**
         * Fonction qui 
         * 
         * @param 
         * @return {void} 
        */
        editModele: function( templateField ){ 
            fieldToSetValue = templateField;
            var field=document.getElementById(templateField);
            URL="index.php?module=EmailTemplates&action=EditView&inboundEmail=true&show_js=1";
            if (field.options[field.selectedIndex].value != 'undefined') {
                URL+="&record="+field.options[field.selectedIndex].value;
            }
            windowName = 'email_template';
            windowFeatures = 'width=800' + ',height=600'    + ',resizable=1,scrollbars=1';
        
            win = window.open(URL, windowName, windowFeatures);
            if(window.focus)
            {
                // put the focus on the popup if the browser supports the focus() method
                win.focus();
            }
        },

        /**
         * Fonction qui 
         * 
         * @param 
         * @return {void} 
        */
        returnPopupHandler: function( template_id, template_name ){ 

            var field=document.getElementById(fieldToSetValue);
            var bfound=0;
            for (var i=0; i < field.options.length; i++) {
                    if (field.options[i].value == template_id) {
                        if (field.options[i].selected==false) {
                            field.options[i].selected=true;
                        }
                        field.options[i].text = template_name;
                        bfound=1;
                    }
            }
            //add item to selection list.
            if (bfound == 0) {
                var newElement=document.createElement('option');
                newElement.text=template_name;
                newElement.value=template_id;
                field.options.add(newElement);
                newElement.selected=true;
            }
        
        
            var field1=document.getElementById('edit_modele_mail');
            field1.style.visibility="visible";
        
            var applyListToTemplateField = 'modele_mail';
            if (fieldToSetValue == 'edit_modele_mail') {
                    applyListToTemplateField = 'edit_modele_mail';
            } 
            var field=document.getElementById(applyListToTemplateField);
        
        
            if (bfound == 1) {
                for (var i=0; i < field.options.length; i++) {
                    if (field.options[i].value == template_id) {
                        field.options[i].text = template_name;
                    } 
                } 
        
            } else {
                var newElement=document.createElement('option');
                newElement.text=template_name;
                newElement.value=template_id;
                field.options.add(newElement);
            } 
        },

        /**
         * Fonction qui enitialise la création / modifition des modeles de mails
         * 
         * @param 
         * @return {void} 
        */
        init: function(){
            window["open_email_template_form"] = ModeleMailHelper.createModele;
            window["edit_email_template_form"] = ModeleMailHelper.editModele;
            window["refresh_email_template_list"] = ModeleMailHelper.returnPopupHandler;
        },
    }

})(jQuery);



