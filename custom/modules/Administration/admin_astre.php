<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

global $current_user, $sugar_config, $app_list_strings;

if (!is_admin($current_user)) sugar_die("Unauthorized access to administration.");

require_once('modules/Configurator/Configurator.php');
require_once 'custom/include/Ode/Model/StatutModel.php';

$configurator = new Configurator();
$sugar_smarty    = new Sugar_Smarty();
$errors        = array();

// Si process == true => On sauvegarde les données 
if (isset($_REQUEST['process']) && $_REQUEST['process'] == 'true') {

    if (isset($_REQUEST['url'])) {
        $configurator->config['opensub']['astre']['url'] = $_REQUEST['url'];
    }
    if (isset($_REQUEST['identifiant'])) {
        $configurator->config['opensub']['astre']['identifiant'] = $_REQUEST['identifiant'];
    }
    if (isset($_REQUEST['password'])) {
        $configurator->config['opensub']['astre']['password'] = $_REQUEST['password'];
    }
    if (isset($_REQUEST['organisme'])) {
        $configurator->config['opensub']['astre']['organisme'] = $_REQUEST['organisme'];
    }
    if (isset($_REQUEST['budget'])) {
        $configurator->config['opensub']['astre']['budget'] = $_REQUEST['budget'];
    }
    if (isset($_REQUEST['exercice'])) {
        $configurator->config['opensub']['astre']['exercice'] = $_REQUEST['exercice'];
    }

    $configurator->config['opensub']['astre']['actif'] = (!empty($_REQUEST['actif']) && $_REQUEST['actif'] == "on") ? "oui" : "non";

    if (isset($_REQUEST['modele_mail'])) {
        $configurator->config['opensub']['astre']['modele_mail'] = $_REQUEST['modele_mail'];
    }
    if (isset($_REQUEST['user_list_hidden'])) {
        $configurator->config['opensub']['astre']['users'] = $_REQUEST['user_list_hidden'];
    }
    if (isset($_REQUEST['exercice_list_hidden'])) {
        $configurator->config['opensub']['astre']['filtres']['exercice']['ids'] = $_REQUEST['exercice_list_hidden'];
    }
    if (isset($_REQUEST['exercice_filtre_type'])) {
        $configurator->config['opensub']['astre']['filtres']['exercice']['type'] = $_REQUEST['exercice_filtre_type'];
    }
    if (isset($_REQUEST['dispositif_list_hidden'])) {
        $configurator->config['opensub']['astre']['filtres']['dispositif']['ids'] = $_REQUEST['dispositif_list_hidden'];
    }
    if (isset($_REQUEST['dispositif_filtre_type'])) {
        $configurator->config['opensub']['astre']['filtres']['dispositif']['type'] = $_REQUEST['dispositif_filtre_type'];
    }
    if (isset($_REQUEST['statut_list_hidden'])) {
        $configurator->config['opensub']['astre']['filtres']['statut']['ids'] = $_REQUEST['statut_list_hidden'];
    }
    if (isset($_REQUEST['statut_filtre_type'])) {
        $configurator->config['opensub']['astre']['filtres']['statut']['type'] = $_REQUEST['statut_filtre_type'];
    }
    $configurator->handleOverride();
    header('Location: index.php?module=Administration&action=index');
}

// Initialisation de la page admin

// Paramètres globaux
$url = (!empty($configurator->config['opensub']['astre']['url'])) ? $configurator->config['opensub']['astre']['url'] : "";
$identifiant = (!empty($configurator->config['opensub']['astre']['identifiant'])) ? $configurator->config['opensub']['astre']['identifiant'] : "";
$password = (!empty($configurator->config['opensub']['astre']['password'])) ? $configurator->config['opensub']['astre']['password'] : "";
$organisme = (!empty($configurator->config['opensub']['astre']['organisme'])) ? $configurator->config['opensub']['astre']['organisme'] : "";
$budget = (!empty($configurator->config['opensub']['astre']['budget'])) ? $configurator->config['opensub']['astre']['budget'] : "";
$exercice = (!empty($configurator->config['opensub']['astre']['exercice'])) ? $configurator->config['opensub']['astre']['exercice'] : "";

$sugar_smarty->assign("url", $url);
$sugar_smarty->assign("identifiant", $identifiant);
$sugar_smarty->assign("password", $password);
$sugar_smarty->assign("organisme", $organisme);
$sugar_smarty->assign("budget", $budget);
$sugar_smarty->assign("exercice", $exercice);
// Parametres de notification
$email_templates = get_bean_select_array(true, 'EmailTemplate', 'name', '', 'name', true);
$modele_mail = (!empty($configurator->config['opensub']['astre']['modele_mail'])) ? $configurator->config['opensub']['astre']['modele_mail'] : "";

$modele_mail_options = get_select_options_with_id($email_templates, $modele_mail);
$actif = (!empty($configurator->config['opensub']['astre']['actif']) && $configurator->config['opensub']['astre']['actif'] === "oui") ? ' checked="checked" ' : "";
$user_list_hidden_value = (!empty($configurator->config['opensub']['astre']['users'])) ? $configurator->config['opensub']['astre']['users'] : "";
$users = getUsersInfos($user_list_hidden_value);
$sugar_smarty->assign("modele_mail_options", $modele_mail_options);
$sugar_smarty->assign("actif", $actif);
$sugar_smarty->assign("user_list_hidden_value", $user_list_hidden_value);
$sugar_smarty->assign("users", $users);

// Filtres tache planifiée
$filtre_types = [
    'aucun' => 'Désactivé',
    'uniquement' => 'Uniquement les éléments séléctionnés',
    'sauf' => 'En exceptant les éléments séléctionnés',
];

$exercice_list_hidden_value = (!empty($configurator->config['opensub']['astre']['filtres']['exercice']['ids'])) ? $configurator->config['opensub']['astre']['filtres']['exercice']['ids'] : "";
$exercices = getExercicesInfos($exercice_list_hidden_value);
$exercice_filtre_type = (!empty($configurator->config['opensub']['astre']['filtres']['exercice']['type'])) ? $configurator->config['opensub']['astre']['filtres']['exercice']['type'] : "";
$exercice_filtre_type_options = get_select_options_with_id($filtre_types, $exercice_filtre_type);

$dispositif_list_hidden_value = (!empty($configurator->config['opensub']['astre']['filtres']['dispositif']['ids'])) ? $configurator->config['opensub']['astre']['filtres']['dispositif']['ids'] : "";
$dispositifs = getdispositifsInfos($dispositif_list_hidden_value);
$dispositif_filtre_type = (!empty($configurator->config['opensub']['astre']['filtres']['dispositif']['type'])) ? $configurator->config['opensub']['astre']['filtres']['dispositif']['type'] : "";
$dispositif_filtre_type_options = get_select_options_with_id($filtre_types, $dispositif_filtre_type);

$statut_list_hidden_value = (!empty($configurator->config['opensub']['astre']['filtres']['statut']['ids'])) ? $configurator->config['opensub']['astre']['filtres']['statut']['ids'] : "";
$statuts = getstatutsInfos($statut_list_hidden_value);
$statut_filtre_type = (!empty($configurator->config['opensub']['astre']['filtres']['statut']['type'])) ? $configurator->config['opensub']['astre']['filtres']['statut']['type'] : "";
$statut_filtre_type_options = get_select_options_with_id($filtre_types, $statut_filtre_type);

$sugar_smarty->assign("exercice_list_hidden_value", $exercice_list_hidden_value);
$sugar_smarty->assign("exercices", $exercices);
$sugar_smarty->assign("exercice_filtre_type_options", $exercice_filtre_type_options);

$sugar_smarty->assign("dispositif_list_hidden_value", $dispositif_list_hidden_value);
$sugar_smarty->assign("dispositifs", $dispositifs);
$sugar_smarty->assign("dispositif_filtre_type_options", $dispositif_filtre_type_options);

$sugar_smarty->assign("statut_list_hidden_value", $statut_list_hidden_value);
$sugar_smarty->assign("statuts", $statuts);
$sugar_smarty->assign("statut_filtre_type_options", $statut_filtre_type_options);

// SuiteCRM variables
$sugar_smarty->assign('MOD', $mod_strings);
$sugar_smarty->assign('APP', $app_strings);
$sugar_smarty->assign('APP_LIST', $app_list_strings);
$sugar_smarty->assign('LANGUAGES', get_languages());
$sugar_smarty->assign("JAVASCRIPT", get_set_focus_js());
$sugar_smarty->assign('config', $sugar_config);
$sugar_smarty->assign('error', $errors);

$sugar_smarty->display('custom/modules/Administration/admin_astre.tpl');


function getUsersInfos($users_value)
{
    $users = [];
    if (!empty($users_value)) {
        $users_ids = explode("|", $users_value);
        if (is_array($users_ids) && count($users_ids) > 0) {
            foreach ($users_ids as $user_id) {
                $obj_user = BeanFactory::getBean("Users", $user_id);
                $user_name = (!empty($obj_user->id)) ? $obj_user->full_name : "";
                if (!empty($user_name)) {
                    $user = [
                        'name' => $user_name,
                        'email' => (!empty($obj_user->email1)) ? $obj_user->email1 : ""
                    ];
                    $users[$user_id] = $user;
                }
            }
        }
    }
    return $users;
}


function getExercicesInfos($exercices_value)
{
    $exercices = [];
    if (!empty($exercices_value)) {
        $exercices_ids = explode("|", $exercices_value);
        if (is_array($exercices_ids) && count($exercices_ids) > 0) {
            foreach ($exercices_ids as $exercice_id) {
                $obj_exercice = BeanFactory::getBean("OPS_exercice", $exercice_id);
                if (!empty($obj_exercice->name)) {
                    $exercices[$exercice_id] = $obj_exercice->name;
                }
            }
        }
    }
    return $exercices;
}

function getDispositifsInfos($dispositifs_value)
{
    $dispositifs = [];
    if (!empty($dispositifs_value)) {
        $dispositifs_ids = explode("|", $dispositifs_value);
        if (is_array($dispositifs_ids) && count($dispositifs_ids) > 0) {
            foreach ($dispositifs_ids as $dispositif_id) {
                $obj_dispositif = BeanFactory::getBean("OPS_dispositif", $dispositif_id);
                if (!empty($obj_dispositif->name)) {
                    $dispositifs[$dispositif_id] = $obj_dispositif->name;
                }
            }
        }
    }
    return $dispositifs;
}

function getStatutsInfos($statuts_value)
{
    $statuts = [];
    if (!empty($statuts_value)) {
        $statuts_ids = explode("|", $statuts_value);
        if (is_array($statuts_ids) && count($statuts_ids) > 0) {
            foreach ($statuts_ids as $statut_id) {
                $obj_statut = BeanFactory::getBean("OPS_statut", $statut_id);
                if (!empty($obj_statut->name)) {
                    $statuts[$statut_id] = [
                        'name' => $obj_statut->name,
                        'guide_name' =>  StatutModel::getGuideInstructionName($obj_statut->id),
                    ];
                }
            }
        }
    }
    return $statuts;
}
