{literal}
    <link href="custom/modules/Administration/css/admin_astre.css" rel="stylesheet" type="text/css">
{/literal}
 
<div class="moduleTitle">
    <h2>Configuration du Connecteur Astre</h2> 
    <div class="clear"></div>
</div>

<BR>

<form id="ConfigureSettings" name="ConfigureSettings" enctype='multipart/form-data' method="POST" action="index.php?module=Administration&action=admin_astre&process=true">

    <span class='error'>{$error.main}</span>

    <table width="100%" cellpadding="0" cellspacing="1" border="0" class="actionsContainer">
        <tr>
            <td>
                <input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" type="submit" name="save" onclick="return verify_data('ConfigureSettings');" value="{$APP.LBL_SAVE_BUTTON_LABEL}"> &nbsp;
                <input title="{$MOD.LBL_CANCEL_BUTTON_TITLE}" onclick="document.location.href='index.php?module=Administration&action=index'" class="button" type="button" name="cancel" value="  {$APP.LBL_CANCEL_BUTTON_LABEL}  "> 
            </td>
        </tr>
    </table>

    <br /> 

	<div class="admin-container" >

        <div class="row" style="background: white;margin: 0px;margin-bottom: 15px; padding-top: 10px">
            <h4> Paramètres globaux </h4>
            <div style="display: block;border-bottom: 2px solid #eee;margin-top: 10px;"></div>
        </div>

		<br>

        <div class="admin-params-container" >

            <div class="row">
                <div class="col-md-2">
                    <label>Url :</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="url" id="url" value="{$url}" />
                </div>
                <div class="col-md-2">Organisme :</div>
                <div class="col-md-4">
                    <input type="text" name="organisme" id="organisme" value="{$organisme}" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <label>Identifiant :</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="identifiant" id="identifiant" value="{$identifiant}" />
                </div>
                <div class="col-md-2">
                    <label>Mot de passe:</label>
                </div>
                <div class="col-md-4">
                    <input type="password" name="password" id="password" value="{$password}" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <label>Budget :</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="budget" id="budget" value="{$budget}" />
                </div>
                <div class="col-md-2">
                    <label>Exercice :</label>
                </div>
                <div class="col-md-4">
                    <input type="text" name="exercice" id="exercice" value="{$exercice}" />
                </div>
            </div>
            
        </div>

	    <br>

        <div class="row" style="background: white;margin: 0px;margin-bottom: 15px; padding-top: 10px">
            <h4> Astre HS : paramètres de notification</h4>
            <div style="display: block;border-bottom: 2px solid #eee;margin-top: 10px;"></div>
        </div>

        <div class="admin-params-container" >

            <div class="row">
                <div class="col-md-2">
                    <label>Modèle d'email :</label>
                </div>
                <div class="col-md-4">
                    <select id="modele_mail" name="modele_mail">
                        {$modele_mail_options}
                    </select>
                    <input type="button" class="button" onclick="javascript:open_email_template_form('modele_mail')" value="Créer" >
                    <input type="button" class="button" onclick="javascript:edit_email_template_form('modele_mail')" name="edit_modele_mail" id="edit_modele_mail" value="Editer">
                </div>
                <div class="col-md-2">
                    <label>Actif :</label>
                </div>
                <div class="col-md-4">
                    <input type="checkbox" id="actif" name="actif" {$actif}/>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <label>Utilisateurs :<span id="btn_add_user"class="badge badge-success add-element" title="Ajouter un utilisateur à la liste"> + </span> </label>
                </div>
                <div class="col-md-4" >
                    <ul id="user_list" class="list-group">
                        {if count($users) == 0}
                            <li class="list-group-item">
                                <span class="element-info" > Aucun élément </span>
                            </li>
                        {else}
                            {foreach from=$users key=user_id item=user}
                                <li class="list-group-item" id="user_list_{$user_id}" >
                                    <span class="element-info" > <strong>{$user.name}</strong> ( {$user.email} ) </span>
                                    <span role="delete-user_list-element" class="badge badge-danger" title="Retirer l'element de la liste" data-id="{$user_id}" data-list-id="user_list"> Retirer </span> 
                                </li>
                            {/foreach}
                        {/if}
                    </ul>
                    <input type="hidden" id="user_list_hidden" name="user_list_hidden" value="{$user_list_hidden_value}" />
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-4"></div>
            </div>

        </div>

        <div class="row" style="background: white;margin: 0px;margin-bottom: 15px; padding-top: 10px">
            <h4> Tâche planifiée : filtres </h4>
            <div style="display: block;border-bottom: 2px solid #eee;margin-top: 10px;"></div>
        </div>

		<br>

        <div class="admin-params-container" >

            <table class="table table-striped">
               <tbody>
               <tr>
                    <td style="width:15%;"> 
                        <label>Exercice <span id="btn_add_exercice"class="badge badge-success add-element" title="Ajouter un exercice à la liste"> + </span> </label>
                    </td>
                    <td style="width:35%;">
                        <select id="exercice_filtre_type" name="exercice_filtre_type" >
                            {$exercice_filtre_type_options}
                        </select>
                    </td>
                    <td style="width:50%;"> 
                        <ul id="exercice_list" >
                            {if count($exercices) == 0}
                                <li class="list-group-item">
                                    <span class="element-info" > Aucun élément </span>
                                </li>
                            {else}
                                {foreach from=$exercices key=exercice_id item=exercice_name}
                                    <li class="list-group-item" id="exercice_list_{$exercice_id}" >
                                        <span class="element-info" > <strong>{$exercice_name}</strong></span>
                                        <span role="delete-exercice_list-element" class="badge badge-danger" title="Retirer l'element de la liste" data-list-id="exercice_list" data-id="{$exercice_id}"> Retirer </span> 
                                    </li>
                                {/foreach}
                            {/if}
                        </ul>
                        <input type="hidden" id="exercice_list_hidden" name="exercice_list_hidden" value="{$exercice_list_hidden_value}" />
                    </td>
                </tr>
                <tr>
                    <td style="width:15%;"> 
                        <label>Dispositif <span id="btn_add_dispositif"class="badge badge-success add-element" title="Ajouter un dispositif à la liste"> + </span> </label>
                    </td>
                    <td style="width:35%;">
                        <select id="dispositif_filtre_type" name="dispositif_filtre_type" >
                            {$dispositif_filtre_type_options}
                        </select>
                    </td>
                    <td style="width:50%;"> 
                        <ul id="dispositif_list" >
                            {if count($dispositifs) == 0}
                                <li class="list-group-item">
                                    <span class="element-info" > Aucun élément </span>
                                </li>
                            {else}
                                {foreach from=$dispositifs key=dispositif_id item=dispositif_name}
                                    <li class="list-group-item" id="dispositif_list_{$dispositif_id}" >
                                        <span class="element-info" > <strong>{$dispositif_name}</strong> </span>
                                        <span role="delete-dispositif_list-element" class="badge badge-danger" title="Retirer l'element de la liste" data-id="{$dispositif_id}" data-list-id="dispositif_list"> Retirer </span> 
                                    </li>
                                {/foreach}
                            {/if}
                        </ul>
                        <input type="hidden" id="dispositif_list_hidden" name="dispositif_list_hidden" value="{$dispositif_list_hidden_value}" />
                    </td>
                </tr>
                <tr>
                    <td style="width:15%;"> 
                        <label>Statut <span id="btn_add_statut"class="badge badge-success add-element" title="Ajouter un statut à la liste"> + </span> </label>
                    </td>
                    <td style="width:35%;">
                        <select id="statut_filtre_type" name="statut_filtre_type" >
                            {$statut_filtre_type_options}
                        </select>
                    </td>
                    <td style="width:50%;"> 
                        <ul id="statut_list" >
                            {if count($statuts) == 0}
                                <li class="list-group-item">
                                    <span class="element-info" > Aucun élément </span>
                                </li>
                            {else}
                                {foreach from=$statuts key=statut_id item=statut}
                                    <li class="list-group-item" id="statut_list_{$statut_id}" >
                                        <span class="element-info" > <strong>{$statut.name}</strong> ( {$statut.guide_name} ) </span>
                                        <span role="delete-statut_list-element" class="badge badge-danger" title="Retirer l'element de la liste" data-id="{$statut_id}" data-list-id="statut_list"> Retirer </span> 
                                    </li>
                                {/foreach}
                            {/if}
                        </ul>
                        <input type="hidden" id="statut_list_hidden" name="statut_list_hidden" value="{$statut_list_hidden_value}" />
                    </td>
                </tr>
               </tbody>
            </table>

        </div>
	</div>

    <br />

    <div style="padding-top: 2px;">
        <input title="{$APP.LBL_SAVE_BUTTON_TITLE}" class="button primary" id="submit" type="submit" name="save" value="{$APP.LBL_SAVE_BUTTON_LABEL}" /> &nbsp;
        <input title="{$MOD.LBL_CANCEL_BUTTON_TITLE}" onclick="document.location.href='index.php?module=Administration&action=index'" class="button" type="button" name="cancel" value="  {$APP.LBL_CANCEL_BUTTON_LABEL}  " />
    </div>
    {$JAVASCRIPT}
</form>

{literal}
    <script type="text/javascript" charset="UTF-8" src="custom/include/Ode/Helper/OdeAjaxHelper.js"></script>
    <script type="text/javascript" charset="UTF-8" src="custom/include/Ode/Helper/OdeQueriesHelper.js"></script>
    <script type="text/javascript" charset="UTF-8" src="custom/include/Ode/Helper/OdeLoadingHelper.js"></script>
    <script type="text/javascript" charset="UTF-8" src="custom/modules/Administration/js/admin_astre.js"></script>
{/literal}
