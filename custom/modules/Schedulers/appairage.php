<?php

require_once 'custom/include/Astre/Config/AstreConfig.php';
require_once 'custom/include/Astre/Controller/AppairageController.php';
require_once 'custom/include/Astre/Entity/DossierEntity.php';

$job_strings[] = 'appairage';

function appairage()
{
    $GLOBALS['log']->fatal(" [ Tache planifiée ][Start] ------ Appairage des tiers et des domiciliation ------ ");

    $config = new AstreConfig();
    $appairageCtl = new AppairageController($config);

    $dossiers = getDossiers($config->filtres);
    foreach ($dossiers as $dossier_id) {
        $appairageCtl->appairer(new DossierEntity($dossier_id));
    }

    $GLOBALS['log']->fatal(" [ Tache planifiée ][End] ------  Appairage des tiers et des domiciliation ------ ");
    return true;
}

function getDossiers($filtres)
{
    global $db;
    $dossiers = [];
    $sql = "SELECT * FROM `ops_dossier` ";
    if ($filtres !== false) {
        if (!empty($filtres['dispositif'])) {

            $dispositif_filtre = ($filtres['dispositif']['type'] === 'sauf') ? " NOT IN " : " IN ";
            $dispositif_conditions = str_replace("|", "', '", $filtres['dispositif']['ids']);
            $dispositif_conditions .= ($filtres['dispositif']['type'] === 'sauf') ? "' ,'" : "";
            $sql .= " INNER JOIN `ops_dispositif_ops_dossier` ON `ops_dossier`.id = `ops_dispositif_ops_dossier`.ops_dossier_id
            WHERE `ops_dispositif_ops_dossier`.ops_dispositif_id " . $dispositif_filtre . " ( '" . $dispositif_conditions . "' ) ";
        }
        if (!empty($filtres['exercice'])) {
            $exercice_filtre = ($filtres['exercice']['type'] === 'sauf') ? " NOT IN " : " IN ";
            $exercice_conditions = str_replace("|", "', '", $filtres['exercice']['ids']);
            $exercice_conditions .= ($filtres['exercice']['type'] === 'sauf') ? "' ,'" : "";
            $sql .= (strpos($sql, 'WHERE') !== false) ? ' AND ' : ' WHERE ';
            $sql .= " `ops_dossier`.ops_exercice_id " . $exercice_filtre . " ( '" . $exercice_conditions . "' ) ";
        }
        if (!empty($filtres['statut'])) {
            $statut_filtre = ($filtres['statut']['type'] === 'sauf') ? " NOT IN " : " IN ";
            $statut_conditions = str_replace("|", "', '", $filtres['statut']['ids']);
            $statut_conditions .= ($filtres['statut']['type'] === 'sauf') ? "' ,'" : "";
            $sql .= (strpos($sql, 'WHERE') !== false) ? ' AND ' : ' WHERE ';
            $sql .= " `ops_dossier`.ops_statut_id " . $statut_filtre . " ( '" . $statut_conditions . "' ) ";
        }
        $sql .= " AND `ops_dossier`.deleted = '0' ";
    } else {
        $sql .= " WHERE `ops_dossier`.deleted = '0' ";
    }

    $result = $db->query($sql);
    while ($row = $db->fetchByAssoc($result)) {
        $dossiers[] = $row['id'];
    }
    return (is_array($dossiers) && count($dossiers) > 0) ? $dossiers : [];
}
