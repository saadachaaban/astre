<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once 'modules/OPS_dispositif/controller.php';
require_once 'custom/include/Ode/Helper/OdeArrayHelper.php';

class CustomOPS_dispositifController extends OPS_dispositifController
{


    /**
     * @access public
     * action_getDispositifsName()
     * Fonction qui retoune le nom de l'dispositif par id
     * 
     * @return json     $data
     */
    public function action_getDispositifsName()
    {

        $libelle_erreur = "";
        $dispositifs = [];

        // Vérification des données recu en Ajax , on passe en parametre Request et la liste des champs à vérifier
        $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);
        if ($data_verified['statut'] == "ok") {
            // On récupére les id's des dispositifs séléctionnés
            $ids = $data_verified["data"]["json_array"]["ids"];
            if (is_array($ids) && count($ids) > 0) {
                foreach ($ids as $dispositif_id) {
                    $obj_dispositif = BeanFactory::getBean("OPS_dispositif", $dispositif_id);
                    if (!empty($obj_dispositif->name)) {
                        $dispositifs[$dispositif_id] =  $obj_dispositif->name;
                    }
                }
            }
        } else {
            $libelle_erreur = $data_verified['data'];
        }

        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => $dispositifs) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }
}
