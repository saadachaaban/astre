<?php
$module_name = 'OPS_dossier';
$_module_name = 'ops_dossier';
$searchdefs[$module_name] =
  array(
    'layout' =>
    array(
      'basic_search' =>
      array(
        'name' =>
        array(
          'name' => 'name',
          'default' => true,
          'width' => '10%',
        ),
        'beneficiaire_individu' =>
        array(
          'type' => 'relate',
          'studio' => 'visible',
          'label' => 'LBL_BENEFICIAIRE_INDIVIDU',
          'id' => 'ops_individu_id',
          'link' => true,
          'width' => '10%',
          'default' => true,
          'name' => 'ops_individu_ops_dossier_name',
        ),
        'commissions' =>
        array(
          'type' => 'relate',
          'studio' => 'visible',
          'label' => 'LBL_COMMISSIONS',
          'id' => 'ops_commission_id',
          'link' => true,
          'width' => '10%',
          'default' => true,
          'name' => 'ops_commission_ops_dossier',
        ),
        'ops_statut_id' =>
        array(
          'type' => 'relate',
          'link' => true,
          'label' => 'LBL_STATUT',
          'id' => 'OPS_STATUT_ID',
          'width' => '10%',
          'default' => true,
          'name' => 'statut',
        ),
        'ops_etape_id' =>
        array(
          'type' => 'relate',
          'link' => true,
          'label' => 'LBL_ETAPE',
          'id' => 'OPS_ETAPE_ID',
          'width' => '10%',
          'default' => true,
          'name' => 'etape',
        ),
        'ops_dispositif_ops_dossier_name' =>
        array(
          'type' => 'relate',
          'link' => true,
          'label' => 'LBL_OPS_DISPOSITIF_OPS_DOSSIER_FROM_OPS_DISPOSITIF_TITLE',
          'id' => 'OPS_DISPOSITIF_OPS_DOSSIEROPS_DISPOSITIF_IDA',
          'width' => '10%',
          'default' => true,
          'name' => 'ops_dispositif_ops_dossier_name',
        ),
        'canal' =>
        array(
          'type' => 'enum',
          'studio' => 'visible',
          'label' => 'LBL_CANAL',
          'width' => '10%',
          'default' => true,
          'name' => 'canal',
        ),
        'current_user_only' =>
        array(
          'name' => 'current_user_only',
          'label' => 'LBL_CURRENT_USER_FILTER',
          'type' => 'bool',
          'default' => true,
          'width' => '10%',
        ),
      ),
      'advanced_search' =>
      array(
        'name' =>
        array(
          'name' => 'name',
          'default' => true,
          'width' => '10%',
        ),
        'ops_etape_id' =>
        array(
          'type' => 'relate',
          'link' => true,
          'label' => 'LBL_ETAPE',
          'id' => 'OPS_ETAPE_ID',
          'width' => '10%',
          'default' => true,
          'name' => 'etape',
        ),
        'ops_statut_id' =>
        array(
          'type' => 'relate',
          'link' => true,
          'label' => 'LBL_STATUT',
          'id' => 'OPS_STATUT_ID',
          'width' => '10%',
          'default' => true,
          'name' => 'statut',
        ),
        'canal' =>
        array(
          'type' => 'enum',
          'studio' => 'visible',
          'label' => 'LBL_CANAL',
          'width' => '10%',
          'default' => true,
          'name' => 'canal',
        ),
        'thematique' =>
        array(
          'type' => 'enum',
          'studio' => 'visible',
          'label' => 'LBL_THEMATIQUE',
          'width' => '10%',
          'default' => true,
          'name' => 'thematique',
        ),
        'beneficiaire_individu' =>
        array(
          'type' => 'relate',
          'studio' => 'visible',
          'label' => 'LBL_BENEFICIAIRE_INDIVIDU',
          'id' => 'ops_individu_id',
          'link' => true,
          'width' => '10%',
          'default' => true,
          'name' => 'ops_individu_ops_dossier_name',
        ),
        'assigned_user_id' =>
        array(
          'name' => 'assigned_user_id',
          'type' => 'enum',
          'label' => 'LBL_ASSIGNED_TO',
          'function' =>
          array(
            'name' => 'get_user_array',
            'params' =>
            array(
              0 => false,
            ),
          ),
          'default' => true,
          'width' => '10%',
        ),
      ),
    ),
    'templateMeta' =>
    array(
      'maxColumns' => '3',
      'maxColumnsBasic' => '4',
      'widths' =>
      array(
        'label' => '10',
        'field' => '30',
      ),
    ),
  );;
