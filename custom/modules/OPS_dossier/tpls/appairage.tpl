<link href="custom/include/libs/fontawesome/css/all.css" rel="stylesheet" type="text/css">
<link href="custom/modules/OPS_dossier/css/appairage.css" rel="stylesheet" type="text/css">


<div id="appairage_modal" class="appairage-modal" >

    <div id="ode_container" class="container ode-container" >

        <div class="row justify-content-md-center ode-container-header" >
            <div class="col-sm-2 ode-container-header-title" class=""> Open<span style="color: #002d62;">Sub</span> <i style="color: #000000;margin: 0px 5px;" class="fas fa-arrows-alt-h"></i> <span style="color: #002d62;">Astre</span></div>
            <div class="col-sm-9 ode-container-header-container"> Appairage des tiers et des domiciliations </div>
            <div class="col-sm-1" style="height: 100%;display: flex;align-items: center; float: right;" >  
                <i id="appairage_modal_close_btn" class="fas fa-times-circle ode-container-close"></i>
            </div>
        </div>

        <div class="row justify-content-md-center ode-container-body" >


            <div role="modal-form" data-name="appairage-loading" style="margin: 10%;">
                <div id="appairage_progress_bar_pourcentage" class="ode-loading-pourcentage" ></div>
                <div class="ode-loading-container">
                    <div class="ode-loading-bar" id="appairage_progress_bar" data-current='0' data-total='0' ></div>
                </div>
                <div id="appairage_progress_bar_message" class="ode-loading-message loading-dots" > Traitement en cours </div>
            </div>

            <div role="modal-form" data-name="appairage-erreur-technique" style="text-align: center;">
                <h3 style="text-align: center; font-size: 15px; margin-top: 6%;" > Erreur technique, veuillez contacter votre administrateur </h3>
                <p style="text-align: center;font-style: italic;" > Code sortie : 400 </p>
                <img src="custom/include/images/erreur_technique.png">
            </div>

            <div role="modal-form" data-name="appairage-etat" style="text-align: center;height:90%">

               <div class="row justify-content-md-center" style="margin:50px;height:80%;"  >

                    <div class="tbl-header">
                        <table id="appairage_etat_table_header" class="appairage-table-header" cellpadding="0" cellspacing="0" border="0">
                            <thead>
                                <tr>
                                    <th style="width:5%;"></th>
                                    <th style="width:30%;">Tiers</th>
                                    <th style="width:7%;">Appairage</th>
                                    <th style="width:6%;">Journal</th>
                                    <th style="width:31%;">Domiciliation</th>
                                    <th style="width:7%;">Appairage</th>
                                    <th style="width:7%;">Journal</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <div class="tbl-content">
                        <table id="appairage_etat_table_content" cellpadding="0" cellspacing="0" border="0" class="table table-striped appairage-table-content">
                            <tbody id="appairage_etat_ligne_container"></tbody>
                        </table>
                    </div>

                </div>

                <div class="row justify-content-md-center" style="height:10%">
                    <div class="col-sm-6" style="text-align: left; padding-left: 5%;font-size: 17px; color: #606060; font-weight: initial;font-family: monospace;">
                        <i style="color:#F08377;" class="fas fa-info-circle"></i> <span id="appairage_etat_info"></span>
                    </div>
                    <div class="col-sm-6" style="text-align: right; padding-right: 3%;">
                        <button id="appairer_dossiers_btn" type="button" class="btn button" title="Lancer l'appairage des dossiers">Appairer</button>
                    </div>
                </div>
            </div>


            <div role="modal-form" data-name="appairage-resultat" style="text-align: center;height:90%">

               <div class="row justify-content-md-center" style="margin:50px;height:80%;"  >

                    <div class="tbl-header">
                        <table id="appairage_resultat_table_header" class="appairage-table-header" cellpadding="0" cellspacing="0" border="0">
                            <thead>
                                <tr>
                                    <th style="width:5%;"></th>
                                    <th style="width:30%;">Tiers</th>
                                    <th style="width:7%;">Appairage</th>
                                    <th style="width:6%;">Journal</th>
                                    <th style="width:31%;">Domiciliation</th>
                                    <th style="width:7%;">Appairage</th>
                                    <th style="width:7%;">Journal</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <div class="tbl-content">
                        <table id="appairage_resultat_table_content" cellpadding="0" cellspacing="0" border="0" class="table table-striped appairage-table-content">
                            <tbody id="appairage_resultat_ligne_container"></tbody>
                        </table>
                    </div>

                </div>

                <div class="row justify-content-md-center" style="height:10%">
                    <div class="col-sm-6" style="text-align: left; padding-left: 5%;font-size: 17px; color: #606060; font-weight: initial;font-family: monospace;">
                        <i style="color:#F08377;" class="fas fa-info-circle"></i> <span id="appairage_resultat_info"></span>
                    </div>
                    <div class="col-sm-6" style="text-align: right; padding-right: 3%;">
                        <!--
                        <button id="appairage_telecharger_rapport_btn" type="button" class="btn button" title="Télécharger le rapport d'appairage des tiers et des domiciliations"><i class="fas fa-save"></i> Télécharger le rapport</button>
                        -->
                        <button id="appairage_terminer_btn" type="button" class="btn button" title="Fermer la fenetre d'appairage"> Terminer </button>
                    </div>
                </div>
            </div>

        </div> 
    </div>

</div>

<script type="text/javascript" charset="UTF-8" src="custom/include/Ode/Helper/OdeAjaxHelper.js"></script>
<script type="text/javascript" charset="UTF-8" src="custom/include/Ode/Helper/OdeQueriesHelper.js"></script>
<script type="text/javascript" charset="UTF-8" src="custom/include/Ode/Helper/OdeLoadingHelper.js"></script>
<script type="text/javascript" charset="UTF-8" src="custom/modules/OPS_dossier/js/appairage.js"></script>

