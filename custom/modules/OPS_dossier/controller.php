<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once 'modules/OPS_dossier/controller.php';
require_once 'custom/include/Ode/Helper/OdeArrayHelper.php';
require_once 'include/MassUpdate.php';
require_once 'custom/include/Astre/Entity/DossierEntity.php';
require_once 'custom/include/Astre/Config/AstreConfig.php';
require_once 'custom/include/Astre/Controller/AppairageController.php';

class CustomOPS_dossierController extends OPS_dossierController
{


    /**
     * @access public
     * action_getDossiersName()
     * Fonction qui retoune le nom de l'dossier par id
     * 
     * @return json     $data
     */
    public function action_getDossiersName()
    {

        $libelle_erreur = "";
        $dossiers = [];

        // Vérification des données recu en Ajax , on passe en parametre Request et la liste des champs à vérifier
        $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);
        if ($data_verified['statut'] == "ok") {
            // On récupére les id's des dossiers séléctionnés
            $ids = $data_verified["data"]["json_array"]["ids"];
            if (is_array($ids) && count($ids) > 0) {
                foreach ($ids as $dossier_id) {
                    $obj_dossier = BeanFactory::getBean("OPS_dossier", $dossier_id);
                    if (!empty($obj_dossier->name)) {
                        $dossiers[$dossier_id] = $obj_dossier->name;
                    }
                }
            }
        } else {
            $libelle_erreur = $data_verified['data'];
        }

        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => $dossiers) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }

    /**
     * @access public
     * action_getDossiers()
     * Fonction qui retourne les ids des dossiers séléctionnés
     * 
     * @return json     $data
     */
    public function action_getDossiers()
    {
        global $db;
        $libelle_erreur = '';
        $dossiers = [];

        do {

            $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);

            if ($data_verified['statut'] === "err") {
                $libelle_erreur = 'Aucun dossier n\'a été récupéré';
                break;
            }

            $data = $data_verified["data"]["json_array"];

            if (!empty($data['dossiers']) && is_array($data['dossiers']) && count($data['dossiers']) > 0) {
                $dossiers = $data['dossiers'];
                break;
            }

            if (!empty($data['filtres'])) {
                // Ce traitement est utilisé dans le cas ou l'utilisateur sélectionne l'intégralité des dossiers + filtres
                $mass = new MassUpdate();
                $mass->generateSearchWhere('OPS_dossier', $data['filtres']);
                $where_clause = $mass->where_clauses;
                $obj_dossier = new OPS_dossier();
                $query = $obj_dossier->create_export_query("", $where_clause);
                $result = $db->query($query);
                while ($dossier = $db->fetchByAssoc($result)) {
                    $dossiers[] = $dossier['id'];
                }
            }

            // Je ne vérifie pas si $request['selectAll'] est non vide, empty($request['selectAll']) retourne true quand $request['selectAll']=0
            if (!is_array($dossiers) || count($dossiers) === 0) {
                $libelle_erreur = 'Aucun dossier n\'a été récupéré';
                break;
            }
        } while (0);

        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => $dossiers) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }

    /**
     * @access public
     * action_getDossiersName()
     * Fonction qui retoune le nom de l'dossier par id
     * 
     * @return json     $data
     */
    public function action_getAppairageDossier()
    {
        $dossier = [];
        $libelle_erreur = "";
        do {

            $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);

            if ($data_verified['statut'] === "err") {
                $libelle_erreur = 'Aucun dossier n\'a été récupéré';
                break;
            }

            $dossier_id = $data_verified["data"]["json_array"];
            if (!empty($dossier_id)) {
                $dossierEntity = new DossierEntity($dossier_id);
                $dossier = $dossierEntity->getOutput();
            }
        } while (0);

        //sleep(2);
        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => $dossier) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }

    /**
     * @access public
     * action_getDossiersName()
     * Fonction qui retoune le nom de l'dossier par id
     * 
     * @return json     $data
     */
    public function action_appairerDossier()
    {
        $dossier = [];
        $libelle_erreur = "";
        do {

            $data_verified = OdeArrayHelper::isDataValid($_REQUEST, ["json" => true]);

            if ($data_verified['statut'] === "err") {
                $libelle_erreur = 'Aucun dossier n\'a été récupéré';
                break;
            }

            $dossier_id = $data_verified["data"]["json_array"];
            if (empty($dossier_id)) {
                $libelle_erreur = 'Aucun dossier n\'a été récupéré';
                break;
            }

            $dossierEntity = new DossierEntity($dossier_id);
            $appairageCtrl = new AppairageController(new AstreConfig());
            $dossier = $appairageCtrl->appairer($dossierEntity);
        } while (0);

        //sleep(2);
        // Si on a aucune erreur le statut est à 'ok' et on retourne les données formatées sinon le statut passe à 'err' et on retourne le libelle de l'erreur 
        $data = (empty($libelle_erreur)) ? array('statut' => 'ok', 'data' => $dossier) : array('statut' => 'err', 'data' => $libelle_erreur);

        ob_clean();
        echo json_encode($data);
        sugar_cleanup(true);
    }
}
