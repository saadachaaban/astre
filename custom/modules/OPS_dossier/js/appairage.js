

$(document).ready(function() {
    Appairage.setClickOpenModal();
    Appairage.setClickCloseModal();
    Appairage.setClickAppairerDossier();
    Appairage.setClickTerminerModal();
    $(window).on("load resize", function() {
        var scrollWidth = $('.tbl-content').width() - $('.tbl-content table').width();
        $('.tbl-header').css({'padding-right':scrollWidth});
      }).resize();
});
 


var OdeLoading = (function($) {

    const ProgressBar = (function() {

        const message_defaut = 'Traitement en cours'; 

        var bar = false;
        var bar_message = false;
        var bar_pourcentage = false;
        var current = 0;
        var total = 0;
        var pourcentage = 0;
        var message = '';

        return {

            init: function(bar_id) {
                bar = ( $(`#${bar_id}`).length > 0 ) ? $(`#${bar_id}`) : false;
                bar_message = ( $(`#${bar_id}_message`).length > 0 ) ? $(`#${bar_id}_message`) : false;
                bar_pourcentage = ( $(`#${bar_id}_pourcentage`).length > 0 ) ? $(`#${bar_id}_pourcentage`) : false;
                if ( bar !== false ){
                    current = parseInt(bar.attr('data-current'));
                    total = parseInt(bar.attr('data-total'));
                }
            },

            reset: function() {
                ProgressBar.setTotal(0);
                bar.css('transition-duration','0s');
                ProgressBar.setCurrent(0);
                bar.css('transition-duration','1s');
                ProgressBar.setMessage(message_defaut);
            },

            getPourcentage: function() {
                if ( total === 0 ) return 0;
                if ( total === current || current > total) return 100;
                return Math.floor( ( current * 100 ) / total );
            },

            increaseCurrent: function() { 
                var new_current = parseInt(current) + 1;            
                ProgressBar.setCurrent( new_current ); 
            },

            getCurrent: function() { return current; },
            getTotal: function() { return total; },
            setTotal: function( _total = 0 ) {
                total = _total;
                if( bar !== false ) {
                    bar.attr('data-total', total);
                }
            },

            setCurrent: function( _current = 0 ) {
                current = _current;
                if( bar !== false ) {
                    bar.attr('data-current', current);
                }
                ProgressBar.setPourcentage();
            },

            setPourcentage: function() {
                pourcentage = ProgressBar.getPourcentage();
                if ( bar_pourcentage !== false ){
                    bar_pourcentage.html(`${pourcentage}%`);
                    bar.css("width",`${pourcentage}%`);
                }
            },

            setMessage: function( _message = '' ) {
                message =  ( _message !== '') ? _message : message_defaut;
                if( bar_message !== false ) {
                    // console.log('setMessage message=',message);
                    bar_message.html(message);
                }
            },
        }
        
    })();

    return {
        getProgressBar: function( bar_id ) {
            const Pb = Object.create(ProgressBar);
            Pb.init(bar_id);
            return Pb;
        }
    }

})(jQuery);

var Appairage = (function($) {

    return {

        /**
         * Fonction qui 
         * 
         * @return {void} 
        */
        setClickOpenModal: function(){
            $('[role="open-modal-appairage"]').each(function() {
                $(this).off().on('click', function () {
                    Appairage.openModal();
                });
            });
        },    

        /**
        * Fonction qui 
        * 
        * @return {void} 
        */
        setClickTerminerModal: function(){
            $('#appairage_terminer_btn').off().on('click', function () {
                $("#appairage_modal").dialog("close");
                var ProgressBar = OdeLoading.getProgressBar('appairage_progress_bar');
                ProgressBar.reset();
            });
        },  

        /**
        * Fonction qui 
        * 
        * @return {void} 
        */
        setClickAppairerDossier: function(){
            $('#appairer_dossiers_btn').off().on('click', function () {
                var dossiers = Appairage.getDossiers();
                console.log("setClickAppairerDossier dossiers = ", dossiers);
                if ( dossiers.length > 0 ){
                    var ProgressBar = OdeLoading.getProgressBar('appairage_progress_bar');
                    ProgressBar.reset();
                    ProgressBar.setTotal(dossiers.length);
                    Appairage.route('appairage-loading');
                    $('#appairage_resultat_ligne_container').empty();
                    dossiers.forEach((dossier_id, index) => {
                        console.log("Appairage appairerDossier dossier_id= ", dossier_id);
                        Appairage.appairerDossier(dossier_id);
                    });
                } else {
                    Appairage.route('appairage-erreur-technique');
                }
            });
        },  

        /**
         * Fonction qui 
         * 
         * @return {void} 
        */
        getDossiers: function(){
            var dossiers = [];
            const lignes = $('#appairage_etat_table_content').find('tr');
            if ( lignes.length > 0 ){
                lignes.each(function() {
                    if ( $(this).attr('data-id') !== undefined && $(this).attr('data-id') !== "" && $(this).attr('data-tiers-type') !== undefined && $(this).attr('data-tiers-type') === 'OPS_personne_morale' ){
                        dossiers.push($(this).attr('data-id')); 
                    }
                });
            };
            return dossiers;
        }, 
        
        /**
         * Fonction qui 
         * 
         * @return {void} 
        */
        setClickTelechargerRapport: function(){
            $('#appairage_telecharger_rapport_btn').off().on('click', function () {
                Appairage.route('appairage-erreur-technique');
            });
        }, 

        /**
         * Fonction qui ouvert une modale
         * 
         * @return {void} 
        */
        openModal: function( ){
            var modal_width = window.innerWidth / 1.5;
            var modal_height = window.innerHeight / 1.4;
            $( "#appairage_modal" ).dialog({
                resizable: false,
                height: modal_height,
                width: modal_width,
                modal: true,
                dialogClass: "no-close",
                draggable: false,
                autoOpen: false,
                show: { effect: "fade", duration: 400 },
                hide: { effect: "fade", duration: 400 },
                buttons: []
            });
            $( "#appairage_modal" ).dialog( "open" );
            $(".ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix").css("display","none");
            $(".ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix").css("display","none");
            Appairage.initModal();
        },

        /**
         * Fonction qui 
         * 
         * @return {void} 
        */
        setClickCloseModal: function() {
            $("#appairage_modal_close_btn").off().on('click', function () {
                $("#appairage_modal").dialog("close");
                var ProgressBar = OdeLoading.getProgressBar('appairage_progress_bar');
                ProgressBar.reset();
            });
        },

        /**
         * Fonction qui 
         * 
         * @param {string} xxx xxx 
         * @return {void} 
        */
        initModal: function() {

            var ProgressBar = OdeLoading.getProgressBar('appairage_progress_bar');
            ProgressBar.reset();

            Appairage.route('appairage-loading');

            var loading = function(){
                var ProgressBar = OdeLoading.getProgressBar('appairage_progress_bar');
                ProgressBar.setMessage('Récupération des dossiers séléctionnés');
            };

            var callBack = function(){
                if ( typeof this.result === "string" ){
                    console.log(`Erreur callBack OPS_dossier::getDossiers : `, this.result);
                } else {
                    var ProgressBar = OdeLoading.getProgressBar('appairage_progress_bar');
                    var message = ( this.result.length === 1 ) 
                                ? `Récupération de l'appairage du dossier séléctionné`
                                : `Récupération des appairages des ${this.result.length} dossiers séléctionnés`;
                    ProgressBar.setTotal(this.result.length);
                    ProgressBar.setMessage(message);
                    $('#appairage_etat_ligne_container').empty();
                    for (const [key, dossier_id] of Object.entries(this.result)) {
                        Appairage.getAppairageDossier(dossier_id);
                    }
                }
            };

            var dossiers = Appairage.getSelectedDossiers();
            var query = OdeQueries.getAjaxActionQuery();
            query.setModule('OPS_dossier');
            query.setAction("getDossiers");
            query.setPostData( dossiers );
            OdeAjax.getByAction( query, loading, callBack );
        },

        /**
         * Fonction qui 
         * 
         * @param {string} xxx xxx 
         * @return {void} 
        */
        route: function( route_name ) {
            var forms = [];
            $('[role="modal-form"]').each( function() {
                forms[$(this).attr("data-name")] = $(this);
            });
            route_name = ( typeof route_name === "string" && route_name !== "" && forms.hasOwnProperty(route_name) ) ? route_name : 'appairage-erreur-technique';
            for (const [form_name, form] of Object.entries(forms)) {
                ( form_name === route_name ) ? $(form).show() : $(form).hide();
            }  
        },

        /**
         * Fonction qui 
         * 
         * @param {string} xxx xxx 
         * @return {void} 
        */
        getAppairageDossier: function( dossier_id ) {

            var loading = function(){};
            var callBack = async function(){
                if ( typeof this.result === "string" ){
                    console.log(`Erreur callBack OPS_dossier::getDossiers : `, this.result);
                } else {
                    var ProgressBar = OdeLoading.getProgressBar('appairage_progress_bar');
                    ProgressBar.increaseCurrent();
                    if ( this.result.statut === true ) {
                        ProgressBar.setMessage(`Récupération de l'appairage du dossier n°${this.result.dossier.num} (${ProgressBar.getCurrent()}/${ProgressBar.getTotal()})`);
                        var ligne_html = AppairageHTML.getLigne(this.result.dossier, this.result.tiers, this.result.domiciliation);
                        $('#appairage_etat_ligne_container').append(ligne_html);
                    }
                    if ( ProgressBar.getCurrent() === ProgressBar.getTotal() ){
                        Appairage.setEtatInfo('etat');
                        await Appairage.sleep(1500);
                        Appairage.route('appairage-etat');
                    }
                }
            };
            var query = OdeQueries.getAjaxActionQuery();
            query.setModule('OPS_dossier');
            query.setAction("getAppairageDossier");
            query.setPostData(dossier_id);
            OdeAjax.getByAction( query, loading, callBack );
 
        },

        /**
         * Fonction qui 
         * 
         * @param {string} xxx xxx 
         * @return {void} 
        */
        appairerDossier: function( dossier_id ) {

            var loading = function(){};
            var callBack = async function(){
                if ( typeof this.result === "string" ){
                    console.log(`Erreur callBack OPS_dossier::appairerDossier : `, this.result);
                } else {

                    var ProgressBar = OdeLoading.getProgressBar('appairage_progress_bar');
                    ProgressBar.increaseCurrent();
                    if ( this.result.statut === true ) {
                        ProgressBar.setMessage(`Appairage du dossier n°${this.result.dossier.num} (${ProgressBar.getCurrent()}/${ProgressBar.getTotal()})`);
                        console.log( "appairerDossier this.result=",this.result)
                        var ligne_html = AppairageHTML.getLigne(this.result.dossier, this.result.tiers, this.result.domiciliation);
                        $('#appairage_resultat_ligne_container').append(ligne_html);
                    }

                    if ( ProgressBar.getCurrent() === ProgressBar.getTotal() ){
                        Appairage.setEtatInfo('resultat');
                        await Appairage.sleep(1500);
                        Appairage.route('appairage-resultat');
                    }
                }
            };
            var query = OdeQueries.getAjaxActionQuery();
            query.setModule('OPS_dossier');
            query.setAction("appairerDossier");
            query.setPostData(dossier_id);
            OdeAjax.getByAction( query, loading, callBack );

        },

        setEtatInfo: function(type) {  
            const lignes = $(`#appairage_${type}_table_content`).find('tr');
            if ( lignes.length > 0 ){
                var info_html = '';
                var count = { valid:0, invalid:0 };
                lignes.each(function() {
                    if ( $(this).attr('data-statut') === 'true' ){
                        count.valid++;
                    } else{
                        count.invalid++;
                    }
                });

                if ( count.valid === 0 ) {
                    info_html += `Aucun dossier appairé`;
                } else if ( count.valid === 1) {
                    info_html += `Un seul dossier appairé`;
                } else {
                    info_html += `${count.valid} dossiers appairés`;
                }

                info_html += ` et `;

                if ( count.invalid === 0 ) {
                    info_html += `aucun dossier non appairé`;
                } else if ( count.invalid === 1) {
                    info_html += `un seul dossier non appairé`;
                } else {
                    info_html += `${count.invalid} dossiers non appairés`;
                }

                $(`#appairage_${type}_info`).html(info_html);
            }else{
                $(`#appairage_${type}_info`).html('Echec de la vérification des dossiers');
            }
        },

        sleep: function(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        },

        /**
         * Fonction qui 
         * 
         * @param {string} xxx xxx 
         * @return {void} 
        */
        getSelectedDossiers: function() {
            var tabId = new Array();
            for (var wp = 0; wp < document.MassUpdate.elements.length; wp++) {
                if (document.MassUpdate.elements[wp].name == 'current_query_by_page') {
                    json = document.MassUpdate.elements[wp].value;
                }
                var reg_for_existing_uid = new RegExp('^' + RegExp.escape(document.MassUpdate.elements[wp].value) + '[\s]*,|,[\s]*' + RegExp.escape(document.MassUpdate.elements[wp].value) + '[\s]*,|,[\s]*' + RegExp.escape(document.MassUpdate.elements[wp].value) + '$|^' + RegExp.escape(document.MassUpdate.elements[wp].value) + '$');
                if (typeof document.MassUpdate.elements[wp].name != 'undefined' &&
                    document.MassUpdate.elements[wp].name == 'mass[]' &&
                    document.MassUpdate.elements[wp].checked &&
                    !reg_for_existing_uid.test(document.MassUpdate.uid.value)) {
                    tabId.push(document.MassUpdate.elements[wp].value);
                }
            }
            return ( document.MassUpdate.select_entire_list.value == 1 ) ? { dossiers: false, filtres: json } : { dossiers: tabId, filtres: false };
        }

    }

})(jQuery);


var AppairageHTML = (function($) {
  
    return {

        getLigne: function( dossier, tiers, domiciliation ){
            console.log('AppairageHTML::getLigne => dossier', dossier);
            console.log('AppairageHTML::getLigne => tiers', tiers);
            console.log('AppairageHTML::getLigne => domiciliation', domiciliation);
            var ligne = `<tr data-id="${dossier.id}" data-statut="${dossier.statut_appairage}" data-tiers-type="${tiers.type}" >`;
            var appairable = ( tiers.type === 'OPS_personne_morale' ) ? true : false;
            if ( dossier.statut === true ){
                ligne    += AppairageHTML.getDossierNum(dossier, appairable);
                ligne    += AppairageHTML.getTiersName(tiers);
                ligne    += AppairageHTML.getTiersAppairage(tiers);
                ligne    += AppairageHTML.getTiersAppairageJournal(tiers);
                ligne    += AppairageHTML.getDomiciliationName(domiciliation);
                ligne    += AppairageHTML.getDomiciliationAppairage(domiciliation);
                ligne    += AppairageHTML.getDomiciliationAppairageJournal(domiciliation);
            }else{
                ligne    += AppairageHTML.getDossierErreur(dossier);
            }
            ligne    += `</tr>`;
            return ligne;
        },

        getDossierNum: function(dossier, appairable){ 
            var title = ( typeof dossier.name === 'string' && dossier.name !== '') ? `title="${dossier.name}"` : ''; 
            var class_name = ( dossier.statut_appairage === true ) ? `class="ode-td-success"` : `class="ode-td-erreur"`;
            if ( appairable === false ) {
                var class_name = `class="ode-td-warning"`;
            }
            var dossier_num_html =     `<td ${title} ${class_name} style="width:5%;text-align: center;">`;
            dossier_num_html    +=         `<a target="_blank" href="/index.php?module=OPS_dossier&action=DetailView&record=${dossier.id}"> ${dossier.num} </a>`;
            dossier_num_html    +=     `</td>`;
            return dossier_num_html;
        },

        getTiersName: function(tiers){ 
            var tiers_name_html =     `<td style="width:35%;">`;
            if ( tiers.statut === false ) {
                tiers_name_html    +=         `<span> <i class="fas fa-exclamation-triangle"></i> Aucun demandeur ou bénéficiaire </span>`;
            } else {
                var tag_individu = ( tiers.type === 'OPS_individu' ) ? '<i class="fas fa-user"></i>' : '<i class="fas fa-users"></i>'; 
                tiers_name_html    +=         `<a target="_blank" style="color: #003a6c;" href="/index.php?module=${tiers.type}&action=DetailView&record=${tiers.id}"> ${tag_individu} ${tiers.name} </a>`;
            }
            tiers_name_html    +=     `</td>`;
            return tiers_name_html;
        },

        getTiersAppairage: function(tiers){ 
            if (  tiers.statut === false || tiers.appairage.statut === false || tiers.appairage.statut === 'err' ) {
                return `<td class="ode-td-empty" style="width:7%;"> ø </td>`;
            }
            var tiers_appairage_html =     `<td style="width:7%;">`;
            tiers_appairage_html    +=         `<a target="_blank" style="color: #003a6c;" href="/index.php?module=OPS_appairage&action=DetailView&record=${tiers.appairage.id}"> ${tiers.appairage.name} </a>`;
            tiers_appairage_html    +=     `</td>`;
            return tiers_appairage_html;
        },

        getTiersAppairageJournal: function(tiers){ 
            if (  tiers.statut === false || tiers.appairage.journal.id === undefined ) {
                return `<td class="ode-td-empty" style="width:5%;"> ø </td>`;
            }
            var tiers_appairage_html =     `<td style="width:5%;">`;
            tiers_appairage_html    +=         `<a target="_blank" href="/index.php?module=OPS_journal&action=DetailView&record=${tiers.appairage.journal.id}">`;
            tiers_appairage_html    +=             `<img style="width: 17px;" src="custom/include/images/journal.png">`;
            tiers_appairage_html    +=         `</a>`;
            tiers_appairage_html    +=     `</td>`;
            return tiers_appairage_html;
        },

        getDomiciliationName: function(domiciliation){ 
            var domiciliation_name_html =     `<td style="width:35%;">`;
            if ( domiciliation.statut === false ) {
                domiciliation_name_html    +=         `<span> <i class="fas fa-exclamation-triangle"></i> Aucune domiciliation séléctionnée </span>`;
            } else {
                domiciliation_name_html    +=         `<a target="_blank" style="color: #003a6c;" href="/index.php?module=OPS_domiciliation&action=DetailView&record=${domiciliation.id}"> ${domiciliation.name} </a>`;
            }
            domiciliation_name_html    +=     `</td>`;
            return domiciliation_name_html;
        },

        getDomiciliationAppairage: function(domiciliation){ 
            if (  domiciliation.statut === false || domiciliation.appairage.statut === false || domiciliation.appairage.statut === 'err' ) {
                return `<td class="ode-td-empty" style="width:7%;"> ø </td>`;
            }
            var domiciliation_appairage_html =     `<td style="width:7%;">`;
            domiciliation_appairage_html    +=         `<a target="_blank" style="color: #003a6c;" href="/index.php?module=OPS_appairage&action=DetailView&record=${domiciliation.appairage.id}"> ${domiciliation.appairage.name} </a>`;
            domiciliation_appairage_html    +=     `</td>`;
            return domiciliation_appairage_html;
        },

        getDomiciliationAppairageJournal: function(domiciliation){ 
            if (  domiciliation.statut === false || domiciliation.appairage.journal.id === undefined ) {
                return `<td class="ode-td-empty" style="width:5%;"> ø </td>`;
            }
            var domiciliation_appairage_html =     `<td style="width:5%;">`;
            domiciliation_appairage_html    +=         `<a target="_blank" href="/index.php?module=OPS_journal&action=DetailView&record=${domiciliation.appairage.journal.id}">`;
            domiciliation_appairage_html    +=             `<img style="width: 17px;" src="custom/include/images/journal.png">`;
            domiciliation_appairage_html    +=         `</a>`;
            domiciliation_appairage_html    +=     `</td>`;
            return domiciliation_appairage_html;
        },

        getDossierErreur: function(dossier){ 
            console.log("AppairageHTML::getDossierErreur => dossier",dossier);
        },

    }

})(jQuery);