<?php

require_once 'custom/include/Astre/Service/AstreService.php';

/**
 * Class AppairageController
 *
 * @package Astre\Controller
 */
class AppairageController
{

    /** @var AstreService */
    protected $astre;

    /** @var DossierEntity */
    protected $dossier;

    /** @var array */
    protected $journal_details = [];

    /**
     * AppairageController constructor.
     * @param AstreConfig $config
     */
    public function __construct(AstreConfig $config)
    {
        $this->astre = new AstreService($config->webservice);
    }

    /**
     * Fonction qui appaire le tiers et sa domiciliation
     * @param DossierEntity     $dossier 
     * @return object
     */
    public function appairer(DossierEntity $dossier)
    {
        $this->dossier = $dossier;

        do {
            // On vérifie si on a bien récupéré le dossier
            if ($this->dossier->statut === false) break;
            // On initialise les détails du journal d'appairage du tiers
            $this->journal_details = [];
            // On vérifie si le dossier dispose d'un bénéficiaire
            $this->isBeneficiaireExist();
            // On recherche le tiers par siret
            $this->getPersonneMoraleBySiret();
            // Vérification de la raison sociale
            $this->isRaisonSocialeConcordante();
            // On met à jour l'appairage
            $this->updateAppairage();
            // On crée le journal d'appairage du tiers
            $this->dossier->setJournalDetails($this->journal_details);
        } while (0);

        return $this->dossier->getOutput();
    }

    /**
     * Cette fonction sert juste à loger dans le journal
     * La récupération/vérification du bénéficaire se fait à l'initialisation de la classe DossierEntity
     * @return void
     */
    public function isBeneficiaireExist()
    {
        // On initialise l'objet détail 
        $detail = $this->dossier->getJournalDetail(
            'Tiers à traiter',
            'Est-ce qu’il existe un bénéficiaire ?'
        );
        // On vérifie le bénéficiaire est renseigné sur le dossier 
        $detail->reponse = ($this->dossier->beneficiaire !== false) ? "Oui : " . $this->dossier->beneficiaire->name : "Non";
        $detail->statut = 'ok';

        $this->journal_details[] = $detail;
    }

    /**
     * Cette fonction fait appelle à Astre
     * @return void
     */
    public function getPersonneMoraleBySiret()
    {

        // On initialise l'objet détail 
        $detail = $this->dossier->getJournalDetail(
            'Recherche par siret',
            'Est-ce que le siret du tiers sur Opensub correspond au siret d\'un tiers sur Astre ?'
        );

        do {

            // On vérifie que le siret n'est pas vide
            if (empty($this->dossier->tiers->siret) || empty(preg_replace('~[^A-Za-z0-9]~', '', $this->dossier->tiers->siret))) {
                $detail->erreur = 'Le Siret n\'est pas renseigné sur OpenSub';
                $detail->description .= 'Veuillez renseigné le siret sur la fiche de la personne morale avant de lancer l\'appairage';
                break;
            }

            // On formate le siret
            $siret_formated = preg_replace('~[^A-Za-z0-9]~', '', $this->dossier->tiers->siret);

            // On met à jour la description du détail
            $detail->description .= '➤ Siret avant formatage : ' . $this->dossier->tiers->siret;
            $detail->description .= " \n" . '➤ Siret apres formatage : ' . $siret_formated;

            // On recherche le tiers par siret 
            $tier_astre = $this->astre->getTiersBySiret($siret_formated);

            // On alimente la description et l'erreur du journal
            $detail->description .= $tier_astre->description;
            $detail->erreur = $tier_astre->erreur;

            if ($tier_astre->statut === true) {

                // On ajoute le tiers récupéré à dossierEntity 
                $this->dossier->tiers_astre = $tier_astre->data;

                // On met à jour la réponse du detail,  NomEnregistrement c'est la raison sociale du tiers sur Astre
                $detail->reponse = 'Oui, ' . $this->dossier->tiers_astre->NomEnregistrement;
                $detail->statut = 'ok';
            }
        } while (0);

        $this->journal_details[] = $detail;
    }

    /**
     * Cette fonction est exécuté que si le tiers a été retrouvé sur Astre
     * @return void
     */
    public function isRaisonSocialeConcordante()
    {

        // On vérifie qu'on a bien récupéré le tiers sur Astre
        if ($this->dossier->tiers_astre !== false) {

            // On initialise l'objet détail 
            $detail = $this->dossier->getJournalDetail(
                'Concordance de la raison sociale',
                'Est-ce que la raison sociale du tiers sur OpenSub est concordante avec celle sur Astre ?'
            );

            do {

                // On vérifie si la raison sociale du tiers retrouvé sur Astre est vide 
                if (empty($this->dossier->tiers_astre->NomEnregistrement)) {
                    $detail->erreur = 'La raison sociale du tiers sur Astre est null';
                    break;
                } else {
                    // On met à jour la raison sociale astre ( OPS_personne_morale $raison_sociale_astre )
                    $this->dossier->updateTiersRaisonSocialeAstre();
                    $detail->description .= 'Le champ "Raison sociale Astre" mis à jour ( Nouvelle valeur : ' . $this->dossier->tiers_astre->NomEnregistrement . ' )';
                }

                // On vérifie que la raison sociale du tiers n'est pas vide
                if (empty($this->dossier->tiers->name)) {
                    $detail->erreur = 'La raison sociale du tiers sur OpenSub est null';
                    break;
                }

                // On vérifie la concordance
                if ($this->dossier->tiers->name === $this->dossier->tiers_astre->NomEnregistrement) {
                    $detail->reponse = 'Oui';
                    $detail->statut = 'ok';
                } else {
                    $detail->erreur = 'OpenSub = ' . $this->dossier->tiers->name . ' | Astre = ' . $this->dossier->tiers_astre->NomEnregistrement;
                }
            } while (0);

            $this->journal_details[] = $detail;
        }
    }


    /**
     * Cette fonction est exécuté que si le tiers a été retrouvé sur Astre
     * @return void
     */
    public function updateAppairage()
    {

        // On initialise l'objet détail 
        $detail = $this->dossier->getJournalDetail(
            'Vérification / Création appairage',
            'Est-ce que le tiers a bien été appairé sur OpenSub ?'
        );

        do {

            // Si l'appairage n'existe pas on l'initialise
            if ($this->dossier->tiers_appairage === false) {
                $this->dossier->initAppairageTiers();
            }

            // Si c'est une erreur de communication avec le webservice => on ne modifie pas l'appairage
            if ($this->astre->hors_service === true) {
                $detail->erreur = 'Erreur de communication avec le websevice';
                $detail->description .= 'L\'appairage n\'a pas pu etre vérifier ';
                break;
            }

            // Si le tiers n'a pas été retouvé sur Astre
            if ($this->dossier->tiers_astre === false) {
                // Si il ne s'agit pas de l'appairage qu'on vient d'initialiser plus haut
                if (strpos($this->dossier->tiers_appairage->name, 'Appairage') === false) {
                    // On met à jour le statut de l'appairage à Erreur
                    $this->dossier->updateAppairageTiers('', 'err');
                    $detail->erreur = 'Le tiers n\'a pas été retrouvé sur Astre, l\'appairage passe en erreur';
                }
                break;
            }

            // On alimente la réponse du détail journal
            if ($this->dossier->tiers_astre->CodeTiers === $this->dossier->tiers_appairage->name) {
                $detail->reponse = 'Oui, appairage vérifié';
            } elseif (strpos($this->dossier->tiers_appairage->name, 'Appairage') !== false) {
                $detail->reponse = 'Oui, appairage crée';
            } else {
                $detail->reponse = 'Oui, appairage modifié';
                $detail->description .= '➤ Ancien : ' . $this->dossier->tiers_appairage->name;
                $detail->description .= " \n" . '➤ Nouveau : ' . $this->dossier->tiers_astre->CodeTiers;
            }

            // On met à jour l'appairage
            $this->dossier->updateAppairageTiers($this->dossier->tiers_astre->CodeTiers, 'ok');
            $detail->statut = 'ok';
        } while (0);

        $this->journal_details[] = $detail;
    }
}
