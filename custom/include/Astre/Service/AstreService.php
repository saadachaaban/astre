<?php

// --------------------- Requests ---------------------
require_once 'custom/include/Astre/Service/Request/RechercheTiersRequest.php';
require_once 'custom/include/Astre/Service/Request/ChargementTiersRequest.php';
require_once 'custom/include/Astre/Service/Request/CreationTiersRibRequest.php';

// --------------------- Responses ---------------------
require_once 'custom/include/Astre/Service/Response/RechercheTiersResponse.php';
require_once 'custom/include/Astre/Service/Response/ChargementTiersResponse.php';
require_once 'custom/include/Astre/Service/Response/CreationTiersRibResponse.php';

/**
 * Class AstreService
 *
 * @package Astre\Service
 */
class AstreService
{
    /** @var bool */
    public $statut = true;

    /** @var bool */
    public $hors_service = false;

    /** @var string */
    protected $url = '';

    /** @var string */
    protected $identifiant = '';

    /** @var string */
    protected $password = '';

    /** @var string */
    protected $organisme = '';

    /** @var string */
    protected $budget = '';

    /** @var string */
    protected $exercice = '';

    /** @var bool|object */
    protected $notification = false;

    /**
     * AstreService constructor.
     * @param object $config
     */
    public function __construct($config)
    {
        if ($config !== false) {
            $this->url = $config->url;
            $this->identifiant = $config->identifiant;
            $this->password = $config->password;
            $this->organisme = $config->organisme;
            $this->budget = $config->budget;
            $this->exercice = $config->exercice;
            $this->notification = $config->notification;
        } else {
            $this->statut = false;
        }
    }

    /**
     * @param string $module_name : Nom du module Astre
     * @param string $post_data : Requete XML
     * @return string
     */
    protected function call($module_name = '', $post_data = '')
    {

        $curl_output = '';

        do {

            // On vérifie que le service est initialisé
            if ($this->statut === false) {
                $GLOBALS['log']->fatal("AstreService::call => Le service n'est pas initilisé, certains paramètres sont indispensables ");
                break;
            }

            // On vérifie que le nom du module n'est pas vide 
            if (empty($module_name)) {
                $GLOBALS['log']->debug("AstreService::call => La fonction ne peut pas être appelé sans le paramètre 'module_name' ");
                break;
            }

            // On vérifie que la requete xml n'est pas vide 
            if (empty($post_data)) {
                $GLOBALS['log']->debug("AstreService::call => La fonction ne peut pas être appelé sans le paramètre 'post_data' ");
                break;
            }

            // On initialise le curl
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $this->url . '/' . $module_name);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: text/xml']);
            $curl_output = curl_exec($curl);
            curl_close($curl);
            /*
            $GLOBALS['log']->fatal("AstreService::call => " . print_r([
                'curl_errno' => curl_errno($curl),
                'empty' => empty($curl_output),
                'curl_error' => curl_error($curl),
            ], true));
            */
            // Si curl retourne une erreur => on notifie l'administrateur par email
            if (curl_errno($curl) || empty($curl_output)) {
                $erreur = (curl_errno($curl)) ? curl_error($curl) : '';
                $this->hors_service = true;
                $this->notificationAstreHS($erreur);
            }
        } while (0);

        return $curl_output;
    }

    /**
     * @param string $erreur : Le libellé de l'erreur rencontré 
     * Fonction qui envoie une notification à l'administrateur d'OpenSub quand le serveur ne répond pas
     */
    protected function notificationAstreHS($erreur = '')
    {
        $GLOBALS['log']->fatal("AstreService::notificationAstreHS => " . print_r($this->notification, true));
        if ($this->notification !== false) {
            // On vérifie si un mail a été envoyé dans les 3h, pour éviter de spamer les administrateurs
            // Send Mail 
            $GLOBALS['log']->fatal("AstreService::notificationAstreHS => Envoie de mail à l'utilisateur " . print_r($this->notification, true));
        }
    }

    /**
     * Initialisation du retour des fonctions public de AstreService
     * @return object
     */
    protected function getOutput()
    {
        return (object) ['statut' => false, 'data' => false, 'erreur' => '', 'description' => ''];
    }

    /**
     * Cette fonction fait appel à deux modules du webservice Astre Tiers
     *      - RechercheTiers : Recherche à partir des critères de l'IHM de recherche Astre GF
     *      - Tiers (consultation) : Recherche de l'existence d'un tiers et sa création dans Astre GF 
     * 
     * @param string $siret
     * @return object $output
     */
    public function getTiersBySiret($siret = '')
    {

        // On initialise le retour de la fonction
        $output = $this->getOutput();

        do {

            // On vérifie que le siret n'est pas vide
            if (empty($siret)) {
                $output->erreur = "Le siret du tiers à rechercher sur astre est null";
                break;
            }

            // On initialise la classe RechercheTiersRequest avant de faire appelle à la fonction getPostData()
            $rechercheTiersRequest = new RechercheTiersRequest([
                'USERNOM' => $this->identifiant,
                'USERPWD' => $this->password,
                'Organisme' => $this->organisme,
                'Budget' => $this->budget,
                'Exercice' => $this->exercice,
                'siren' => $siret
            ]);

            // La description sert à alimenter le journal
            $output->description .= 'Recherche tiers par siret';
            $output->description .= " \n" . '➤ Requête';
            $output->description .= " \n" . '#[' . $rechercheTiersRequest->xml_encoded . ']#';

            // On déclenche l'appel au webservice
            $curl_output = $this->call('RechercheTiers', $rechercheTiersRequest->xml);

            $output->description .= " \n" . '➤ Réponse';
            $output->description .= " \n" . '#[' . base64_encode($curl_output) . ']#';

            // On traduit en langage humain le retour du webservice
            $response = new RechercheTiersResponse($curl_output);
            if ($response->statut === false) {
                $output->erreur = ($this->hors_service === false) ? $response->erreur : 'Erreur de communication avec le webservice Astre';
                break;
            }

            // Le ->N correspond au code tiers ( plus d'infos dans RechercheTiersResponse )
            $code_tiers = (!empty($response->result[0]->N)) ? $response->result[0]->N : '';
            if (empty($code_tiers)) {
                $output->erreur = "Tiers récupéré par siret mais code tiers null";
                break;
            }

            // On initialise la classe ChargementTiersRequest avant de faire appelle à la fonction getPostData() qui retourne la trame XML à envoyer
            $chargementTiersRequest = new ChargementTiersRequest([
                'USERNOM' => $this->identifiant,
                'USERPWD' => $this->password,
                'Organisme' => $this->organisme,
                'Budget' => $this->budget,
                'Exercice' => $this->exercice,
                'CodeTiers' => $code_tiers
            ]);

            $output->description .= " \n" . 'Récupération tiers par code tiers :';
            $output->description .= " \n" . '➤ Requête';
            $output->description .= " \n" . '#[' . $chargementTiersRequest->xml_encoded . ']#';

            // On déclenche l'appel au webservice
            $curl_output = $this->call('Tiers', $chargementTiersRequest->xml);

            $output->description .= " \n" . '➤ Réponse';
            $output->description .= " \n" . '#[' . base64_encode($curl_output) . ']#';

            // On traduit en langage humain le retour du webservice
            $response = new ChargementTiersResponse($curl_output);

            // On alimente le retour
            if ($response->statut === true) {
                $output->data = $response->result;
                $output->statut = true;
            } else {
                $output->erreur = $response->erreur;
            }
        } while (0);

        return $output;
    }

    /**
     * Cette fonction fait appel à un seul module du webservice Astre Tiers
     *      - TiersRib (création) : Recherche de l'existence d'un tiers et sa création dans Astre GF 
     * 
     * @param string $siret
     * @return object $output
     */
    public function createDomiciliation($domiciliation = [])
    {

        // On initialise le retour de la fonction
        $output = $this->getOutput();

        do {

            // On vérifie qu'on a bien un tableau en entrée
            if (empty($domiciliation) || !is_array($domiciliation)) {
                $output->erreur = "domi !== array";
                break;
            }

            // On initialise la classe CreationTiersRibRequest avant de faire appelle à la fonction getPostData()
            $creationTiersRibRequest = new CreationTiersRibRequest(array_merge([
                'USERNOM' => $this->identifiant,
                'USERPWD' => $this->password,
                'Organisme' => $this->organisme,
                'Budget' => $this->budget,
                'Exercice' => $this->exercice,
            ], $domiciliation));

            // La description sert à alimenter le journal
            $output->description .= " \n" . 'Creation d\'une domiciliation :';
            $output->description .= " \n" . '➤ Requête';
            $output->description .= " \n" . '#[' . $creationTiersRibRequest->xml_encoded . ']#';

            // On déclenche l'appel au webservice
            $curl_output = $this->call('Tiers', $creationTiersRibRequest->xml);

            $output->description .= " \n" . '➤ Réponse';
            $output->description .= " \n" . '#[' . base64_encode($curl_output) . ']#';

            // On traduit en langage humain le retour du webservice
            $response = new CreationTiersRibResponse($curl_output);

            // On alimente le retour
            if ($response->statut === true) {
                $output->data = $response->result;
                $output->statut = true;
            } else {
                $output->erreur = $response->erreur;
            }
        } while (0);

        return $output;
    }
}
