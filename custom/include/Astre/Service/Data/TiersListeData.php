<?php

final class TiersListeData
{
    public $T_0TIERS_ID         = '';
    public $N                   = '';
    public $Nom_Enregistrement  = '';
    public $Sigle               = '';
    public $Raison_Sociale      = '';
    public $Raison_Sociale_2    = '';
    public $Code_Postal         = '';
    public $Localite            = '';
}
