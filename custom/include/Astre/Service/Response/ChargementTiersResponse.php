<?php

require_once 'custom/include/Astre/Service/Response/AstreResponse.php';
require_once 'custom/include/Astre/Service/Data/TiersData.php';

/**
 * Class ChargementTiersResponse
 *
 * @package Astre\Service\Response
 */
class ChargementTiersResponse extends AstreResponse
{

    /** @var bool */
    public $statut = false;

    /** @var bool|TiersData */
    public $result = false;

    /** @var string */
    public $erreur = '';

    /** @var bool|object */
    protected $xml_data = false;

    /** @var string */
    protected $erreur_code = '';

    /** @var string */
    protected $erreur_libelle = '';

    /** @var string */
    protected $erreur_detail = '';

    /**
     * ChargementTiersResponse constructor.
     * @param string $curl_output
     */
    public function __construct($curl_output = '')
    {
        parent::__construct($curl_output);
        do {

            if ($this->xml_data === false) {
                $this->erreur = (!empty($this->erreur_libelle)) ? $this->erreur_libelle : 'Erreur de récupération du contenu de l\'xml';
                break;
            }

            if (!isset($this->xml_data->chargementResponse->response->TiersReturn) || empty($this->xml_data->chargementResponse->response->TiersReturn)) {
                break;
            }

            $TiersReturn = json_decode(json_encode($this->xml_data->chargementResponse->response->TiersReturn));
            $tiers_retour = new TiersData();
            foreach ($TiersReturn as $champ_name => $champ_value) {
                if (!empty($champ_name) && isset($tiers_retour->$champ_name) && !empty($champ_value) && gettype($champ_value) === "string") {
                    $tiers_retour->$champ_name = $champ_value;
                }
            }

            if (!empty($tiers_retour->CodeTiers)) {
                $this->result = $tiers_retour;
                $this->statut = true;
            } else {
                $this->erreur = 'Erreur de récupération du tiers';
            }
        } while (0);
    }
}
