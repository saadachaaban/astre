<?php

require_once 'custom/include/Astre/Service/Response/AstreResponse.php';
require_once 'custom/include/Astre/Service/Data/TiersListeData.php';

/**
 * Class RechercheTiersResponse
 *
 * @package Astre\Service\Response
 */
class RechercheTiersResponse extends AstreResponse
{

    /** @var bool */
    public $statut = false;

    /** @var array[TiersListeData] */
    public $result = [];

    /** @var string */
    public $erreur = '';

    /** @var bool|object */
    protected $xml_data = false;

    /** @var string */
    protected $erreur_code = '';

    /** @var string */
    protected $erreur_libelle = '';

    /** @var string */
    protected $erreur_detail = '';

    /**
     * RechercheTiersResponse constructor.
     * @param string $curl_output
     */
    public function __construct($curl_output = '')
    {
        parent::__construct($curl_output);
        do {

            if ($this->xml_data === false) {
                $this->erreur = (!empty($this->erreur_libelle)) ? $this->erreur_libelle : 'Erreur de récupération du contenu de l\'xml';
                break;
            }

            if (!isset($this->xml_data->response->liste) || empty($this->xml_data->response->liste)) break;

            $liste = json_decode(json_encode($this->xml_data->response->liste));

            foreach ($liste as $tiers) {
                if (!empty($tiers)) {
                    $tiers_retour = new TiersListeData();
                    foreach ($tiers as $champ_name => $champ_value) {
                        if (gettype($champ_value) === "string" && !empty($champ_value) && isset($tiers_retour->$champ_name)) {
                            $tiers_retour->$champ_name = $champ_value;
                        }
                    }
                    $this->result[] = $tiers_retour;
                }
            }

            if (is_array($this->result) && count($this->result) > 0) {
                $this->statut = true;
            }
        } while (0);
    }
}
