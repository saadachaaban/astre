<?php

/**
 * Class AstreResponse
 *
 * @package Astre\Service\Response
 */
class AstreResponse
{

    /**
     * AstreResponse constructor.
     * @param string $curl_output
     */
    public function __construct($curl_output = '')
    {

        do {

            // On vérifie que le retour du curl est bien de type 'string' et non vide
            if (empty($curl_output) || gettype($curl_output) !== 'string') {
                $this->erreur_libelle = 'Erreur serveur : Le retour de l\'api est vide';
                $GLOBALS['log']->debug("AstreResponse::__construct => curl_output est vide");
                break;
            }

            // On converti l'xml string en objet SimpleXMLElement
            try {
                $xml = new SimpleXMLElement($curl_output);
            } catch (\Exception $err) {
                $this->erreur_libelle = 'Erreur serveur : Le retour de l\'api ne correspond pas au format attendu';
                $GLOBALS['log']->debug("AstreResponse::__construct => erreur conversion en \SimpleXMLElement" . $err->getMessage());
                break;
            }

            // On récupére le contenu de l'envelope soapenv
            $childrens = $xml->children('soapenv', true);
            if (!isset($childrens->Body)) {
                $this->erreur_libelle = 'Erreur serveur : Le retour de l\'api ne correspond pas au format attendu';
                $GLOBALS['log']->debug("AstreResponse::__construct => 'body' n'existe pas dans l'xml");
                break;
            }

            // On initialise la variable xml_data de la réponse avec le contenu du body
            $body_childrens = $childrens->Body->children('ns1', true);
            if (!empty($body_childrens)) {
                $this->xml_data = $body_childrens;
            }

            // On initialise la variable erreur_code de la réponse avec le contenu de faultcode
            $faultcode = (array) $xml->xpath('//faultcode')[0];
            if (is_array($faultcode) && count($faultcode) > 0) {
                $this->erreur_code = $faultcode[0];
            }

            // On initialise la variable erreur_libelle de la réponse avec le contenu de faultstring
            $faultstring = (array) $xml->xpath('//faultstring')[0];
            if (is_array($faultstring) && count($faultstring) > 0) {
                $this->erreur_libelle = $faultstring[0];
            }

            // On initialise la variable erreur_detail de la réponse avec le contenu de detail
            $detail = (array) $xml->xpath('//detail')[0];
            if (is_array($detail) && count($detail) > 0) {
                $this->erreur_detail = $detail[0];
            }
        } while (0);
    }
}
