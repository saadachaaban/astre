<?php

require_once 'custom/include/Astre/Service/Response/AstreResponse.php';
require_once 'custom/include/Astre/Service/Data/DomiciliationData.php';


/**
 * Class CreationTiersRibResponse
 *
 * @package Astre\Service\Response
 */
class CreationTiersRibResponse extends AstreResponse
{

    /** @var bool */
    public $statut = false;

    /** @var bool|DomiciliationData */
    public $result = false;

    /** @var string */
    public $erreur = '';

    /** @var bool|object */
    protected $xml_data = false;

    /** @var string */
    protected $erreur_code = '';

    /** @var string */
    protected $erreur_libelle = '';

    /** @var string */
    protected $erreur_detail = '';

    /**
     * CreationTiersRibResponse constructor.
     * @param string $curl_output
     */
    public function __construct($curl_output = '')
    {

        parent::__construct($curl_output);

        do {

            if ($this->xml_data === false) {
                $this->erreur = (!empty($this->erreur_libelle)) ? $this->erreur_libelle : 'Erreur de récupération du contenu de l\'xml';
                break;
            }

            if (!isset($this->xml_data->chargementResponse->response->TiersReturn) || empty($this->xml_data->chargementResponse->response->TiersReturn)) {
                break;
            }
        } while (0);
    }
}
