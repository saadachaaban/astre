<?php

require_once 'custom/include/Astre/Service/Request/AstreRequest.php';

/**
 * Class ChargementTiersRibRequest
 *
 * @package Astre\Service\Request
 */
class ChargementTiersRibRequest extends AstreRequest
{
    /** @var string */
    public $xml = '';

    /** @var string */
    public $xml_encoded = '';

    /** @var array */
    protected $USERNOM = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $USERPWD =  ['limit' => 0, 'value' => ''];

    /**
     * ChargementTiersRibRequest constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        parent::__construct($data);
        $this->xml = $this->getXml();
        $this->xml_encoded = $this->getXmlEncoded();
    }

    /**
     * Fonction qui retourne la trame à envoyer, les variables de l'xml sont initialisés dans parent::__construct
     * @return string
     */
    protected function getXml()
    {
        return '';
    }
}
