<?php

require_once 'custom/include/Astre/Service/Request/AstreRequest.php';

/**
 * Class ChargementTiersRequest
 *
 * @package Astre\Service\Request
 */
class ChargementTiersRequest extends AstreRequest
{

    /** @var string */
    public $xml = '';

    /** @var string */
    public $xml_encoded = '';

    /** @var array */
    protected $USERNOM = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $USERPWD =  ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $Organisme = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $Budget = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $Exercice = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CodeTiers = ['limit' => 0, 'value' => ''];

    /**
     * ChargementTiersRequest constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        parent::__construct($data);
        $this->xml = $this->getXml();
        $this->xml_encoded = $this->getXmlEncoded();
    }

    /**
     * Fonction qui retourne la trame à envoyer, les variables de l'xml sont initialisés dans parent::__construct
     * @return string
     */
    protected function getXml()
    {
        return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tier="http://gfi.astre.webservices/gf/tiers">
                    <soapenv:Header/>
                    <soapenv:Body>
                    <tier:chargement>
                        <tier:request>
                            <tier:Authentification>
                                <tier:USERNOM>' . $this->USERNOM['value'] . '</tier:USERNOM>
                                <tier:USERPWD>' . $this->USERPWD['value'] . '</tier:USERPWD>
                            </tier:Authentification>
                            <tier:Contexte>
                                <tier:Organisme>' . $this->Organisme['value'] . '</tier:Organisme>
                                <tier:Budget>' . $this->Budget['value'] . '</tier:Budget>
                                <tier:Exercice>' . $this->Exercice['value'] . '</tier:Exercice>
                            </tier:Contexte>
                            <tier:TiersCle>
                                <tier:CodeTiers>' . $this->CodeTiers['value'] . '</tier:CodeTiers>
                            </tier:TiersCle>
                        </tier:request>
                    </tier:chargement>
                    </soapenv:Body>
                </soapenv:Envelope>';
    }
}
