<?php

require_once 'custom/include/Astre/Service/Request/AstreRequest.php';

/**
 * Class CreationTiersRibRequest
 *
 * @package Astre\Service\Request
 */
class CreationTiersRibRequest extends AstreRequest
{

    /** @var string */
    public $xml = '';

    /** @var string */
    public $xml_encoded = '';

    /** @var array */
    protected $USERNOM = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $USERPWD =  ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $Organisme = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $Budget = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $Exercice = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CodeTiers = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $IdRib = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CodeDomiciliation = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CodePaiement = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $LibelleCourt = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $IndicateurRibDefaut = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CodeStatut = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CodeBanque = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CodeGuichet = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $Comptefrancais = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CleRib = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $LibelleBanque = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $LibelleGuichet = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CodeDevise = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $LibelleDevise = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $LibellePays = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CodeIso2Pays = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CompteEtranger = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CodeBic = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CleIban = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $NumeroIban = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $LibelleCompteEtranger = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $Libelle2CompteEtranger = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $NumeroTiersSubrogatoire = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $IdRibSubrogatoire = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $IntituleRibSub = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $IndicateurRibEtranger = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $TiersIdSubrogatoire = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $VilleTiersSub = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $NomTiersSub = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $NumRibSubrogatoire = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $CodePostalTiersSub = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $Commentaire = ['limit' => 0, 'value' => ''];

    /**
     * CreationTiersRibRequest constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        parent::__construct($data);
        $this->xml = $this->getXml();
        $this->xml_encoded = $this->getXmlEncoded();
    }

    /**
     * Fonction qui retourne la trame à envoyer, les variables de l'xml sont initialisés dans parent::__construct
     * @return string
     */
    protected function getXml()
    {
        return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tier="http://gfi.astre.webservices/gf/tiers/tiersrib">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <tier:creation>
                            <tier:request>
                                <tier:Authentification>
                                    <tier:USERNOM>' . $this->USERNOM['value'] . '</tier:USERNOM>
                                    <tier:USERPWD>' . $this->USERPWD['value'] . '</tier:USERPWD>
                                </tier:Authentification>
                                <tier:Contexte>
                                    <tier:Organisme>' . $this->Organisme['value'] . '</tier:Organisme>
                                    <tier:Budget>' . $this->Budget['value'] . '</tier:Budget>
                                    <tier:Exercice>' . $this->Exercice['value'] . '</tier:Exercice>
                                </tier:Contexte>
                                <tier:TiersRib>
                                    <tier:CodeTiers>' . $this->CodeTiers['value'] . '</tier:CodeTiers>
                                    <tier:IdRib>' . $this->IdRib['value'] . '</tier:IdRib>
                                    <tier:CodeDomiciliation>' . $this->CodeDomiciliation['value'] . '</tier:CodeDomiciliation>
                                    <tier:CodePaiement>' . $this->CodePaiement['value'] . '</tier:CodePaiement>
                                    <tier:LibelleCourt>' . $this->LibelleCourt['value'] . '</tier:LibelleCourt>
                                    <tier:IndicateurRibDefaut>' . $this->IndicateurRibDefaut['value'] . '</tier:IndicateurRibDefaut>
                                    <tier:CodeStatut>' . $this->CodeStatut['value'] . '</tier:CodeStatut>
                                    <tier:CodeBanque>' . $this->CodeBanque['value'] . '</tier:CodeBanque>
                                    <tier:CodeGuichet>' . $this->CodeGuichet['value'] . '</tier:CodeGuichet>
                                    <tier:Comptefrancais>' . $this->Comptefrancais['value'] . '</tier:Comptefrancais>
                                    <tier:CleRib>' . $this->CleRib['value'] . '</tier:CleRib>
                                    <tier:LibelleBanque>' . $this->LibelleBanque['value'] . '</tier:LibelleBanque>
                                    <tier:LibelleGuichet>' . $this->LibelleGuichet['value'] . '</tier:LibelleGuichet>
                                    <tier:CodeDevise>' . $this->CodeDevise['value'] . '</tier:CodeDevise>
                                    <tier:LibelleDevise>' . $this->LibelleDevise['value'] . '</tier:LibelleDevise>
                                    <tier:LibellePays>' . $this->LibellePays['value'] . '</tier:LibellePays>
                                    <tier:CodeIso2Pays>' . $this->CodeIso2Pays['value'] . '</tier:CodeIso2Pays>
                                    <tier:CompteEtranger>' . $this->CompteEtranger['value'] . '</tier:CompteEtranger>
                                    <tier:CodeBic>' . $this->CodeBic['value'] . '</tier:CodeBic>
                                    <tier:CleIban>' . $this->CleIban['value'] . '</tier:CleIban>
                                    <tier:NumeroIban>' . $this->NumeroIban['value'] . '</tier:NumeroIban>
                                    <tier:LibelleCompteEtranger>' . $this->LibelleCompteEtranger['value'] . '</tier:LibelleCompteEtranger>
                                    <tier:Libelle2CompteEtranger>' . $this->Libelle2CompteEtranger['value'] . '</tier:Libelle2CompteEtranger>
                                    <tier:NumeroTiersSubrogatoire>' . $this->NumeroTiersSubrogatoire['value'] . '</tier:NumeroTiersSubrogatoire>
                                    <tier:IdRibSubrogatoire>' . $this->IdRibSubrogatoire['value'] . '</tier:IdRibSubrogatoire>
                                    <tier:IntituleRibSub>' . $this->IntituleRibSub['value'] . '</tier:IntituleRibSub>
                                    <tier:IndicateurRibEtranger>' . $this->IndicateurRibEtranger['value'] . '</tier:IndicateurRibEtranger>
                                    <tier:TiersIdSubrogatoire>' . $this->TiersIdSubrogatoire['value'] . '</tier:TiersIdSubrogatoire>
                                    <tier:VilleTiersSub>' . $this->VilleTiersSub['value'] . '</tier:VilleTiersSub>
                                    <tier:NomTiersSub>' . $this->NomTiersSub['value'] . '</tier:NomTiersSub>
                                    <tier:NumRibSubrogatoire>' . $this->NumRibSubrogatoire['value'] . '</tier:NumRibSubrogatoire>
                                    <tier:CodePostalTiersSub>' . $this->CodePostalTiersSub['value'] . '</tier:CodePostalTiersSub>
                                    <tier:Commentaire>' . $this->Commentaire['value'] . '</tier:Commentaire>
                                </tier:TiersRib>
                            </tier:request>
                        </tier:creation>
                    </soapenv:Body>
                </soapenv:Envelope>';
    }
}
