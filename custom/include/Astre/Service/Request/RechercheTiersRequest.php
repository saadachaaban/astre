<?php

require_once 'custom/include/Astre/Service/Request/AstreRequest.php';

/**
 * Class RechercheTiersRequest
 *
 * @package Astre\Service\Request
 */
class RechercheTiersRequest extends AstreRequest
{

    /** @var string */
    public $xml = '';

    /** @var string */
    public $xml_encoded = '';

    /** @var array */
    protected $USERNOM = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $USERPWD = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $Organisme = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $Budget = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $Exercice = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $NbEnreg = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $ListeResultat = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $casseCodeTiers = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $NbEnregPage = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $isFinancier = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $numeroTiers = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $nom = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $nomSigleRaison = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $nomContact = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $indicateurAssociation = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $codeFamille = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $libelleFamille = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $siren = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $agence = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $codeApe = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $libelleCodeApe = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $banque = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $guichet = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $numeroCompte = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $cleRIB = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $AdrEmail = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $codePostal = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $ville = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $encodeKeyStatut = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $typeTiers = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $isPermanent = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $ancienNumero = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $encodeKeyNomana = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $codeElement = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $codeService = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $encodeKeyIdCompl = ['limit' => 0, 'value' => ''];

    /** @var array */
    protected $valeurIdCompl = ['limit' => 0, 'value' => ''];

    /**
     * RechercheTiersRequest constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        parent::__construct($data);
        $this->xml = $this->getXml();
        $this->xml_encoded = $this->getXmlEncoded();
    }

    /**
     * Fonction qui retourne la trame à envoyer, l'xml est initialisé dans parent::__construct
     * @return string
     */
    protected function getXml()
    {
        return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:rec="http://gfi.astre.webservices/gf/recherchetiers">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <rec:request>
                            <rec:Authentification>
                                <rec:USERNOM>' . $this->USERNOM['value'] . '</rec:USERNOM>
                                <rec:USERPWD>' . $this->USERPWD['value'] . '</rec:USERPWD>
                            </rec:Authentification>
                            <rec:Contexte>
                                <rec:Organisme>' . $this->Organisme['value'] . '</rec:Organisme>
                                <rec:Budget>' . $this->Budget['value'] . '</rec:Budget>
                                <rec:Exercice>' . $this->Exercice['value'] . '</rec:Exercice>
                            </rec:Contexte>
                            <rec:Criteres>
                                <rec:NbEnreg>' . $this->NbEnreg['value'] . '</rec:NbEnreg>
                                <rec:ListeResultat>' . $this->ListeResultat['value'] . '</rec:ListeResultat>
                                <rec:casseCodeTiers>' . $this->casseCodeTiers['value'] . '</rec:casseCodeTiers>
                                <rec:NbEnregPage>' . $this->NbEnregPage['value'] . '</rec:NbEnregPage>
                                <rec:isFinancier>' . $this->isFinancier['value'] . '</rec:isFinancier>
                                <rec:numeroTiers>' . $this->numeroTiers['value'] . '</rec:numeroTiers>
                                <rec:nom>' . $this->nom['value'] . '</rec:nom>
                                <rec:nomSigleRaison>' . $this->nomSigleRaison['value'] . '</rec:nomSigleRaison>
                                <rec:nomContact>' . $this->nomContact['value'] . '</rec:nomContact>
                                <rec:indicateurAssociation>' . $this->indicateurAssociation['value'] . '</rec:indicateurAssociation>
                                <rec:codeFamille>' . $this->codeFamille['value'] . '</rec:codeFamille>
                                <rec:libelleFamille>' . $this->libelleFamille['value'] . '</rec:libelleFamille>
                                <rec:siren>' . $this->siren['value'] . '</rec:siren>
                                <rec:agence>' . $this->agence['value'] . '</rec:agence>
                                <rec:codeApe>' . $this->codeApe['value'] . '</rec:codeApe>
                                <rec:libelleCodeApe>' . $this->libelleCodeApe['value'] . '</rec:libelleCodeApe>
                                <rec:banque>' . $this->banque['value'] . '</rec:banque>
                                <rec:guichet>' . $this->guichet['value'] . '</rec:guichet>
                                <rec:numeroCompte>' . $this->numeroCompte['value'] . '</rec:numeroCompte>
                                <rec:cleRIB>' . $this->cleRIB['value'] . '</rec:cleRIB>
                                <rec:AdrEmail>' . $this->AdrEmail['value'] . '</rec:AdrEmail>
                                <rec:codePostal>' . $this->codePostal['value'] . '</rec:codePostal>
                                <rec:ville>' . $this->ville['value'] . '</rec:ville>
                                <rec:encodeKeyStatut>' . $this->encodeKeyStatut['value'] . '</rec:encodeKeyStatut>
                                <rec:typeTiers>' . $this->typeTiers['value'] . '</rec:typeTiers>
                                <rec:isPermanent>' . $this->isPermanent['value'] . '</rec:isPermanent>
                                <rec:ancienNumero>' . $this->ancienNumero['value'] . '</rec:ancienNumero>
                                <rec:encodeKeyNomana>' . $this->encodeKeyNomana['value'] . '</rec:encodeKeyNomana>
                                <rec:codeElement>' . $this->codeElement['value'] . '</rec:codeElement>
                                <rec:codeService>' . $this->codeService['value'] . '</rec:codeService>
                                <rec:encodeKeyIdCompl>' . $this->encodeKeyIdCompl['value'] . '</rec:encodeKeyIdCompl>
                                <rec:valeurIdCompl>' . $this->valeurIdCompl['value'] . '</rec:valeurIdCompl>
                            </rec:Criteres>
                        </rec:request>
                    </soapenv:Body>
                </soapenv:Envelope>';
    }
}
