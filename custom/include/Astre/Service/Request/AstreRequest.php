<?php

/**
 * Class AstreRequest
 * J'effectue le meme traitement pour les 'string', 'integer', et 'double' ( Evolution si besoin )
 * 
 * @package Astre\Service\Request
 */
class AstreRequest
{
    /**
     * AstreRequest constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        if (is_array($data) && count($data) > 0) {
            foreach ($data as $name => $value) {
                // On boucle sur le tableau $data pour initialiser les variables du xml
                if (isset($this->$name) && !empty($value) && in_array(gettype($value), ['string', 'integer', 'double'])) {
                    // On vérifie si le champ est limité en caractères. Si oui, on prend en compte la limite
                    $this->$name['value'] = ($this->$name['limit'] > 0 && strlen(strval($value)) > $this->$name['limit'])
                        ? substr(strval($value), 0, $this->$name['limit'])
                        : $value;
                }
            }
        }
    }

    /**
     * Fonction qui remplace l'identifiant et le mot de passe dans l'xml avant de l'encoder en base64
     * @return string
     */
    public function getXmlEncoded()
    {
        if (empty($this->USERNOM['value']) || empty($this->USERPWD['value'])) return '';
        return (!empty($this->xml)) ? base64_encode(str_replace([$this->USERNOM['value'], $this->USERPWD['value']], '********', $this->xml)) : '';
    }
}
