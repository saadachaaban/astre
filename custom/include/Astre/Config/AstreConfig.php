<?php

final class AstreConfig
{
    public $statut;
    public $webservice;
    public $filters;

    protected $url;
    protected $identifiant;
    protected $password;
    protected $organisme;
    protected $budget;
    protected $exercice;
    protected $modele_mail;
    protected $users;
    protected $actif;
    protected $config_filters = [];
    protected $notification;

    public function __construct()
    {

        global $sugar_config;

        if (is_array($sugar_config['opensub']['astre']) && count($sugar_config['opensub']['astre']) > 0) {

            $this->url = (!empty($sugar_config['opensub']['astre']['url'])) ? $sugar_config['opensub']['astre']['url'] : '';
            $this->identifiant = (!empty($sugar_config['opensub']['astre']['identifiant'])) ? $sugar_config['opensub']['astre']['identifiant'] : '';
            $this->password = (!empty($sugar_config['opensub']['astre']['password'])) ? $sugar_config['opensub']['astre']['password'] : '';
            $this->organisme = (!empty($sugar_config['opensub']['astre']['organisme'])) ? $sugar_config['opensub']['astre']['organisme'] : '';
            $this->budget = (!empty($sugar_config['opensub']['astre']['budget'])) ? $sugar_config['opensub']['astre']['budget'] : '';
            $this->exercice = (!empty($sugar_config['opensub']['astre']['exercice'])) ? $sugar_config['opensub']['astre']['exercice'] : '';
            $this->statut = (!empty($this->url) && !empty($this->identifiant)) ? true : false;

            $this->modele_mail = (!empty($sugar_config['opensub']['astre']['modele_mail'])) ? $sugar_config['opensub']['astre']['modele_mail'] : '';
            $this->users = (!empty($sugar_config['opensub']['astre']['users'])) ? $sugar_config['opensub']['astre']['users'] : '';
            $this->actif = (!empty($sugar_config['opensub']['astre']['actif']) && $sugar_config['opensub']['astre']['actif'] === 'oui') ? 'oui' : 'non';


            if (is_array($sugar_config['opensub']['astre']['filtres']) && count($sugar_config['opensub']['astre']['filtres']) > 0) {
                if (!empty($sugar_config['opensub']['astre']['filtres']['exercice']['ids']) && !empty($sugar_config['opensub']['astre']['filtres']['exercice']['type']) && $sugar_config['opensub']['astre']['filtres']['exercice']['type'] !== 'aucun') {
                    $this->config_filters['exercice'] = (object) [
                        'ids' => $sugar_config['opensub']['astre']['filtres']['exercice']['ids'],
                        'type' => $sugar_config['opensub']['astre']['filtres']['exercice']['type']
                    ];
                }
                if (!empty($sugar_config['opensub']['astre']['filtres']['dispositif']['ids']) && !empty($sugar_config['opensub']['astre']['filtres']['dispositif']['type']) && $sugar_config['opensub']['astre']['filtres']['dispositif']['type'] !== 'aucun') {
                    $this->config_filters['dispositif'] = (object) [
                        'ids' => $sugar_config['opensub']['astre']['filtres']['dispositif']['ids'],
                        'type' => $sugar_config['opensub']['astre']['filtres']['dispositif']['type']
                    ];
                }
                if (!empty($sugar_config['opensub']['astre']['filtres']['statut']['ids']) && !empty($sugar_config['opensub']['astre']['filtres']['statut']['type']) && $sugar_config['opensub']['astre']['filtres']['statut']['type'] !== 'aucun') {
                    $this->config_filters['statut'] = (object) [
                        'ids' => $sugar_config['opensub']['astre']['filtres']['statut']['ids'],
                        'type' => $sugar_config['opensub']['astre']['filtres']['statut']['type']
                    ];
                }
            }
        } else {
            $GLOBALS['log']->fatal("AstreConfig => Le connecteur Astre n'est pas configuré");
        }

        $this->notification =  (!empty($this->users) && !empty($this->modele_mail) && $this->actif === 'oui')
            ? (object) [
                'users' => $this->users,
                'modele' => $this->modele_mail
            ] : false;

        $this->filters =  (is_array($this->config_filters) && count($this->config_filters) > 0)
            ? (object) $this->config_filters : false;

        $this->webservice = ($this->statut === true)
            ? (object) [
                'url' => $this->url,
                'identifiant' => $this->identifiant,
                'password' => $this->password,
                'organisme' => $this->organisme,
                'budget' => $this->budget,
                'exercice' => $this->exercice,
                'notification' => $this->notification
            ] : false;
    }
}
