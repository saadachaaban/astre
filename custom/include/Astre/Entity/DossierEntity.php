<?php

require_once 'custom/include/Ode/Service/DossierService.php';
require_once 'custom/include/Ode/Service/IndividuService.php';
require_once 'custom/include/Ode/Service/DomiciliationService.php';
require_once 'custom/include/Ode/Service/JournalService.php';

/**
 * Class DossierEntity
 *
 * @package Astre\Entity
 */
class DossierEntity
{

    /** @var bool|object */
    protected $dossier;

    /** @var string */
    protected $dossier_erreur;

    /** @var string */
    protected $tiers_erreur;

    /** @var string */
    protected $domiciliation_erreur;

    /** @var bool */
    public $statut = false;

    /** @var bool|object */
    public $beneficiaire = false;

    /** @var bool|object */
    public $demandeur = false;

    /** @var bool|object */
    public $tiers = false;

    /** @var bool|object */
    public $tiers_astre = false;

    /** @var bool|object */
    public $tiers_appairage = false;

    /** @var bool|object */
    public $tiers_appairage_journal = false;

    /** @var bool|object */
    public $tiers_domiciliations = false;

    /** @var bool|object */
    public $domiciliation_appairage = false;

    /** @var bool|object */
    public $domiciliation_appairage_journal = false;

    /** @var bool|object */
    public $domiciliation = false;

    /** @var string */
    public $dossier_num = '';

    /**
     * DossierEntity constructor.
     * @param string $dossier_id
     */
    public function __construct($dossier_id = '')
    {
        $this->dossier = DossierService::get($dossier_id);
        if ($this->dossier !== false) {
            $this->dossier_num = $this->dossier->num_dossier;
            $this->setDemandeur();
            $this->setBeneficiaire();
            $this->setTiers();
            $this->setTiersDomiciliations();
            $this->setTiersAppairage();
            $this->setTiersAppairageJournal();
            $this->setDomiciliation();
            $this->setDomiciliationAppairage();
            $this->setDomiciliationAppairageJournal();
            if ($this->demandeur !== false || $this->tiers !== false) {
                $this->statut = true;
            }
        }
    }

    /**
     * Fonction qui retourne l'état d'appairage du dossier
     * @return object
     */
    public function getOutput()
    {
        $tiers = $this->getTiersInfos();
        $domiciliation = $this->getDomiciliationInfos();
        $dossier = $this->getDossierInfos();
        $dossier->statut_appairage = ($tiers->statut === true && $domiciliation->statut === true && $tiers->appairage->statut === "ok" && $domiciliation->appairage->statut === "ok") ? true : false;
        return (object) [
            'statut' => $this->statut,
            'dossier' => $dossier,
            'tiers' => $tiers,
            'domiciliation' => $domiciliation
        ];
    }

    /**
     * Fonction qui retourne les informations qui concerne le dossier
     * @return object
     */
    public function getDossierInfos()
    {
        if (empty($this->dossier_erreur)) {
            return (object) ['statut' => true, 'id' => $this->dossier->id, 'name' => $this->dossier->libelle_dossier, 'num' => $this->dossier->num_dossier];
        } else {
            return (object) ['statut' => false, 'erreur' => $this->dossier_erreur];
        }
    }

    /**
     * Fonction qui retourne les informations qui concerne le tiers à traiter
     * @return object
     */
    public function getTiersInfos()
    {
        if (empty($this->tiers_erreur)) {
            return (object) [
                'statut' => true,
                'id' => $this->tiers->id,
                'name' => $this->tiers->name_display,
                'type' => $this->tiers->module_type,
                'appairage' => $this->getAppairageTiersInfos()
            ];
        } else {
            return (object) ['statut' => false, 'erreur' => $this->tiers_erreur];
        }
    }

    /**
     * Fonction qui retourne les informations qui concerne la domiciliation du dossier
     * @return object
     */
    public function getDomiciliationInfos()
    {
        if ($this->domiciliation !== false) {
            return (object) [
                'statut' => true,
                'id' => $this->domiciliation->id,
                'name' => $this->domiciliation->name,
                'type' => $this->domiciliation->module_type,
                'appairage' => $this->getAppairageDomiciliationInfos()
            ];
        } else {
            return (object) ['statut' => false, 'erreur' => $this->domiciliation_erreur];
        }
    }

    /**
     * Fonction d'initialisation du demandeur
     * @return void
     */
    protected function setDemandeur()
    {
        $this->demandeur = DossierService::getDemandeur($this->dossier->id, $this->dossier->type_tiers);
    }

    /**
     * Fonction d'initialisation du beneficiaire
     * @return void
     */
    protected function setBeneficiaire()
    {
        if (!empty($this->dossier->ops_personne_individu)) {
            $this->beneficiaire = IndividuService::get($this->dossier->ops_personne_individu);
        }
    }

    /**
     * Fonction d'initialisation du tiers
     * @return void
     */
    protected function setTiers()
    {
        if ($this->beneficiaire !== false) {
            $this->tiers = $this->beneficiaire;
            $this->tiers->type = 'beneficiaire';
        } else {
            if ($this->demandeur !== false) {
                $this->tiers = $this->demandeur;
                $this->tiers->type = 'demandeur';
            } else {
                $this->tiers_erreur = 'Erreur de récupération du demandeur';
            }
        }
    }

    /**
     * Fonction d'initialisation des domiciliations du tiers
     * @return void
     */
    protected function setTiersDomiciliations()
    {
        if ($this->tiers !== false) {
            if ($this->tiers->module_type === 'OPS_personne_morale') {
                $this->tiers_domiciliations = PersonneMoraleService::getDomiciliations($this->tiers->id);
            } else if ($this->tiers->module_type === 'OPS_individu') {
                $this->tiers_domiciliations = IndividuService::getDomiciliations($this->tiers->id);
            }
        }
    }

    /**
     * Fonction d'initialisation de l'appairage du tiers
     * @return void
     */
    protected function setTiersAppairage()
    {
        if ($this->tiers !== false) {
            if ($this->tiers->module_type === 'OPS_personne_morale') {
                $this->tiers_appairage = PersonneMoraleService::getAppairage($this->tiers->id, 'gestionfin');
            } else if ($this->tiers->module_type === 'OPS_individu') {
                $this->tiers_appairage = IndividuService::getAppairage($this->tiers->id, 'gestionfin');
            }
        }
    }

    /**
     * Fonction d'initialisation du journal d'appairage du tiers
     * @return void
     */
    public function setTiersAppairageJournal()
    {
        if ($this->tiers_appairage !== false) {
            $this->tiers_appairage_journal = JournalService::getByParent('OPS_appairage', $this->tiers_appairage->id);
        }
    }

    /**
     * Fonction d'initialisation de la domiciliation du dossier
     * @return void
     */
    protected function setDomiciliation()
    {
        $this->dossier->domiciliation = $this->dossier->astre_domiciliation;
        if (!empty($this->dossier->domiciliation)) {
            $this->domiciliation = DomiciliationService::get($this->dossier->domiciliation);
        }
    }

    /**
     * Fonction d'initialisation de l'appairage de la domiciliation du dossier
     * @return void
     */
    protected function setDomiciliationAppairage()
    {
        if ($this->domiciliation !== false) {
            $this->domiciliation_appairage = DomiciliationService::getAppairage($this->domiciliation->id, 'gestionfin');
        }
    }

    /**
     * Fonction d'initialisation du journal d'appairage de la domiciliation
     * @return void
     */
    public function setDomiciliationAppairageJournal()
    {
        if ($this->domiciliation_appairage !== false) {
            $this->domiciliation_appairage_journal = JournalService::getByParent('OPS_appairage', $this->domiciliation_appairage->id);
        }
    }



    protected function getAppairageTiersInfos()
    {
        if ($this->tiers_appairage !== false) {
            return (object) [
                'id' => $this->tiers_appairage->id,
                'name' => $this->tiers_appairage->name,
                'statut' => $this->tiers_appairage->statut,
                'journal' => ($this->tiers_appairage_journal !== false) ? (object) ['id' => $this->tiers_appairage_journal->id, 'name' => $this->tiers_appairage_journal->name, 'statut' => $this->tiers_appairage_journal->statut] : false
            ];
        } else {
            return (object) ['statut' => false, 'erreur' => 'Non appairé', 'journal' => (object) ['statut' => false]];
        }
    }

    protected function getAppairageDomiciliationInfos()
    {
        if ($this->domiciliation_appairage !== false) {
            return (object) [
                'id' => $this->domiciliation_appairage->id,
                'name' => $this->domiciliation_appairage->name,
                'statut' => $this->domiciliation_appairage->statut,
                'journal' => ($this->domiciliation_appairage_journal !== false) ? (object) ['id' => $this->domiciliation_appairage_journal->id, 'name' => $this->domiciliation_appairage_journal->name, 'statut' => $this->domiciliation_appairage_journal->statut] : false
            ];
        } else {
            return (object) ['statut' => false, 'erreur' => 'Non appairé', 'journal' => (object) ['statut' => false]];
        }
    }

    public function getJournalDetail($name = '', $tache = '')
    {
        return (object) [
            'name' => $name,
            'tache' => $tache,
            'reponse' => 'Non',
            'statut' => 'err',
            'erreur' => '',
            'description' => '',
        ];
    }

    public function updateAppairageTiers($appairage_name = '', $appairage_statut = '')
    {
        if ($this->tiers !== false && $this->tiers_appairage !== false) {
            $name = (!empty($appairage_name)) ? $appairage_name : 'Appairage Tiers' . $this->tiers->name_display;
            $statut = ($appairage_statut === 'ok') ? 'ok' : 'err';
            $obj_appairage = BeanFactory::getBean('OPS_appairage', $this->tiers_appairage->id);
            if (!empty($obj_appairage->id)) {
                $obj_appairage->name = $name;
                $obj_appairage->statut = $statut;
                if ($obj_appairage->save()) {
                    $this->setTiersAppairage();
                }
            }
        }
    }

    public function initAppairageTiers()
    {
        if ($this->tiers !== false && $this->tiers_appairage === false) {
            $obj_appairage = BeanFactory::newBean('OPS_appairage');
            $obj_appairage->parent_type = $this->tiers->module_type;
            $obj_appairage->parent_id = $this->tiers->id;
            $obj_appairage->name = "Appairage Tiers : " . $this->tiers->name_display;
            $obj_appairage->statut = "err";
            $obj_appairage->logiciel = "gestionfin";
            if ($obj_appairage->save()) {
                $this->setTiersAppairage();
            }
        }
    }

    public function setJournalDetails($details = [])
    {
        if ($this->tiers !== false && $this->tiers_appairage !== false) {

            // Récupération ou création du journal si il n'existe pas
            if ($this->tiers_appairage_journal !== false) {
                $obj_journal = BeanFactory::getBean('OPS_journal', $this->tiers_appairage_journal->id);
            } else {
                $obj_journal = BeanFactory::newBean('OPS_journal');
                $obj_journal->name = 'Journal Appairage Tiers : ' . $this->tiers->name_display;
                $obj_journal->parent_type = 'OPS_appairage';
                $obj_journal->parent_id = $this->tiers_appairage->id;
                $obj_journal->statut = $this->tiers_appairage->statut;
                if ($obj_journal->save()) {
                    $this->tiers_appairage_journal = JournalService::get($obj_journal->id);
                }
            }

            if (!empty($obj_journal->id) && is_array($details) && count($details) > 0) {
                JournalService::deleteDetails($obj_journal->id);
                $obj_journal->load_relationship('ops_journal_detail_ops_journal');
                foreach ($details as $key => $detail) {
                    $obj_detail = BeanFactory::newBean('OPS_journal_detail');
                    $obj_detail->name = $detail->name;
                    $obj_detail->tache = $detail->tache;
                    $obj_detail->reponse = $detail->reponse;
                    $obj_detail->description = $detail->description;
                    $obj_detail->statut = $detail->statut;
                    $obj_detail->erreur = $detail->erreur;
                    $obj_detail->ordre = $key + 1;
                    $obj_detail->save();
                    $obj_journal->ops_journal_detail_ops_journal->add($obj_detail);
                }
            }
        }
    }

    public function updateTiersRaisonSocialeAstre()
    {
        if ($this->tiers !== false && $this->tiers->module_type === 'OPS_personne_morale' && $this->tiers_astre !== false && !empty($this->tiers_astre->NomEnregistrement)) {
            $obj_personne_morale = BeanFactory::getBean('OPS_personne_morale', $this->tiers->id);
            if (!empty($obj_personne_morale->id)) {
                $obj_personne_morale->name = $this->tiers_astre->NomEnregistrement;
                $obj_personne_morale->save();
            }
        }
    }
}
