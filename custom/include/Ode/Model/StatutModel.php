<?php

//namespace ODE\Model;
//use ODE\Helper\OdeArrayHelper;

if (!defined('sugarEntry')) define('sugarEntry', true);

class StatutModel
{

    private $time_start;
    private $time_end;
    private $id;

    /**
     * Constructeur de la classe StatutModel
     * 
     * @access public
     * @name __construct
     * @return void
     */
    public function __construct($id)
    {

        $this->time_start = microtime(true);
        $this->time_end = microtime(true);
        $this->id = $id;
    }

    /**
     * @access public
     * @name getBean()
     * Fonction qui retourne les données de la personne morale en tableau
     *
     *  @return array               - $appairage: retourne l'appairage de la personne morale
     */
    public function getBean()
    {

        global $db;
        $statuts = array();
        $sql = "SELECT * FROM `ops_statut` WHERE `id` = '" . $this->id . "' AND `deleted` = '0' ";
        $result = $db->query($sql);
        while ($row = $db->fetchByAssoc($result)) {
            $statuts[] = $row;
        }
        return (!empty($statuts) && count($statuts) === 1) ? $statuts[0] : array();
    }

    /**
     * @access public
     * @name getEngagementIds()
     * Fonction qui retourne l'id du engagement
     *
     *  @return array               - $engagement_id : retourne l'id du engagement si il existe
     */
    public static function getGuideInstructionName($statut_id)
    {
        global $db;
        $guides = array();
        if (!empty($statut_id)) {
            $sql = "SELECT `name`
            FROM `ops_guide_instruction`
            INNER JOIN `ops_guide_instruction_ops_etape` ON `ops_guide_instruction`.id = `ops_guide_instruction_ops_etape`.ops_guide_instruction_id
            INNER JOIN `ops_etape_ops_statut` ON `ops_guide_instruction_ops_etape`.ops_etape_id = `ops_etape_ops_statut`.ops_etape_id
            WHERE `ops_etape_ops_statut`.ops_statut_id ='" . $statut_id . "'";

            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $guides[] = $row['name'];
            }
        }
        return (!empty($guides[0])) ? $guides[0] : "";
    }
}
