var OdeQueries = OdeQueries || {};

OdeQueries = (function() {

    const AjaxActionQuery = (function() {

        var module  = '';
        var action    = '';
        var postData  = {}; 

        return {
            setModule: function(mod) {
                module = mod;
            },
            getModule: function() {
                return module;
            },
            setAction: function(act) {
                action = act;
            },
            getAction: function() {
                return action;
            },
            setPostData: function(data) {
                postData = data;
            },
            getPostData: function() {
                return postData;
            },
        }

    })();

    return {
        getAjaxActionQuery: function() {
            return Object.create(AjaxActionQuery);
        }
    }

})();