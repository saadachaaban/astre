var OdeLoadingHelper = (function ($) {

    return {

        addOverlay: function(){
            $('body').append($('<div id="overlay_body" class="overlay-body" ></div>'));
        },
    
        addSpinner: function( el = null , static_pos ){
            this.showOverlay();
            el = ( el !== null ) ? el : $(document.body);
            var spinner = el.children('.spinner');
            if (spinner.length && !spinner.hasClass('spinner-remove')) return null;
            !spinner.length && (spinner = $('<div class="spinner' + (static_pos ? '' : ' spinner-absolute') + '"/>').appendTo(el));
            this.animateSpinner(spinner, 'add');
        },
    
        removeSpinner: function(el = null, complete){
            this.hideOverlay();
            el = ( el !== null ) ? el : $(document.body);
            var spinner = el.children('.spinner');
            spinner.length && this.animateSpinner(spinner, 'remove', complete);
        },
    
        hideOverlay: function(){ $("#overlay_body").hide();},

        showOverlay: function(){ 
            if ( $("#overlay_body").length === 0 ) this.addOverlay();
            $("#overlay_body").show("fade", { direction: "right", align: "center" }, 200 ) ;
        },
    
        animateSpinner: function(el, animation, complete){
            if (el.data('animating')) {
                el.removeClass(el.data('animating')).data('animating', null);
                el.data('animationTimeout') && clearTimeout(el.data('animationTimeout'));
            }
            el.addClass('spinner-' + animation).data('animating', 'spinner-' + animation);
            el.data('animationTimeout', setTimeout(function() { animation == 'remove' && el.remove(); complete && complete(); }, parseFloat(el.css('animation-duration')) * 1000));
        },

        addSpinnerMenu: function(static_pos){
            $("#g-menu-overlay").show();
            var spinner =  $("#g-menu-overlay").children('.spinner');
            if (spinner.length && !spinner.hasClass('spinner-remove')) return null;
            !spinner.length && (spinner = $('<div style="left: 36%;" class="spinner' + (static_pos ? '' : ' spinner-absolute') + '"/>').appendTo($("#g-menu-overlay")));
            this.animateSpinner(spinner, 'add');
        },
    
        removeSpinnerMenu: function(complete){
            $("#g-menu-overlay").hide();
            var spinner = $("#g-menu-overlay").children('.spinner');
            spinner.length && this.animateSpinner(spinner, 'remove', complete);
        },

    }    

})(jQuery);
