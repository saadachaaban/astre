var OdeLog = OdeLog || {};
 
OdeLog = (function() {

    const Logger = (function() {

        var queue = [];
        var ECHO_TOKEN = {};
        var RESET_INPUT = "%c ";
        var RESET_CSS = "";

        return {

            // Attach formatting utility method.
            alertFormatting: function( value ) {
                queue.push({
                    value: value,
                    css: "display: inline-block ; background-color: #e0005a ; color: #ffffff ; font-weight: bold ; padding: 3px 7px 3px 7px ; border-radius: 3px 3px 3px 3px ;"
                });
                return( ECHO_TOKEN );
            },


            // Attach formatting utility method.
            successFormatting: function( value ) {
                queue.push({
                    value: value,
                    css: "display: inline-block ; background-color: #28A745 ; color: #ffffff ; font-weight: bold ; padding: 3px 7px 3px 7px ; border-radius: 3px 3px 3px 3px ;"
                });
                return( ECHO_TOKEN );
            },

            // Attach formatting utility method.
            warningFormatting: function( value ) {
                queue.push({
                    value: value,
                    css: "display: inline-block ; background-color: gold ; color: black ; font-weight: bold ; padding: 3px 7px 3px 7px ; border-radius: 3px 3px 3px 3px ;"
                });
                return( ECHO_TOKEN );
            },

            // I provide an echo-based proxy to the given Console Function. This uses an
            // internal queue to aggregate values before calling the given Console
            // Function with the desired formatting.
            using: function( consoleFunction ) {

                function consoleFunctionProxy() {

                    // As we loop over the arguments, we're going to aggregate a set of
                    // inputs and modifiers. The Inputs will ultimately be collapsed down
                    // into a single string that acts as the first console.log parameter
                    // while the modifiers are then SPREAD into console.log as 2...N.
                    // --
                    // NOTE: After each input/modifier pair, I'm adding a RESET pairing.
                    // This implicitly resets the CSS after every formatted pairing.
                    var inputs = [];
                    var modifiers = [];

                    for ( var i = 0 ; i < arguments.length ; i++ ) {

                        // When the formatting utility methods are called, they return
                        // a special token. This indicates that we should pull the
                        // corresponding value out of the QUEUE instead of trying to
                        // output the given argument directly.
                        if ( arguments[ i ] === ECHO_TOKEN ) {

                            var item = queue.shift();
                            inputs.push( ( "%c" + item.value ), RESET_INPUT );
                            modifiers.push( item.css, RESET_CSS );
                            
                        // For every other argument type, output the value directly.
                        } else {

                            var arg = arguments[ i ];

                            if (
                                ( typeof( arg ) === "object" ) ||
                                ( typeof( arg ) === "function" )
                                ) {

                                inputs.push( "%o", RESET_INPUT );
                                modifiers.push( arg, RESET_CSS );

                            } else {

                                inputs.push( ( "%c" + arg ), RESET_INPUT );
                                modifiers.push( RESET_CSS, RESET_CSS );

                            }

                        }

                    }

                    consoleFunction( inputs.join( "" ), ...modifiers );

                    // Once we output the aggregated value, reset the queue. This should have
                    // already been emptied by the .shift() calls; but the explicit reset
                    // here acts as both a marker of intention as well as a fail-safe.
                    queue = [];

                }

                return( consoleFunctionProxy );

            }
        }

    })();

    return {
        log: Logger.using( console.log ),
        warn: Logger.using( console.warn ),
        error: Logger.using( console.error ),
        trace: Logger.using( console.trace ),
        group: Logger.using( console.group ),
        groupEnd: Logger.using( console.groupEnd ),

        // Formatting functions.
        asAlert: Logger.alertFormatting,
        asWarning: Logger.warningFormatting,
        asSuccess: Logger.successFormatting
    }

})();



/*  ----------------------------------------------------------------------------------------------
------------------------------------------- Exemples ---------------------------------------------------
----------------------------------------------------------------------------------------------


// Let's try mixing a bunch of values together.
OdeLog.log(
    OdeLog.asAlert( "This is great!" ),
    "Right!",
    { "i am": "an object" },
    null,
    [ "an array" ],
    function amAFunction() {},
    OdeLog.asWarning( "I mean, right?!?!?!" ),
    OdeLog.asSuccess( " Yes success" )
);

OdeLog.log();

// Let's try a more sensible example.
OdeLog.group( OdeLog.asWarning( "Arnold Schwarzenegger Movies" ) );
OdeLog.log( "The Running Man" );
OdeLog.log( "Terminator 2", OdeLog.asAlert( "Amazing!" ), OdeLog.asWarning( "TOP 100" ) );
OdeLog.log( "Predator" );
OdeLog.log( "Twins", OdeLog.asWarning( "TOP 50" ) );
OdeLog.groupEnd();

OdeLog.log();

OdeLog.log( OdeLog.asAlert( "Winner Winner" ), "Chicken dinner!" );

*/