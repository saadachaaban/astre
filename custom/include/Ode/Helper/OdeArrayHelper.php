<?php

//namespace ODE\Helper;

class OdeArrayHelper
{

    /**
     * @access public
     * @name xxxxx()
     * Fonction qui xxxxxx
     *
     *  @param array            $xxx: xxxx
     *  @return string          $xxx: xxxx 
     */
    public static function searchForId($search_value, $array, $id_path)
    {
        // Iterating over main array 
        foreach ($array as $key1 => $val1) {
            $temp_path = $id_path;
            // Adding current key to search path 
            array_push($temp_path, $key1);
            // Check if this value is an array 
            // with atleast one element 
            if (is_array($val1) and count($val1)) {
                // Iterating over the nested array 
                foreach ($val1 as $key2 => $val2) {
                    if ($val2 == $search_value) {
                        // Adding current key to search path 
                        array_push($temp_path, $key2);
                        return join("->", $temp_path);
                    }
                }
            } elseif ($val1 == $search_value) {
                return join("->", $temp_path);
            }
        }
        return null;
    }

    /**
     * @access public
     * @name xxxxx()
     * Fonction qui xxxxxx
     *
     *  @param array            $xxx: xxxx
     *  @return string          $xxx: xxxx 
     */
    public static function array_orderby()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }

    /**
     * @access public
     * @name isDataValid()
     * Fonction qui vérifie les données recu dans request, retourne les erreurs ou les données 
     *
     *  @param array          - $request : $_REQUEST
     *  @param array          - $champs : les champs obligatoires
     *  @return array         - $data_verified [ 'statut' => 'ok'/'err' , 'data' => $data(array) / $erreurs(string) ]
     */
    public static function isDataValid($request, $champs)
    {

        $erreurs = array();
        $donnees = array();

        foreach ($champs as $champ => $is_obligatoire) {
            if (array_key_exists($champ, $request) !== false) {
                if ($is_obligatoire) {
                    if (!empty($request[$champ])) {
                        if ($champ == "json") {
                            $json_array = OdeArrayHelper::jsonToArray(base64_decode($request['json']));
                            if ($json_array['statut'] == 'ok') {
                                $donnees['json_array'] = $json_array['data'];
                            } else {
                                $erreurs[] = $json_array['data'];
                            }
                        } else {
                            $donnees[$champ] = $request[$champ];
                        }
                    } else {
                        $erreurs[] = ucfirst(str_replace("_", " ", $champ)) . " est vide. ";
                    }
                } else {
                    $donnees[$champ] = $request[$champ];
                }
            } else {
                $erreurs[] = ucfirst(str_replace("_", " ", $champ)) . " est innexistant. ";
            }
        }

        $data_verified['statut'] = (count($erreurs) == 0) ? 'ok' : 'err';
        $data_verified['data'] = (count($erreurs) == 0) ? $donnees : implode(",", $erreurs);

        return $data_verified;
    }

    public static function decodeJson($json, $is_array)
    {

        if (is_string($json)) {
            // $donnees = json_decode(urldecode($json), $is_array); Supprime le caractere + à revoir 
            $donnees = json_decode($json, $is_array);
            switch (json_last_error()) {
                case JSON_ERROR_NONE:
                    return array(true, $donnees);
                case JSON_ERROR_DEPTH:
                    return array(false, ' - Profondeur maximale atteinte');
                case JSON_ERROR_STATE_MISMATCH:
                    return array(false, ' - Inadéquation des modes ou underflow');
                case JSON_ERROR_CTRL_CHAR:
                    return array(false, ' - Erreur lors du contrôle des caractères');
                case JSON_ERROR_SYNTAX:
                    return array(false, ' - Erreur de syntaxe ; JSON malformé');
                case JSON_ERROR_UTF8:
                    return array(false, ' - Caractères UTF-8 malformés, probablement une erreusr d\'encodage');
                default:
                    return array(false, ' - Erreur inconnue');
            }
        } else {
            return array(false, ' - Format requete different de string ');
        }
    }

    /**
     * jsonToArray()
     * Fonction qui prend en param un json, retourne un tableau si ok sinon l'erreur Json
     * 
     *  @param Json/String $json        - Json
     *  @return Array  $array_return    - ['statut' => '', 'data' => '']
     */
    public static function jsonToArray($json)
    {

        // On décode le json , true pour un retour de type array 
        $json_decoded = OdeArrayHelper::decodeJson($json, true);

        // Si le json n'a pas pu etre décoder, on test un petit formatage avant de retourner une erreur
        $json_decoded = ($json_decoded[0] == false) ? OdeArrayHelper::decodeJson(str_replace('&quot;', '"', $json), true) : $json_decoded;

        return ($json_decoded[0] !== false) ? array('statut' => 'ok', 'data' =>  $json_decoded[1]) : array('statut' => 'err', 'data' =>  $json_decoded[1]);
    }

    //$GLOBALS['log']->fatal(" isDataValid :: champs = ".print_r($champs,true));

    public static function removeBOM($data)
    {
        if (0 === strpos(bin2hex($data), 'efbbbf')) {
            return substr($data, 3);
        }
        return $data;
    }

    public static function clean($jsonString)
    {
        if (!is_string($jsonString) || !$jsonString) return '';

        // Remove unsupported characters
        // Check http://www.php.net/chr for details
        for ($i = 0; $i <= 31; ++$i)
            $jsonString = str_replace(chr($i), "", $jsonString);

        $jsonString = str_replace(chr(127), "", $jsonString);

        // Remove the BOM (Byte Order Mark)
        // It's the most common that some file begins with 'efbbbf' to mark the beginning of the file. (binary level)
        // Here we detect it and we remove it, basically it's the first 3 characters.
        if (0 === strpos(bin2hex($jsonString), 'efbbbf')) $jsonString = substr($jsonString, 3);

        return $jsonString;
    }


    public static function getTempsFormated($seconds)
    {
        // '%02d:%02d:%02d'
        return sprintf('%02d heures %02d minutes %02d secondes', (round($seconds) / 3600), (round($seconds) / 60 % 60), round($seconds) % 60);
    }

    public static function dateToFrench($date, $format)
    {
        $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
        $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
        return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date))));
    }

    public static function formatString($string)
    {
        $string_formated = "";
        if (gettype($string) && !empty($string)) {
            // A mastered and complete dashed slugify | Implementation by Sebounet (Lanteas)
            $string_formated = strtoupper($string);
            if (class_exists('Transliterator')) {
                try {
                    $string_formated =  \Transliterator::create('NFD; [:Nonspacing Mark:] Remove; NFC')->transliterate($string_formated);
                } catch (Exception $e) {
                    if (class_exists('Normalizer')) {
                        try {
                            $string_formated = Normalizer::normalize($string_formated, Normalizer::NFD);
                        } catch (Exception $e) {
                            $string_formated = preg_replace('@[^\0-\x80]@u', "", $string_formated);
                        }
                    }
                }
            }
            $string_formated = trim($string_formated);
            $string_formated = preg_replace('/[-.]|(\s)/', "",  $string_formated);
            $string_formated = preg_replace('/[\']/', "",  $string_formated);
            $string_formated = preg_replace('/[^a-zA-Z0-9_]/', "_",  $string_formated);
            $string_formated = preg_replace('/([_]+)/', "_",  $string_formated);
            $string_formated = preg_replace('/(^[_]|[_]$)/', "",  $string_formated);
            $string_formated = strtolower($string_formated);
        }
        return $string_formated;
    }
}
