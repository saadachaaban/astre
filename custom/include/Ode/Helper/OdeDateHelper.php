<?php

namespace ODE\Helper;

use DateTime;

class OdeDateHelper
{

	public static function alter($date, $before, $after)
	{
		return DateTime::createFromFormat($before, $date)->format($after);
	}

	public static function toSQL($date)
	{

		$before = self::getFormat($date);
		if ($before === false) return "";
		$after = (strpos($before, "H:i") !== false) ? 'Y-m-d H:i' : 'Y-m-d';
		return self::alter($date, $before, $after);
	}

	public static function toHTML($date)
	{

		$before = self::getFormat($date);
		if ($before === false) return "";
		$after = (strpos($before, "H:i") !== false) ? 'd/m/Y H:i' : 'd/m/Y';
		return self::alter($date, $before, $after);
	}

	public static function validateDate($date, $format)
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}

	public static function getFormat($date)
	{
		if (self::validateDate($date, 'Y-m-d')) return 'Y-m-d';
		if (self::validateDate($date, 'd/m/Y')) return 'd/m/Y';
		if (self::validateDate($date, 'm/d/Y')) return 'm/d/Y';
		if (self::validateDate($date, 'Y-m-d H:i:s')) return 'Y-m-d H:i:s';
		if (self::validateDate($date, 'Y-m-d H:i')) return 'Y-m-d H:i';
		if (self::validateDate($date, 'd/m/Y H:i:s')) return 'd/m/Y H:i:s';
		if (self::validateDate($date, 'd/m/Y H:i')) return 'd/m/Y H:i';
		if (self::validateDate($date, 'm/d/Y H:i')) return 'm/d/Y H:i';

		$GLOBALS['log']->fatal("ODE/Helper/OdeDateHelper.php :: getFormat() => Le format de la date ( " . $date . " ) n'est pas pris en compte.");
		return false;
	}

	public static function getDateTimeNow()
	{
		$date = new DateTime('NOW');
		return $date->format('Y-m-d H:i:s');
	}
}
