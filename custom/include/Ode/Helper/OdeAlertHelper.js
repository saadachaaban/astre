var OdeAlertHelper = (function($) {
    return {
        set: function( element, statut, message ){
            if ( statut === "success" ){
                element.text(message);
                element.removeClass().addClass("col-md-9 alert alert-success");
                element.slideDown('normal');
            }
            if ( statut === "warning" ){
                element.text(message);
                element.removeClass().addClass("col-md-9 alert alert-warning");
                element.slideDown('normal');
            }
            if ( statut === "info" ){
                element.text(message);
                element.removeClass().addClass("col-md-9 alert alert-info");
                element.slideDown('normal');
            }
            if ( statut === "error" ){
                element.text(message);
                element.removeClass().addClass("col-md-9 alert alert-danger");
                element.slideDown('normal');
            }
            setTimeout(function(){ element.slideUp('slow'); }, 2000);
        },   
    }
})(jQuery);
