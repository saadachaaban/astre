String.prototype.utf8ToB64 = function() {
    return window.btoa(unescape(encodeURIComponent( this )));
};

Object.getLength = function(obj) {
    var size = 0,key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

var OdeAjax = (function ($) {

    return {

        getByAction: function(query, loading ,callback) {

            const self = this;
            const module     = query.getModule();
            const action     = query.getAction();
            const data       = query.getPostData();

            var data_formated = "&json=" + JSON.stringify(data).utf8ToB64();

            $.ajax({
                type: "POST",
                url: "index.php?module=" + module + "&action=" + action ,
                beforeSend: function() {
                    loading.apply();
                },
                data: data_formated
             
            }).done(function(dataReturned) {
                var result = self.formatReturnedData(dataReturned);
                callback.apply(result);
            }).fail(function() {
            
                console.log("OdeAjax::getByAction => fail");
            
            });

        },
        
        formatReturnedData: function(dataReturned) {
            var data = {
                statut: false,
                result: {}
            };
            
            dataReturned = ( this.isJson(dataReturned) ) ? JSON.parse(dataReturned) : dataReturned;
            if ( typeof dataReturned === "string" ){
                data.result = this.isAjaxErreur() ? "La ressource demandée n'existe pas" : dataReturned ;
            } else if ( typeof dataReturned === "object" && Object.getLength(dataReturned) > 0 ){
                if ( dataReturned["statut"] !== undefined && dataReturned["statut"] !== null && dataReturned["data"] !== undefined && dataReturned["data"] !== null ){
                    data.statut = (  dataReturned["statut"] === "ok" ) ? true : false;
                    data.result = dataReturned["data"];
                } else {
                    data.result = "Le format retour n'est conforme à ce qui est attendu";
                }
            } else {
                data.result = "La ressource demandée n'existe pas";
            }
            return data;
        },

        isAjaxErreur: function() {
            return false;
        },
    
        isJson: function(item) {

            item = typeof item !== "string"
                ? JSON.stringify(item)
                : item;
        
            try {
                item = JSON.parse(item);
            } catch (e) {
                return false;
            }
        
            if (typeof item === "object" && item !== null) {
                return true;
            }

            return false;
        }

    }

})(jQuery);
