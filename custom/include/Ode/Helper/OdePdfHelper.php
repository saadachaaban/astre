<?php

namespace ODE\Helper;

use TCPDF;
use BeanFactory;
use ODE\Model\DossierModel;
use ODE\Model\DispositifModel;
use ODE\Model\GenerateurFormulaireModel;
use ODE\Model\GenerateurVueModel;
use ODE\Generateur\Factory\OdeFieldFactory;

class OdePdfHelper extends TCPDF
{

    private $config;
    private $header;
    private $type_retour;
    private $dossier_id;
    private $config_path;
    private $data_retour;

    /**
     * Constructeur de la classe OdePDF
     *
     * @access public
     * @name __construct
     * @return void
     */
    public function __construct($dossier_id = null, $type_vue = null, $type_retour = null)
    {
        $this->config_path = "custom/include/Ode/Config/config_pdf.php";
        $this->config = $this->getConfig();
        $this->header = $this->getDossierData($dossier_id);
        $this->type_vue = $type_vue;
        $this->type_retour = $type_retour;
        parent::__construct(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false);
    }

    /**
     * @access private
     * @name getConfig()
     * Fonction qui retourne la configuration du pdf dynamique
     *
     *  @return array          $config: la configuration du pdf dynamique
     */
    private function getConfig()
    {
        if (file_exists($this->config_path)) {
            include $this->config_path;
            if (is_array($config) && count($config) > 0) {
                foreach (["titre", "onglet", "label", "champ"] as $param_title) {
                    $style = 'font-family:' . $config["police_pdf"] . ';';
                    foreach ($config[$param_title] as $parametre => $valeur) {
                        switch ($parametre) {
                            case 'couleur_pdf':
                                $style .= 'color:' . $valeur . ';';
                                break;
                            case 'taille_pdf':
                                $style .= 'font-size:' . $valeur . 'px;';
                                break;
                            case 'pdf_gras':
                                if ($valeur == 1) {
                                    $style .= 'font-weight: bold;';
                                }
                                break;
                            case 'pdf_italique':
                                if ($valeur == 1) {
                                    $style .= 'font-style: italic;';
                                }
                                break;
                            case 'pdf_souligner':
                                if ($valeur == 1) {
                                    $style .= 'text-decoration: underline;';
                                }
                                break;
                        }
                    }
                    $config[$param_title] = $style;
                }
                return $config;
            }
        }
        return false;
    }

    /**
     * @access private
     * @name getDossierData()
     * Fonction qui retourne les champs du dossier ( champs du formulaire associé au dispositif )
     *
     *  @param string            $dossier_id: L'id du dossier
     *  @return array            $header_data: Les données d'initilisation du Header
     */
    private function getDossierData($dossier_id)
    {

        $dossierModel = new DossierModel($dossier_id);
        $dossier = $dossierModel->getBean();

        $this->dossier_id = (is_array($dossier) && count($dossier) > 0) ? $dossier_id : false;

        $dossier_num = ($this->dossier_id !== false && !empty($dossier['num_dossier'])) ? $dossier['num_dossier'] : "";
        $dossier_name = ($this->dossier_id !== false && !empty($dossier['name'])) ? $dossier['name'] : "";
        $dossier_date = ($this->dossier_id !== false && !empty($dossier['date_entered'])) ? "Transmis le : " . $dossier['date_entered'] : "";
        $dispositif_name = ($this->dossier_id !== false) ? $dossierModel->getDispositifName() : "";
        $demandeur_name = ($this->dossier_id !== false) ? $dossierModel->getDemandeurName() : "";

        return [
            "dossier_num" => $dossier_num,
            "dossier_name" => $dossier_name,
            "dossier_date" => $dossier_date,
            "dispositif_name" => $dispositif_name,
            "demandeur_name" => $demandeur_name
        ];
    }

    /**
     * @access public
     * @name generer()
     * Fonction qui genere le document PDF
     *
     */
    public function generer()
    {

        $this->data_retour = ["statut" => false, "data" => [], "erreur" => "Erreur Serveur"];

        /*
        $GLOBALS['log']->fatal(" OdePdfHelper :: genererPDF => " . print_r( array(
            "dossier_id" => $this->dossier_id,
            "type_retour" => $this->type_retour,
            "config" => $this->config,
            "type_vue" => $this->type_vue,
            "header" => $this->header
        ), true ) );
        */

        do {

            ob_start();

            // On vérifie qu'on a bien récupérer les parametres de configuration
            if ($this->config === false) {
                $GLOBALS['log']->fatal(" OdePdfHelper :: generer => Erreur de récupération de la config du PDF dynamique ");
                break;
            }

            // On vérifie que l'id du dossier n'est pas null ou empty
            if ($this->dossier_id === false) {
                $GLOBALS['log']->fatal(" OdePdfHelper :: generer => Le dossier à télécharger est introuvable ");
                break;
            }

            // On récupére les champs du dossier avec leurs valeurs ( champs du formulaire associé au dispositif )
            $onglets = $this->getOngletDossier();
            if (!is_array($onglets) || count($onglets) === 0) {
                $GLOBALS['log']->fatal(" OdePDF :: generer => Les onglets du formulaire n'ont pas pu etre récupérer ");
                break;
            }

            // Initialiser les parametres du PDF
            $this->setPdfParams();

            // On construit le corps du PDF
            $this->setPdfCorps($onglets);

            // On sort le format selon le type_retour
            $file_location = $_SERVER['DOCUMENT_ROOT'] . '/upload/dossier_' . $this->header['dossier_num'] . '.pdf';

            switch ($this->type_retour) {
                case 'base64':
                    $this->Output($file_location, 'F');
                    $file = base64_encode(file_get_contents($file_location));
                    $this->data_retour = ["statut" => true, "data" => ["name" => 'dossier_' . $this->header['dossier_num'] . '.pdf', "base64" => $file, "size" => filesize($file_location)], "erreur" => ""];
                    unlink($file_location);
                    break;
                case 'download':
                    $file = $this->Output('dossier_' . $this->header['dossier_num'] . '.pdf', 'D');
                    $this->data_retour = ["statut" => true, "data" => ["file" => $file], "erreur" => ""];
                case 'mail':
                    $file = $this->Output('', 'S');
                    $this->data_retour = ["statut" => true, "data" => ["file" => $file], "erreur" => ""];
                case 'file':
                    $file = $this->Output($file_location, 'I');
                    $this->data_retour = ["statut" => true, "data" => ["file" => $file], "erreur" => ""];
                default:
                    $file = $this->Output('', 'S');
                    break;
            }
        } while (0);

        return $this->data_retour;
    }



    /**
     * @access private
     * @name getOngletDossier()
     * Fonction qui retourne les champs du dossier ( champs du formulaire associé au dispositif )
     *
     *  @return array            $champs_dossier: champs du formulaire associé au dispositif
     */
    private function getOngletDossier()
    {
        $onglets = [];

        do {

            $dossierModel = new DossierModel($this->dossier_id);
            $dispositif_id = $dossierModel->getDispositifId();
            if (empty($dispositif_id)) {
                $GLOBALS['log']->fatal(" OdePdfHelper :: getChampsDossier => Le dispositif n'a pas pu etre récupéré. ");
                break;
            }

            $dispositifModel = new DispositifModel($dispositif_id);
            $dispositif = $dispositifModel->getBean();
            if (!is_array($dispositif) || count($dispositif) === 0) {
                $GLOBALS['log']->fatal(" OdePdfHelper :: getChampsDossier => Le dispositif id = " . $dispositif_id . " est introuvable");
                break;
            }

            // On récupere la vue agent du formulaire associé au dispositif
            $formulaire_id = $dispositifModel->getFormulaireId();
            $formulaireModel = new GenerateurFormulaireModel($formulaire_id);
            $vue_id = ($this->type_vue === "agent") ? $formulaireModel->getVueAgentId() : $formulaireModel->getVueUsagerId();
            if (empty($vue_id)) {
                $GLOBALS['log']->fatal(" OdePdfHelper :: getChampsDossier => La vue '" . $this->type_vue . "' id='" . $vue_id . "' associé formulaire id = " . $formulaire_id . " est introuvable");
                break;
            }

            $vueModel = new GenerateurVueModel($vue_id);
            $vue = $vueModel->getBean();
            if (!is_array($vue) || count($vue) === 0) {
                $GLOBALS['log']->fatal(" OdePdfHelper :: getChampsDossier => La id = " . $vue_id . " est introuvable");
                break;
            }

            // On récupere les onglets de la vue 
            $onglets = $vueModel->getOnglets();
        } while (0);

        return $onglets;
    }


    /**
     * @access private
     * @name _initParamsPDF()
     * Fonction qui initialise les parametres du PDF
     *
     */
    private function setPdfParams()
    {
        $margin_header = 5;
        $margin_footer = 10;
        $interligne = 1.05;
        $titre =  mb_strtoupper($this->header['dossier_name'], 'UTF-8');

        // set document information
        $this->SetCreator($this->config["createur"]);
        $this->SetAuthor($this->config["createur"]);
        $this->SetTitle(str_replace("&#039;", "'", $titre));

        // set header and footer fonts
        $this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $this->SetMargins($this->config["margin"]["left"], $this->config["margin"]["top"], $this->config["margin"]["right"]);
        $this->SetHeaderMargin($margin_header);
        $this->SetFooterMargin($margin_footer);

        //set auto page breaks
        $this->SetAutoPageBreak(TRUE, $this->config["margin"]["bottom"]);

        //set image scale factor
        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);
        //set some language-dependent strings
        //$pdf->setLanguageArray($l);
        // set font
        $this->SetFont($this->config["police_pdf"], '', 11);

        //définiton de l'interligne
        $this->setCellHeightRatio($interligne);
        //Ajout d'une page
        $this->AddPage();
    }


    /**
     * @access private
     * @name setPdfCorps()
     * Fonction qui retourne les champs du dossier ( champs du formulaire associé au dispositif )
     *
     *  @return array            $champs_dossier: champs du formulaire associé au dispositif
     */
    private function setPdfCorps($onglets)
    {

        $corps_html = "";
        $obj_dossier = BeanFactory::getBean("OPS_dossier", $this->dossier_id);
        $fieldFactory = new OdeFieldFactory($obj_dossier, false, []);
        foreach ($onglets as $onglet) {
            $corps_html .= '<span style="' . $this->config['onglet'] . '"><br>' . $onglet["libelle"] . ' </span><br />';
            $corps_html .= '<br />';
            $corps_html .= '<table cellspacing="4" cellpadding="3" >';
            if (is_array($onglet["lignes"]) && count($onglet["lignes"]) > 0) {
                foreach ($onglet["lignes"] as $ligne) {
                    if ($ligne["type"] === "ligne_double_champ") {
                        if ($this->config['in_colonnes'] == "1") {
                            $corps_html .= '<tr>';
                            if (is_array($ligne["champs"][0]) && count($ligne["champs"][0]) > 0) {
                                $corps_html .= '<td><span style="' . $this->config['label'] . '">' . $ligne["champs"][0]["libelle"] . ' : </span></td>';
                                $corps_html .= '<td bgcolor="#c3e7fd"><span style="' . $this->config['champ'] . '">' . $fieldFactory->getChampPdfValue($ligne["champs"][0]) . '</span></td>';
                            }
                            if (is_array($ligne["champs"][1]) && count($ligne["champs"][1]) > 0) {
                                $corps_html .= '<td><span style="' . $this->config['label'] . '">' . $ligne["champs"][1]["libelle"] . ' : </span></td>';
                                $corps_html .= '<td bgcolor="#c3e7fd"><span style="' . $this->config['champ'] . '">' . $fieldFactory->getChampPdfValue($ligne["champs"][1]) . '</span></td>';
                            }
                            $corps_html .= '</tr>';
                        } else {
                            if (is_array($ligne["champs"][0]) && count($ligne["champs"][0]) > 0) {
                                $corps_html .= '<tr>';
                                $corps_html .=      '<td><span style="' . $this->config['label'] . '">' . $ligne["champs"][0]["libelle"] . ' : </span></td>';
                                $corps_html .=      '<td bgcolor="#c3e7fd"><span style="' . $this->config['champ'] . '">' . $fieldFactory->getChampPdfValue($ligne["champs"][0]) . '</span></td>';
                                $corps_html .= '</tr>';
                            }
                            if (is_array($ligne["champs"][1]) && count($ligne["champs"][1]) > 0) {
                                $corps_html .= '<tr>';
                                $corps_html .=      '<td><span style="' . $this->config['label'] . '">' . $ligne["champs"][1]["libelle"] . ' : </span></td>';
                                $corps_html .=      '<td bgcolor="#c3e7fd"><span style="' . $this->config['champ'] . '">' . $fieldFactory->getChampPdfValue($ligne["champs"][1]) . '</span></td>';
                                $corps_html .= '</tr>';
                            }
                        }
                    } else if ($ligne["type"] === "section") {
                        $corps_html .= '<tr>';
                        $corps_html .= '<td colspan="5" style="' . $this->config["titre"] . 'border-bottom: 1pt solid black;" >'.$ligne['title'].'</td>';
                        $corps_html .= '</tr>';
                    }
                }
            }
            $corps_html .= '</table>';
        }

        $this->writeHTML($corps_html, true, false, true, false, '');
    }


    /****************************************************************************************************************************************/
    /*********************************************************** Surcharge TCPDF ***********************************************************/
    /****************************************************************************************************************************************/

    /**
     * @access public
     * @name Header()
     * Fonction qui construit le header du pdf  ( Surcharge fonction TCPDF::Header )
     *
     */
    public function Header()
    {

        $table =    '<table>
                        <tr>
                            <td colspan="2"><img height="60px" width="auto" src="' . $this->config["logo_chemin"] . '" align="left"></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td colspan="2"><span style="font-size:12px;text-align:right;">' . $this->header["dossier_date"] . ' </span> </td>
                        </tr>
                        <tr>
                            <td ></td>
                            <td colspan="5" style="text-align: center;padding-bottom:10px;"> 
                                <span style="' . $this->config["titre"] . '" >' .  $this->header["dispositif_name"] . '</span> <br />
                                <span style="' . $this->config["titre"] . '" > 
                                    Dossier N° ' . $this->header["dossier_num"] . '<br />
                                </span> 
                                <span style="' . $this->config["titre"] . '" >' .  $this->header["demandeur_name"] . '</span> 
                            </td>
                            <td ></td>
                        </tr>
                    </table>';

        //Ecriture du tableau dans l'êntete
        $this->writeHTML($table, true, false, true, false, '');
    }

    /**
     * @access public
     * @name Footer()
     * Fonction qui construit le footer du pdf  ( Surcharge fonction TCPDF::Footer )
     *
     */
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}
