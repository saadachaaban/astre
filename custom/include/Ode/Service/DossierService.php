<?php

require_once 'custom/include/Ode/Helper/OdeArrayHelper.php';
require_once 'custom/include/Ode/Service/IndividuService.php';
require_once 'custom/include/Ode/Service/PersonneMoraleService.php';

class DossierService
{

    static function get($dossier_id)
    {
        global $db;
        $dossier = [];
        if (!empty($dossier_id)) {
            $sql = "SELECT * FROM `ops_dossier` WHERE `id` = '" . $dossier_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $dossier = $row;
            }
            if (is_array($dossier) && count($dossier) > 0) {
                $dossier['module_type'] = 'OPS_dossier';
            }
            if (!empty($dossier['champs_custom'])) {
                $champs_custom_json_retour = OdeArrayHelper::jsonToArray(base64_decode($dossier['champs_custom']));
                if ($champs_custom_json_retour['statut'] === "ok") {
                    $champs_custom = $champs_custom_json_retour['data'];
                    if (is_array($champs_custom) && count($champs_custom) > 0) {
                        $dossier = array_merge($dossier, $champs_custom);
                        unset($dossier['champs_custom']);
                    }
                } else {
                    $GLOBALS['log']->fatal("DossierService::get() =>  Les champs custom du dossier id = " . $dossier_id . " n'ont pas été récupérés ( " . $champs_custom_json_retour['data'] . " ) ");
                }
            }
        } else {
            $GLOBALS['log']->fatal("DossierService::get() => L'id du dossier à récupérer est vide");
        }
        return (is_array($dossier) && count($dossier) > 0) ? (object) $dossier : false;
    }

    static function getDemandeur($dossier_id, $demandeur_type = '')
    {
        global $db;
        $demandeur = false;

        do {

            if (empty($dossier_id)) break;

            if (empty($demandeur_type) || !in_array($demandeur_type, ['Individu', 'Personne Morale'])) {
                $dossier = DossierService::get($dossier_id);
                $demandeur_type = ($dossier->type_tiers) ? $dossier->type_tiers : '';
            }

            switch ($demandeur_type) {
                case 'Personne Morale':
                    $sql_personne_morale_id = "SELECT `ops_personne_morale_id` FROM `ops_personne_morale_ops_dossier` WHERE `ops_dossier_id` = '" . $dossier_id . "' AND `deleted` = '0' ";
                    $query_personne_morale_id = $db->query($sql_personne_morale_id);
                    $result_personne_morale_id = $db->fetchByAssoc($query_personne_morale_id);
                    if (!empty($result_personne_morale_id['ops_personne_morale_id'])) {
                        $demandeur = PersonneMoraleService::get($result_personne_morale_id['ops_personne_morale_id']);
                    }
                    break;
                case 'Individu':
                    $sql_individu_id = "SELECT `ops_individu_id` FROM `ops_individu_ops_dossier` WHERE `ops_dossier_id` = '" . $dossier_id . "' AND `deleted` = '0' ";
                    $query_individu_id = $db->query($sql_individu_id);
                    $result_individu_id = $db->fetchByAssoc($query_individu_id);
                    if (!empty($result_individu_id['ops_individu_id'])) {
                        $demandeur = IndividuService::get($result_individu_id['ops_individu_id']);
                    }
                    break;
            }
        } while (0);

        return $demandeur;
    }
}
