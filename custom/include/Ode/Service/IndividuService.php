<?php

require_once 'custom/include/Ode/Service/DomiciliationService.php';

class IndividuService
{

    static function get($individu_id)
    {
        global $db;
        $individu = [];
        if (!empty($individu_id)) {
            $sql = "SELECT * FROM `ops_individu` WHERE `id` = '" . $individu_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $individu = $row;
            }
            if (is_array($individu) && count($individu) > 0) {
                $individu['module_type'] = 'OPS_individu';
                $individu['name_display'] = $individu['salutation'] . ' ' . $individu['first_name'] . ' ' . $individu['last_name'];
            }
        } else {
            $GLOBALS['log']->fatal("IndividuService::get() => L'id de l'individu à récupérer est vide");
        }
        return (is_array($individu) && count($individu) > 0) ? (object) $individu : false;
    }

    static function getAppairage($individu_id, $appairage_type = '', $appairage_statut = '')
    {
        global $db;
        $appairage = false;
        if (!empty($individu_id)) {
            $sql = "SELECT * FROM `ops_appairage` WHERE `parent_id` = '" . $individu_id . "' AND `parent_type` = 'OPS_individu' ";
            $sql .= (!empty($appairage_type)) ? " AND `logiciel` = '" . $appairage_type . "' " : '';
            $sql .= (!empty($appairage_statut)) ? " AND `statut` = '" . $appairage_statut . "' " : '';
            $sql .= " AND `deleted` = '0' ";
            $query = $db->query($sql);
            $appairage = $db->fetchByAssoc($query);
        } else {
            $GLOBALS['log']->fatal("IndividuService::getAppairage() => L'id de l'individu est vide");
        }
        return (is_array($appairage) && count($appairage) > 0) ? (object) $appairage : false;
    }

    static function getDomiciliations($individu_id = '')
    {
        global $db;
        $domiciliations = [];
        if (!empty($individu_id)) {
            $sql = "SELECT ops_domiciliation_id FROM `ops_individu_ops_domiciliation` WHERE `ops_individu_id` = '" . $individu_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $domiciliation = DomiciliationService::get($row['ops_domiciliation_id']);
                if ($domiciliation !== false) {
                    $domiciliations[] = $domiciliation;
                }
            }
        } else {
            $GLOBALS['log']->fatal("IndividuService::getDomiciliations() => L'id de l'individu est vide");
        }
        return (is_array($domiciliations) && count($domiciliations) > 0) ? $domiciliations : false;
    }
}
