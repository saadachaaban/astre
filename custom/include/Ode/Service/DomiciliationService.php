<?php

class DomiciliationService
{

    static function get($domiciliation_id)
    {
        global $db;
        $domiciliation = [];
        if (!empty($domiciliation_id)) {
            $sql = "SELECT * FROM `ops_domiciliation` WHERE `id` = '" . $domiciliation_id . "' AND `deleted` = '0' ";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) {
                $domiciliation = $row;
            }
            if (is_array($domiciliation) && count($domiciliation) > 0) {
                $domiciliation['module_type'] = 'OPS_domiciliation';
                $domiciliation['name_display'] = preg_replace('~[^A-Za-z0-9]~', '', $domiciliation['bic']) . '-' . preg_replace('~[^A-Za-z0-9]~', '', $domiciliation['iban']);
            }
        } else {
            $GLOBALS['log']->fatal("DomiciliationService::get() => L'id de l'domiciliation à récupérer est vide");
        }
        return (is_array($domiciliation) && count($domiciliation) > 0) ? (object) $domiciliation : false;
    }

    static function getAppairage($domiciliation_id, $appairage_type = '', $appairage_statut = '')
    {
        global $db;
        $appairage = false;
        if (!empty($domiciliation_id)) {
            $sql = "SELECT * FROM `ops_appairage` WHERE `parent_id` = '" . $domiciliation_id . "' AND `parent_type` = 'OPS_domiciliation' ";
            $sql .= (!empty($appairage_type)) ? " AND `logiciel` = '" . $appairage_type . "' " : '';
            $sql .= (!empty($appairage_statut)) ? " AND `statut` = '" . $appairage_statut . "' " : '';
            $sql .= " AND `deleted` = '0' ";
            $query = $db->query($sql);
            $appairage = $db->fetchByAssoc($query);
        } else {
            $GLOBALS['log']->fatal("DomiciliationService::getAppairage() => L'id de l'domiciliation est vide");
        }
        return (is_array($appairage) && count($appairage) > 0) ? (object) $appairage : false;
    }
}
