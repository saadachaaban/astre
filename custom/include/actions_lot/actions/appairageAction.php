<?php

if (!defined('sugarEntry')) define('sugarEntry', true);

class AppairageAction
{

    /**
     * @access public
     * @name getAction()
     * Fonction qui retourne l'action " Lancer l'appairage "
     * 
     *  @return array       - $action
     */
    public function getAction()
    {

        return array(
            'module_name' => 'OPS_dossier',
            'action_content' => $this->getActionContent(),
            'action_link' => $this->getActionLink(),
        );
    }

    /**
     * @access public
     * @name getActionOrdre()
     * Fonction qui retourne l'ordre de l'action
     * 
     *  @return array       - $action_ordre
     */
    public function getActionOrdre()
    {
        return 5;
    }

    /**
     * @access private
     * @name getActionContent()
     * Fonction qui retourne le content de l'action
     * 
     *  @return array       - $action_content
     */
    private function getActionContent()
    {
        return $this->getActionLink() . $this->getActionHTML();
    }

    /**
     * @access private
     * @name getActionLink()
     * Fonction qui retourne le link de l'action
     * 
     *  @return array       - $action_link
     */
    private function getActionLink()
    {
        return '<a role="open-modal-appairage" > Appairage des tiers et des domiciliations</a>';
    }

    /**
     * @access private
     * @name getActionHTML()
     * Fonction qui retourne l'html de l'action
     * 
     *  @return array       - $action_html
     */
    private function getActionHTML()
    {
        return file_get_contents('custom/modules/OPS_dossier/tpls/appairage.tpl');
    }
}
