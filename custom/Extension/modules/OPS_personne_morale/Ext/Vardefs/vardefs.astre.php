<?php

$dictionary["OPS_personne_morale"]["fields"]["raison_sociale_astre"] = array(
  'required' => false,
  'name' => 'raison_sociale_astre',
  'vname' => 'LBL_RAISON_SOCIALE_ASTRE',
  'type' => 'varchar',
  'massupdate' => 0,
  'no_default' => false,
  'comments' => '',
  'help' => '',
  'importable' => 'true',
  'duplicate_merge' => 'disabled',
  'duplicate_merge_dom_value' => '0',
  'audited' => true,
  'inline_edit' => true,
  'reportable' => true,
  'unified_search' => false,
  'merge_filter' => 'disabled',
  'len' => '255',
  'size' => '20',
);
