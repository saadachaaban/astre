<?php

$manifest = [
    0 => [
        'acceptable_sugar_versions' => [
            0 => '6.5.25'
        ]
    ],
    1 => [
        'acceptable_sugar_flavors' => [
            0 => 'CE',
            1 => 'PRO',
            2 => 'ENT'
        ]
    ],
    'readme' => '',
    'key' => 'ODE',
    'author' => 'LANTEAS',
    'description' => "Module complémentaire : Connecteur Astre",
    'icon' => '',
    'is_uninstallable' => true,
    'name' => "LANTEAS - Connecteur Astre",
    'published_date' => '2021-12-01 10:00:00',
    'type' => 'module',
    'version' => '1.0DEV5',
    'remove_tables' => 'false'
];

$installdefs = [

    'id' => 'connecteur_astre',

    'administration' => [
        [
            'from' => '<basepath>/custom/modules/Administration/astre_administration.php'
        ]
    ],

    'scheduledefs' => [
        [
            'from' => '<basepath>/custom/modules/Schedulers/appairage.php',
        ],
    ],

    'copy' => [
        [
            'from' => '<basepath>/custom/modules/Administration',
            'to' => 'custom/modules/Administration',
        ],
        [
            'from' => '<basepath>/custom/modules/OPS_dossier',
            'to' => 'custom/modules/OPS_dossier',
        ],
        [
            'from' => '<basepath>/custom/modules/Administration',
            'to' => 'custom/modules/Administration',
        ],
        [
            'from' => '<basepath>/custom/modules/Users',
            'to' => 'custom/modules/Users',
        ],
        [
            'from' => '<basepath>/custom/include',
            'to' => 'custom/include',
        ],
        [
            'from' => '<basepath>/SugarModules/modules/OPS_exercice/controller.php',
            'to' => 'modules/OPS_exercice/controller.php',
        ],
        [
            'from' => '<basepath>/SugarModules/modules/OPS_dispositif/controller.php',
            'to' => 'modules/OPS_dispositif/controller.php',
        ],
        [
            'from' => '<basepath>/custom/modules/OPS_exercice/controller.php',
            'to' => 'custom/modules/OPS_exercice/controller.php',
        ],
        [
            'from' => '<basepath>/custom/modules/OPS_dispositif/controller.php',
            'to' => 'custom/modules/OPS_dispositif/controller.php',
        ],
        [
            'from' => '<basepath>/custom/modules/OPS_statut/controller.php',
            'to' => 'custom/modules/OPS_statut/controller.php',
        ],
        [
            'from' => '<basepath>/SugarModules/modules/OPS_dossier',
            'to' => 'modules/OPS_dossier',
        ],
        [
            'from' => '<basepath>/SugarModules/modules/OPS_generateur_vue',
            'to' => 'modules/OPS_generateur_vue',
        ],
        [
            'from' => '<basepath>/custom/Extension/modules/OPS_personne_morale/Ext/Language',
            'to' => 'custom/Extension/modules/OPS_personne_morale/Ext/Language',
        ],
        [
            'from' => '<basepath>/custom/Extension/modules/OPS_personne_morale/Ext/Vardefs',
            'to' => 'custom/Extension/modules/OPS_personne_morale/Ext/Vardefs',
        ],
        [
            'from' => '<basepath>/SugarModules/modules/OPS_journal_detail',
            'to' => 'modules/OPS_journal_detail',
        ],
    ],

    'language' => [
        [
            'from' => '<basepath>/custom/modules/Administration/language/admin_astre.fr_FR.php',
            'to_module' => 'Administration',
            'language' => 'fr_FR'
        ],
        [
            'from' => '<basepath>/custom/modules/Schedulers/language/fr_FR.lang.php',
            'to_module' => 'Schedulers',
            'language' => 'fr_FR'
        ],
        [
            'from' => '<basepath>/custom/modules/Schedulers/language/fr_FR.lang.php',
            'to_module' => 'Schedulers',
            'language' => 'en_us'
        ],
    ],

];
