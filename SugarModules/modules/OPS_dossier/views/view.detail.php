<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once 'custom/include/helpers/ode_helper.php';
require_once 'custom/include/helpers/ode_date.php';
require_once 'custom/include/ode_generateur/OdeFieldFactory.php';
require_once 'custom/include/Astre/Entity/DossierEntity.php';

class OPS_dossierViewDetail extends ViewDetail
{

	public $erreurs;
	public $dossierEntity;

	function OPS_dossierViewDetail()
	{
		parent::ViewDetail();
	}

	function display()
	{



		global $mod_strings, $app_strings, $app_list_strings, $beanFiles, $current_user;

		$this->dossierEntity = new DossierEntity($this->bean->id);

		if (empty($this->bean->id)) {
			sugar_die($app_strings['ERROR_NO_RECORD']);
		}

		$smarty = new Sugar_Smarty();

		// On initialise un tableau d'erreurs
		$this->erreurs = array();

		// Initialisation de l'id et name du dossier à afficher
		if (!empty($this->bean->id)) {

			$dossier_id = $this->bean->id;
			$dossier_offset_id = intval($_GET['offset']);

			if (!empty($this->bean->name)) {

				$dossier_name = $this->bean->name;
				$dossier_num_dossier = $this->bean->num_dossier;

				// Barre de commande dossier
				$dossier_boutons = $this->getLinksVueDossier($this->bean);
				$dossier_barre_boutons = implode('', array_values($dossier_boutons));


				$dossier_ligne_workflow = $this->getLigneWorkflowVueDossier($this->bean);

				// On récupere les onglets associés à la vue dossier
				$onglets = $this->bean->getOnglets();


				if ($onglets !== false) {
					// Initialisation des boutons et des layouts onglets 
					$onglets_boutons = $this->getInitBoutonsOnglets($onglets);
					$onglets_layouts = $this->getInitLayoutsOnglets($onglets);
				} else {
					$this->erreurs[] = "Les onglets associé à la vue dossier n'ont pas pu être récupéré.";
				}
			} else {
				$this->erreurs[] = "Le nom du dossier n'a pas pu être récupéré.";
			}
		} else {
			$this->erreurs[] = "Le dossier n'a pas pu être récupéré.";
		}

		// Si on a aucune erreur => on affiche la vue détail du dossier sinon on affiche une page d'erreur avec la liste des erreurs
		if (is_array($this->erreurs) && count($this->erreurs) == 0) {

			$current_template = "modules/OPS_dossier/tpls/detail.tpl";
			$smarty->assign("dossier_id", $dossier_id);
			$smarty->assign("dossier_offset", $dossier_offset_id);
			$smarty->assign("dossier_name",  $dossier_name);
			$smarty->assign("dossier_boutons",  $dossier_barre_boutons);
			$smarty->assign("dossier_ligne_workflow",  $dossier_ligne_workflow);
			$smarty->assign("onglets_boutons", $onglets_boutons);
			$smarty->assign("onglets_layouts", $onglets_layouts);



			// Rempli la popup modale pour le lancement de flux...

			$obj_flux = new OPS_flux;
			$liste_flux = $obj_flux->get_full_list();
			$options_flux = "";

			$data_flux = array();
			foreach ($liste_flux as $key => $flux) {
				if (!empty($flux->id)) {
					$options_flux .= '<option value="' . $flux->id . '">' . $flux->name . '</option>';
					$data_flux[$flux->id]['id'] = $flux->id;
					$data_flux[$flux->id]['name'] = $flux->name;
					$data_flux[$flux->id]['description'] = $flux->description;
				}
			}

			if (is_array($data_flux)) {
				if (count($data_flux) > 0) {
					$data_flux_json = json_encode($data_flux);
				} else {
					$data_flux_json = json_encode(['erreur' => 'Les flux n\'ont pas pu etre récupérés.']);
				}
			} else {
				$data_flux_json = json_encode(['erreur' => 'Les données récupérées ne sont pas au bon format.']);
			}

			$content_hidden_data_flux = '<textarea style="display:none;" id="data_flux" />' . $data_flux_json . '</textarea>';

			$tr_select_flux = '<tr style="width: 100%;">
	                                <td style="color: rgb(83, 77, 100); width: 20%; font-weight: bold; font-size: 14px; font-family: &quot;Lato&quot;, Lato, Arial, sans-serif; padding-top: 19px;">Séléctionnez un flux :</td>
	                                <td colspan="3" class="modal_td_input">
	                                    <select name="select_flux" id="select_flux" style="height:40px; float:left; margin-right: 5px; " onchange="changement_description()">' . $options_flux . '</select>
	                                </td>
	                            </tr>';
			$smarty->assign("TR_SELECT_FLUX", $tr_select_flux);
			$smarty->assign("DATA_FLUX", $content_hidden_data_flux);
			$smarty->assign("TITRE_MODAL", "Lancer un flux sur le dossier n°" . $dossier_num_dossier);
		} else {

			$current_template = "custom/include/helpers/erreurs/erreur.tpl";
			$smarty->assign("erreurs",  $this->erreurs);
		}

		// On display la vue
		$smarty->display($current_template);
	}

	/**
	 * @access private
	 * @name getLinksVueDossier()
	 * Fonction qui retourne les boutons de la vue détail du dossier 
	 * 
	 *  @param array			$dossier : Bean OPS_dossier
	 *  @return string    $dossier_boutons_html : code html , les boutons de la vue détail
	 */
	private function getLinksVueDossier($dossier)
	{

		global $current_user;

		// TODO : Contraindre ou étendre selon les habilitations !
		$dossier_html_boutons = array();


		$dossier_html_boutons['edition']       = $this->getBtnModifierDossier($dossier); // On initialise le bouton modifier le dossier 
		$dossier_html_boutons['suppression']   = $this->getBtnSupprimerDossier($dossier); // On initialise le bouton supprimer le dossier 
		$dossier_html_boutons['instruction']   = $this->getBtnLancerFluxDossier(); // On initialise le bouton lancer flux 
		$dossier_html_boutons['pdf_dynamique']   = $this->getBtnGenererPdfDossier(); // On initialise le bouton générer le pdf
		$dossier_html_boutons['change_status'] = $this->getBtnModifierStatutDossier($dossier); // On initialise le bouton lancer flux 
		$dossier_html_boutons['date_status']   = $this->getBtnAfficheStatutDossier($dossier); // On initialise le bouton lancer flux 


		// On négocie les éléments contenus dans la barre de commande OPS_dossier.
		// Les habilitations (rôles et privilèges) permettent la distribution des éléments à afficher...
		//
		// L'adminitrateur peut quant à lui disposer de l'ensemble des éléments, les autres utilisateurs sont contitionnés.

		if (!$current_user->isAdmin()) {

			$dossier_html_boutons['visualisation'] = $this->getCodeRedirectDossier(); // On initialise le code visualiser le dossier 

			$habilitation_dispositif = $_SESSION['habilitation_dispositif'];

			# Vérification de l'habilitation sur le dispositif associé
			if (!isset($habilitation_dispositif[$dossier->ops_dispositif_id])) {

				$privilege = array(
					'visualisation' => false,
					'instruction'   => false,
					'edition'       => false,
					'suppression'   => false
				);
			} else {

				// Définit les privilèges par défaut au niveau du dispositif.
				$privilege = array(
					'visualisation' => boolval(intval($habilitation_dispositif[$dossier->ops_dispositif_id]['visualisation'])),
					'instruction'   => boolval(intval($habilitation_dispositif[$dossier->ops_dispositif_id]['instruction'])),
					'edition'       => boolval(intval($habilitation_dispositif[$dossier->ops_dispositif_id]['edition'])),
					'suppression'   => boolval(intval($habilitation_dispositif[$dossier->ops_dispositif_id]['suppression']))
				);
				// (re)Définit les privilèges surchargés prévalant au niveau étape courant pour ce dispositif.
				if (isset($habilitation_dispositif[$dossier->ops_dispositif_id]['etapes'][$dossier->ops_etape_id])) {

					$privilege = array(
						'visualisation' => $privilege['visualisation'] |= boolval(intval($habilitation_dispositif[$dossier->ops_dispositif_id]['etapes'][$dossier->ops_etape_id]['visualisation'])),
						'instruction'   => $privilege['instruction'] |= boolval(intval($habilitation_dispositif[$dossier->ops_dispositif_id]['etapes'][$dossier->ops_etape_id]['instruction'])),
						'edition'      => $privilege['edition'] |= boolval(intval($habilitation_dispositif[$dossier->ops_dispositif_id]['etapes'][$dossier->ops_etape_id]['edition'])),
						'suppression'   => $privilege['suppression'] |= boolval(intval($habilitation_dispositif[$dossier->ops_dispositif_id]['etapes'][$dossier->ops_etape_id]['suppression']))
					);
				}
			}

			$dossier_html_boutons['visualisation'] = ($privilege['visualisation']) ? '' : $dossier_html_boutons['visualisation']; // reverse
			$dossier_html_boutons['edition']       = ($privilege['edition']) ?      $dossier_html_boutons['edition']    : '';
			$dossier_html_boutons['suppression']   = ($privilege['suppression']) ?   $dossier_html_boutons['suppression']   : '';
			$dossier_html_boutons['instruction']   = ($privilege['instruction']) ?   $dossier_html_boutons['instruction']   : '';
			$dossier_html_boutons['change_status'] = ($privilege['instruction']) ?   $dossier_html_boutons['change_status'] : '';
			$dossier_html_boutons['pdf_dynamique'] = ($privilege['instruction']) ?   $dossier_html_boutons['pdf_dynamique'] : '';
		}


		return $dossier_html_boutons;
	}

	/**
	 * @access private
	 * @name getCodeRedirectDossier()
	 * Fonction qui génére un script JS de redirection si pas de visualisation du dossier
	 * 
	 *  @param bean    $dossier : objet OPS_dossier initialisé en amont
	 *  @return string    $dossier_html_boutons['visualisation'] : code html , script js
	 */
	private function getCodeRedirectDossier()
	{
		return '<script type="text/javascript" charset="UTF-8">self.location.href = "/index.php?module=OPS_dossier&action=index&parentTab=DOssiers";</script>';
	}


	/**
	 * @access private
	 * @name getBtnModifierDossier()
	 * Fonction qui génére le bouton modifier le dossier
	 * 
	 *  @param bean    $dossier : objet OPS_dossier initialisé en amont
	 *  @return string    $dossier_html_boutons['modify'] : code html , bouton modifier
	 */
	private function getBtnModifierDossier($dossier)
	{
		return (!empty($dossier->id)) ? '<input title="Modifier le dossier" 
				class="button" 
				type="button" 
				value="Modifier" 
				onclick="location.href=\'index.php?module=OPS_dossier&action=modifier&id_dossier=' . $dossier->id . '\'">' : '';
	}

	/**
	 * @access private
	 * @name getBtnSupprimerDossier()
	 * Fonction qui génére le bouton supprimer le dossier
	 * 
	 *  @param bean    $dossier : objet OPS_dossier initialisé en amont
	 *  @return string    $dossier_html_boutons['remove'] : code html , bouton supprimer
	 */
	private function getBtnSupprimerDossier($dossier)
	{
		return (!empty($dossier->id)) ? '<input title="Supprimer le dossier" 
				class="button"
				type="button" 
				value="Supprimer"  
				onclick="deleteDossier()" >' : '';
	}

	/**
	 * @access private
	 * @name getBtnLancerFluxDossier()
	 * Fonction qui génére le bouton lancer flux
	 * 
	 *  @return string    $dossier_html_boutons['trigger'] : code html , bouton lancer flux
	 */
	private function getBtnLancerFluxDossier()
	{
		return '<input title="Déclencher un traitement" 
				class="button" 
				type="button" 
				value="Lancer un flux" 
				onclick="openModalLancerFluxUnitaire()">';
	}

	/**
	 * @access private
	 * @name getBtnGenererPdfDossier()
	 * Fonction qui génére le bouton lancer flux
	 * 
	 *  @return string    $dossier_html_boutons['trigger'] : code html , bouton lancer flux
	 */
	private function getBtnGenererPdfDossier()
	{

		global $sugar_config;
		return '<input 
					type="button"
					class="button" 
					href="#"  
					title="Télécharger le dossier en PDF" 
					onClick="window.location = \'' . $sugar_config['site_url'] . '/index.php?entryPoint=generer_pdf&dossier_id=' . $this->bean->id . '\'; " 
					value="PDF" />';
	}



	/**
	 * @access private
	 * @name getBtnModifierStatutDossier()
	 * Fonction qui génére le select statut + bouton modifier statut + affichage statut courant
	 * 
	 *  @param bean    $dossier : objet OPS_dossier initialisé en amont
	 *  @return string    $dossier_html_boutons['change_status'] : code html , bouton modifier statut
	 */
	private function getBtnModifierStatutDossier($dossier)
	{

		// Récupération de la liste des statuts pour le Workflow 
		$obj_statut = new OPS_statut();
		$obj_statut->retrieve($dossier->ops_statut_id);

		$liste_wf = $obj_statut->liste_statuts($dossier);


		$select_actions = '<select name="action_id" id="action_id" onchange="onChangeSelectStatut()" >';
		$select_actions .= '<option value="disabled" disabled="disabled" selected="selected" > Séléctionner un statut </option>';
		foreach ($liste_wf as $statut_id => $statut_name) {

			if ("undefined" != $statut_id) {

				$select_actions .= '<option value="' . $statut_id . '" >' . $statut_name . '</option>';
			}
		}

		$select_actions .= "</select>";



		$str_liste_actions = '<div style="display: inline-block;" role="group-buttons">' . $select_actions;

		if (!empty($liste_wf)) {

			$str_liste_actions .= '<input title="Modifier le statut" 
										class="button"
										type="button"
										onclick="execute(\'editStatut\',\'OPS_dossier\')"
										name="workflow_bouton"
										value="Modifier le statut" >';
		}

		$str_liste_actions .= "</div>";


		return $str_liste_actions;
	}


	/**
	 * @access private
	 * @name getBtnAfficheStatutDossier()
	 * Fonction qui génére le libellé du statut courant et sa date
	 * 
	 *  @param bean    $dossier : objet OPS_dossier initialisé en amont
	 *  @return string    $dossier_html_boutons['date_status'] : code html , block statut et date
	 */
	private function getBtnAfficheStatutDossier($dossier)
	{

		// Récupération de la liste des statuts pour le Workflow 
		$obj_statut = new OPS_statut();
		$obj_statut->retrieve($dossier->ops_statut_id);

		$statut_courant = $obj_statut->name;
		$date_avancement = $dossier->date_avancement;

		$str_liste_actions = "";


		// Ajout du statut en plus 
		if (!empty($statut_courant)) {

			$str_liste_actions .= "<div class='info-label'>" . $statut_courant . " : " . $date_avancement . "</div>";
		}

		return $str_liste_actions;
	}


	/**
	 * @access private
	 * @name getLinksDossier()
	 * Fonction qui génére les boutons d'onglets 
	 * 
	 *  @param bean       $dossier : objet OPS_dossier initialisé en amont
	 *  @return string      $btn_onglets_html : code html , les boutons sont groupés dans une div
	 */
	private function getLigneWorkflowVueDossier($dossier)
	{

		// Récupération du dispositif lié au dossier pour récupérer le guide d'instruction
		$dispositifs = $dossier->get_linked_beans('ops_dispositif_ops_dossier', 'OPS_dispositif');
		$dispositif = (is_array($dispositifs) && count($dispositifs) == 1) ? $dispositifs[0] : false;

		// Récupération du guide d'instruction
		$guide_instructions = ($dispositif !== false) ? $dispositif->get_linked_beans('ops_guide_instruction_ops_dispositif', 'OPS_guide_instruction') : array();
		$guide_instruction = (is_array($guide_instructions) && count($guide_instructions) == 1) ? $guide_instructions[0] : false;

		// Récupération de la liste des étapes du guide d'instruction
		$liste_des_etapes = ($guide_instruction !== false) ? $guide_instruction->liste_etapes() : array();

		// Récupération de l'étape courante pour ne pas afficher les étapes suivantes ou on aurait pu revenir en arrière
		$etape_courante = BeanFactory::getBean('OPS_etape', $dossier->ops_etape_id);

		// Récupération des status passés 
		$tab_historiques = $dossier->get_linked_beans('ops_historisation_ops_dossier', 'OPS_historisation', '', '', '', '', "flag_retour = '1'");
		$tabhisto = array("Tableau historisation");


		$str_workflow = ""; // Init HTML Workflow code.


		$OPS_dossier_history = array();
		$dossier_tab_history = $tab_historiques;
		//$dossier_tab_history = array_reverse($tab_historiques);

		foreach ($dossier_tab_history as $key => $historisation) {

			$_history = array(
				'id'			=> $historisation->id,
				'date_entered'  => $historisation->date_entered,
				'date_modified' => $historisation->date_modified,
				'ops_statut_id' => $historisation->ops_statut_id,
				'statut'		=> $historisation->statut,
				'ops_etape_id'  => $historisation->ops_etape_id,
				'etape'      => $historisation->etape
			);


			$step_pointer = $historisation->ops_etape_id;
			$OPS_dossier_history[$step_pointer] = $_history;
		}

		$str_workflow .= '<div class="align-items-center d-flex justify-content-between" role="dossier-workflow">';


		$total_steps  = count($liste_des_etapes);
		$focused_step = 0;
		$has_picto  = false;
		$finished    = false;
		$completed  = false;
		$decorate    = true;
		foreach ($liste_des_etapes as $k => $value) {

			$extra = '';


			$obj_statut = BeanFactory::getBean('OPS_statut', $dossier->ops_statut_id);

			$current_step = ($value['id'] == $dossier->ops_etape_id) ? true : false;

			$step_status_name  = $OPS_dossier_history[$value['id']]['statut'];

			$step_statut_id  = $OPS_dossier_history[$value['id']]['ops_statut_id'];
			$step_obj_statut    = BeanFactory::getBean('OPS_statut', $step_statut_id);
			$step_code_couleur  = (!empty($step_obj_statut->id)) ? $step_obj_statut->code_couleur : 'rgba(0, 0, 0, 0.1)';
			$phase_code_couleur  = (!empty($step_obj_statut->id)) ? $step_obj_statut->code_couleur : 'rgba(0, 0, 0, 0.05)';


			$step_date_modified = $OPS_dossier_history[$value['id']]['date_modified'];

			$step_coloration  = ' style="background-color:' . $step_code_couleur . ';" ';
			$phase_coloration = ' style="background-color:' . $phase_code_couleur . ';" ';

			// TODO ! Do not decorate phase until step is not effective passed.
			#   $dossier->ops_etape_id
			#   $dossier->ops_statut_id

			if ($decorate) { /* Allows CSS coloration and Label status  */
			} else { /* no longer labelized */
				$step_coloration  = '';
				$step_status_name = '';
			}


			if ($current_step /*current step ?*/) {

				if ($has_picto) {
					$extra .= ' current fa fa-archive ';
					$has_picto = false;
					$decorate = false;
					$phase_coloration = '';
				} else {

					if ($obj_statut->avancement == 'suivant') {
						$has_picto  = true;
						$decorate = false;
					} elseif ($obj_statut->avancement == 'courant') {
						$has_picto  = false;
						$extra .= ' current fa fa-archive ';
						$decorate = false;
						$phase_coloration = '';
					} else { // 'termine'
						$has_picto  = false;
						$extra .= ' current  fa fa-archive ';
						$finished   = true;
						$decorate = false;
						if ($focused_step >= $total_steps - 1) {
							$extra .= ' successful ';
						} else {
							$extra .= ' premature ';
							$phase_coloration = '';
						}
					}
				}
			} else {
				if ($has_picto) {
					$extra .= ' current fa fa-archive ';
					$has_picto = false;
					$decorate = false;
					$phase_coloration = '';
				}
			}


			$str_workflow .= '<div class="step-circle ' . $extra . '" ' . $step_coloration . ' data-step-id="' . $value['id'] . '" ><span>' . $value['name'] . '</span></div>';
			$str_workflow .= '<div class="flex-grow step-phase" data-step-name="' . $value['name'] . '" ' . $phase_coloration . ' ><label>' . $value['name'] . '</label></div>';

			$focused_step++;
			if ($current_step && $focused_step >= $total_steps /* last step in workflow */) {
				$finished = true;
				if ($obj_statut->avancement == 'termine') {
					$complete = true;
				}
			}
		}


		// The final workflow process. [ Premature Failed | Failed Finalized (complete) | Success (complete) ]
		if ($complete /*success ends ?*/) {
			$extra = 'fa fa-check success';
		} else {
			$extra = ' fa fa-times ';
			if ($finished /* last step in workflow */) {
				$extra .= ' failed ';
			}
		}
		if ($finished /* last step in workflow */) {
			$extra .= ' current ';
		}

		// End
		$str_workflow .= '<div class="step-circle ' . $extra . '"></div>';


		// / [role="dossier-workflow"]
		$str_workflow .= '</div>';


		return $str_workflow;
	}


	/**
	 * @access private
	 * @name getInitBoutonsOnglets()
	 * Fonction qui génére les boutons d'onglets 
	 * 
	 *  @param array			$onglets : la liste des onglets rattaché à la vue
	 *  @return string      $btn_onglets_html : code html , les boutons sont groupés dans une div
	 */
	private function getInitBoutonsOnglets($onglets)
	{

		$btn_onglets_html = "";
		$count = 0;
		foreach ($onglets as $ordre => $onglet) {
			$btn_onglets_html .= '<div class="div_button_onglet" >';
			$btn_onglets_html .=    '<div role="onglet" id="btn_onglet_' . $onglet['id'] . '" title="' . $onglet['libelle'] . '" data-id="' . $onglet['id'] . '" data-libelle="' . $onglet['libelle'] . '" data-cle="' . $onglet['cle'] . '" onclick="displayOnglet(\'' . $onglet['id'] . '\')" ';
			$btn_onglets_html .=    ($count == 0) ? 'class="btn_onglet btn_onglet_selected" data-selected="1" >' : 'class="btn_onglet btn_onglet_not_selected" data-selected="0" >';
			$btn_onglets_html .=        '<div class="div_libelle_button_onglet" id="titre_onglet_' . $onglet['id'] . '">';
			$btn_onglets_html .=           $onglet['libelle'];
			$btn_onglets_html .=        '</div>';
			$btn_onglets_html .=    '</div>';
			$btn_onglets_html .= '</div>';
			$count++;
		}
		return $btn_onglets_html;
	}

	/**
	 * @access private
	 * @name getInitLayoutsOnglets()
	 * Fonction qui génére les layouts d'onglets, en d'autre terme le container qui contient les lignes  
	 * 
	 *  @param array			$onglets : la liste des onglets rattaché à la vue
	 *  @return string      $layout_onglets_html : code html , une div par layout, le premier est affiché par défaut
	 */
	private function getInitLayoutsOnglets($onglets)
	{



		$layout_onglets_html = "";
		$count = 0;
		foreach ($onglets as $ordre => $onglet) {
			$layout_onglets_html .= '<div id="layout_onglet_' . $onglet['id'] . '" class="div_layout_onglet" ';
			if ($count == 0) {

				$layout_onglets_html .= 'style="display:block;" >';
			} else {
				$layout_onglets_html .= 'style="display:none;" >';
			}
			$layout_onglets_html .= '<table class="table_layout_onglet">';
			$layout_onglets_html .= '<tbody id="tbody_layout_onglet_' . $onglet['id'] . '"> ';
			$layout_onglets_html .= $this->getLignesOnglet($onglet);
			$layout_onglets_html .= '</tbody>';
			$layout_onglets_html .= '</table>';
			$layout_onglets_html .= '</div>';
			$count++;
		}

		return $layout_onglets_html;
	}


	/**
	 * @access private
	 * @name getLignesOnglet()
	 * Fonction qui génere les lignes d'un onglet
	 *
	 *  @param array			$onglet: les informations de l'onglet sous forme de tableau
	 *  @return string      $lignes: retour les lignes et les champs qu'ils contiennent sous forme html 
	 */
	private function getLignesOnglet($onglet)
	{

		$lignes = '';
		if (is_array($onglet['champs']) && count($onglet['champs']) > 0) {
			foreach ($onglet['champs'] as $num_ligne => $ligne) {

				if (is_array($ligne)) {
					ksort($ligne);
				}

				$lignes .= '<tr>';
				foreach ($ligne as $pos => $champ) {

					if ($this->isValidChamp($champ) === true) {
						if ($champ['type'] == "vide") {
							$lignes .= '<td class="td_layout_onglet"></td>';
						} else {
							$lignes .= '<td class="td_layout_onglet" >';
							$lignes .= ($champ['type'] !== "liste_choix_multi") ? '<div id="champ_' . $champ['name'] . '" class="champ_referentiel  champ_panel">' : '<div style="display:flex;" id="champ_' . $champ['name'] . '" class="champ_referentiel  champ_panel">';
							$lignes .= ($champ['type'] !== 'astre') ? '<div id="titre_champ_' . $champ['name'] . '" style="white-space: initial;width:25%;height: 24px;">' . $champ['libelle'] . ':</div>' : '';
							$lignes .= $this->getChampHtml($champ);
							$lignes .= '</div>';
							$lignes .= '</td>';
						}
					}
				}
				$lignes .= '</tr>';
			}
		} else {
			$lignes .= '<tr>';
			$lignes .= '<td class="td_layout_onglet"></td>';
			$lignes .= '<td class="td_layout_onglet"></td>';
			$lignes .= '</tr>';
		}

		return $lignes;
	}

	/**
	 * @access private
	 * @name getChampHtml()
	 * Fonction qui génere les lignes d'un onglet
	 *
	 *  @param array			$onglet: les informations de l'onglet sous forme de tableau
	 *  @return string      $lignes: retour les lignes et les champs qu'ils contiennent sous forme html 
	 */
	private function getChampHtml($champ)
	{

		$html = false;
		$default_html = '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" disabled/>';

		switch ($champ['type']) {

			case 'text': {
					$html = $this->getChampText($champ);
					break;
				}
			case 'text_long': {
					$html = $this->getChampTextLong($champ);
					break;
				}
			case 'relation': {
					$html = $this->getChampRelation($champ);
					break;
				}
			case 'adresse': {
					$html = $this->getChampAdresse($champ);
					break;
				}
			case 'lien': {
					$html = $this->getChampLien($champ);
					break;
				}
			case 'pourcentage': {
					$html = $this->getChampPourcentage($champ);
					break;
				}
			case 'bouton_radio': {
					$html = $this->getChampBoutonRadio($champ);
					break;
				}
			case 'checkbox': {
					$html = $this->getChampCheckbox($champ);
					break;
				}
			case 'chaine_libre': {
					$html = $this->getChampChaineLibre($champ);
					break;
				}
			case 'date': {
					$html = $this->getChampDate($champ);
					break;
				}
			case 'liste': {
					$html = $this->getChampListe($champ);
					break;
				}
			case 'liste_choix_simple': {
					$html = $this->getChampListeChoixSimple($champ);
					break;
				}
			case 'liste_choix_multi': {
					$html = $this->getChampListeChoixMulti($champ);
					break;
				}
			case 'numerique': {
					$html = $this->getChampNumerique($champ);
					break;
				}
			case 'telephone': {
					$html = $this->getChampTelephone($champ);
					break;
				}
			case 'montant': {
					$html = $this->getChampMontant($champ);
					break;
				}
			case 'wysiwyg': {
					$html = $this->getChampWysiwyg($champ);
					break;
				}
			case 'astre': {
					$html = $this->getChampAstre($champ);
					break;
				}
		}

		return ($html !== false) ? $html : $default_html;
	}

	/**
	 * @access private
	 * @name getChampRelation()
	 * Fonction qui génere l'affichage des champs de type relation
	 *
	 *  @param array			$champ: Champ de type "relation"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampRelation($champ)
	{


		$champ_html = false;
		$array_champs = array("assigned_user_id", "created_by", "modified_user_id");
		$value = (in_array($champ['name'], $array_champs)) ? "" : $this->getValueChamp($champ['name'], $champ['description']);
		$json_formated = base64_decode($champ['params']);
		$retour_json_to_array = ODE::jsonToArray($json_formated);

		if ($retour_json_to_array['statut'] == "ok") {
			if (in_array($champ['name'], $array_champs)) {

				if ($champ['name'] === "assigned_user_id") {
					$enregistrement_id = $this->bean->assigned_user_id;
					$enregistrement_name = $this->bean->assigned_user_name;
				}
				if ($champ['name'] === "created_by") {
					$enregistrement_id = $this->bean->created_by;
					$enregistrement_name = $this->bean->created_by_name;
				}
				if ($champ['name'] === "modified_user_id") {
					$enregistrement_id = $this->bean->modified_user_id;
					$enregistrement_name = $this->bean->modified_by_name;
				}
				if (!empty($enregistrement_id) &&  !empty($enregistrement_name)) {
					$enregistrements[0] = array(
						'id' => $enregistrement_id,
						'name' => $enregistrement_name
					);
				} else {
					$enregistrements = array();
				}
				$data_module = array(
					'unique' => 'unique',
					'type' => '',
					'condition' => '',
				);
				$module = array(
					'module_name' => 'Users',
					'enregistrements' => $enregistrements,
					'nb_enregistrement' => count($enregistrements),
				);
			} else {
				$data_module = $retour_json_to_array['data'];
				$module = $this->getRelatedModule($data_module['module_name'], $data_module['relation_name'], $data_module['unique'], $data_module['type'], $data_module['condition'],  $value);
			}

			if ($data_module['unique'] == 'unique') {
				if ($module['nb_enregistrement'] > 0) {
					// $link = $data_module['lien']; IF LINK == oui 
					if ($data_module['lien'] == 'oui') {
						if (!empty($module['module_name']) && !empty($module['enregistrements'][0]['id']) && !empty($module['enregistrements'][0]['name'])) {
							$champ_html  = '<div class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block; font-weight: unset; ';
							$champ_html  .= (strlen($module['enregistrements'][0]['name']) > 57) ? 'height: 65px;' : '';
							$champ_html  .= '">';
							$champ_html  .= '   <a href="index.php?module=' . $module['module_name'] . '&amp;action=DetailView&amp;record=' . $module['enregistrements'][0]['id'] . '">
													<span id="span_champ_' . $champ['name'] . '" class="sugar_field">' . $module['enregistrements'][0]['name'] . '</span>
												</a>
											</div>';
						}
					} else {
						$champ_html = '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="' . $module['enregistrements'][0]['name'] . '" disabled/>';
					}
				}
			} else {

				if ($data_module['lien'] == 'oui') {
					$content = "";
					foreach ($module['enregistrements'] as $enregistrement) {
						$content .= '<a href="index.php?module=' . $module['module_name'] . '&amp;action=DetailView&amp;record=' . $enregistrement['id'] . '">
										<span id="span_champ_' . $champ['name'] . '" style="display: -webkit-box;-webkit-line-clamp: 3; -webkit-box-orient: vertical;" class="sugar_field">' . $enregistrement['name'] . '</span>

									</a>';
					}
				} else {
					$content = "";
					foreach ($module['enregistrements'] as $enregistrement) {
						$content .= $enregistrement['name'] . "<br>";
					}
				}

				$height_compl = ($module['nb_enregistrement'] > 0) ? ($module['nb_enregistrement'] - 1) * 20 : 0;
				$height = 45 + $height_compl;

				$champ_html  = '<div class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block; font-weight: unset;line-height: 150%; height:' . $height . 'px; ">';
				$champ_html  .= $content;
				$champ_html  .= '</div>';
			}
		}


		return $champ_html;
	}

	/**
	 * @access private
	 * @name getRelatedModuleTable()
	 * Fonction qui génere l'affichage des champs de type relation
	 *
	 *  @param array			$champ: Champ de type "relation"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getRelatedModuleTable($module_name, $relation_name, $unique)
	{

		$retour_enregistrements = array();
		$enregistrements =  $this->bean->get_linked_beans($relation_name, $module_name);

		if (is_array($enregistrements) && count($enregistrements) > 0) {
			if ($unique === true) {
				if (!empty($enregistrements[0]->id) && !empty($enregistrements[0]->name)) {
					$retour_enregistrements[0] = array(
						"id" => $enregistrements[0]->id,
						"name" => $enregistrements[0]->name,
					);
				}
			} else {
				foreach ($enregistrements as $key => $enregistrement) {
					if (!empty($enregistrements[$key]->id) && !empty($enregistrements[$key]->name)) {
						$retour_enregistrements[$key] = array(
							"id" => $enregistrements[$key]->id,
							"name" => $enregistrements[$key]->name,
						);
					}
				}
			}
		}

		return $retour_enregistrements;
	}

	/**
	 * @access private
	 * @name getRelatedModuleParentID()
	 * Fonction qui génere l'affichage des champs de type relation
	 *
	 *  @param array			$champ: Champ de type "relation"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getRelatedModuleParentID($module_name, $enregistrement_id)
	{


		$enregistrement = array();
		$module_liste = array_intersect($GLOBALS['moduleList'], array_keys($GLOBALS['beanList']));
		if (in_array($module_name, $module_liste, TRUE)) {
			$obj_module = BeanFactory::getBean($module_name, $enregistrement_id);
			if (!empty($obj_module->id)) {
				$enregistrement[0] = array(
					"id" => $obj_module->id,
					"name" => $obj_module->name,
				);
			} else {
				if ($enregistrement_id == "1" || $enregistrement_id == 1) {
					$enregistrement[0] = array(
						"id" => "1",
						"name" => "Administrator",
					);
				}
			}
		} else {
			$GLOBALS['log']->fatal(" view.detail.php :: getRelatedModuleParentID() => Module $module_name non déclaré dans la liste des modules.");
		}
		return $enregistrement;
	}

	/**
	 * @access private
	 * @name getRelatedModule()
	 * Fonction qui ... 
	 *
	 *  @param unknow		   $_module_name: unknow
	 *  @param unknow		   $_relation_name: unknow
	 *  @param unknow		   $unique: unknow
	 *  @param unknow		   $type: unknow
	 *  @param unknow		   $condition: unknow
	 *  @param unknow		   $champ_value: unknow
	 *  @return array       
	 */
	private function getRelatedModule($_module_name, $_relation_name, $unique, $type, $condition, $champ_value)
	{

		$retour_enregistrements = array();

		if ($type == "table") {
			if (!empty($condition)) {
				if (!empty($this->bean->$condition)) {
					foreach ($_module_name as $module) {
						if ($module['value'] == $this->bean->$condition) {
							$module_name = $module['name'];
						}
					}
					foreach ($_relation_name as $relation) {
						if ($relation['value'] == $this->bean->$condition) {
							$relation_name = $relation['name'];
						}
					}
					$retour_enregistrements = $this->getRelatedModuleTable($module_name, $relation_name, $unique);
				} else {
					$GLOBALS['log']->fatal(" view.detail.php :: getRelatedModule() => champs référentiel le champ $condition du dossier est null ");
				}
			} else {
				$module_name = $_module_name;
				$relation_name = $_relation_name;
				$retour_enregistrements = $this->getRelatedModuleTable($_module_name, $_relation_name, $unique);
			}
		}

		if ($type == "parent_id" && !empty($champ_value)) {
			// Avec l'id le parent un seul enregistrement est retourné
			$module_name = $_module_name;
			$retour_enregistrements = $this->getRelatedModuleParentID($_module_name, $champ_value);
		}

		$module = array(
			"module_name" => $module_name,
			"enregistrements" => $retour_enregistrements,
			"nb_enregistrement" => ($unique === true) ? 1 : count($retour_enregistrements),
		);
		return $module;
	}

	/**
	 * @access private
	 * @name getChampTextLong()
	 * Fonction qui génere l'affichage des champs de type text
	 *
	 *  @param array			$champ: Champ de type "text"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampTextLong($champ)
	{
		$champ_html = false;
		$name = $champ['name'];
		$value = $this->getValueChamp($champ['name'], $champ['description']);

		if (!empty($name) && $value !== false) {
			$champ_html = '<textarea class="ode_input_hidden" id="value_champ_avis_service" style="display: block;height: 11em;resize: none;overflow-y: scroll;" disabled>';
			$champ_html .= html_entity_decode($value);
			$champ_html .= '</textarea>';
		}
		return $champ_html;
	}

	/**
	 * @access private
	 * @name getChampWysiwyg()
	 * Fonction qui génere l'affichage des champs de type text
	 *
	 *  @param array			$champ: Champ de type "text"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampWysiwyg($champ)
	{
		$champ_html = false;
		$name = $champ['name'];
		$value = $this->getValueChamp($champ['name'], $champ['description']);

		if (!empty($name) && $value !== false) {
			$champ_html = '<div id="value_champ_' . $champ['name'] . '" class="ode_input_hidden" style="display: block;height: 11em;resize: none;overflow-y: scroll;" disabled>';
			$value = preg_replace('/\s|-/i', '+', $value);
			$champ_html .= utf8_encode(base64_decode($value));
			$champ_html .= '</div>';
		}
		return $champ_html;
	}

	/**
	 * @access private
	 * @name getChampLien()
	 * Fonction qui génere l'affichage des champs de type text
	 *
	 *  @param array			$champ: Champ de type "text"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampLien($champ)
	{

		$champ_html = false;
		$libelle_lien = "Cliquez ici pour visualiser";
		$value = $this->getValueChamp($champ['name'], $champ['description']);

		if (!empty($champ['name']) && $value !== false) {
			$json_formated = base64_decode($champ['params']);
			$retour_json_to_array = ODE::jsonToArray($json_formated);
			if ($retour_json_to_array['statut'] == "ok") {
				$params = $retour_json_to_array['data'];
				if (!empty($params['libelle_lien'])) {
					$libelle_lien = $params['libelle_lien'];
				}
			}
			$value_display = (strpos($value, "http") === false && strpos($value, "//") === false) ? "//" . $value : $value;
		}

		$champ_html  = '<div class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block; font-weight: unset; ">';
		if (!empty($value_display) && $value_display !== "//") {
			$champ_html  .= '  <a target="_blank" rel="noopener noreferrer" href="' . $value_display . '">
									<span id="span_champ_' . $champ['name'] . '" class="sugar_field">' . $libelle_lien . '</span>
								</a>';
		}
		$champ_html  .= '</div>';

		return $champ_html;
	}

	/**
	 * @access private
	 * @name getChampPourcentage()
	 * Fonction qui génere l'affichage des champs de type text
	 *
	 *  @param array			$champ: Champ de type "text"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampPourcentage($champ)
	{
		$champ_html = false;

		$value = $this->getValueChamp($champ['name'], $champ['description']);
		$pourcentage = number_format(floatval($value), 2, ',', ' ');

		if ((float)$pourcentage > 0) {
			$champ_html = '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="' . $pourcentage . ' %" disabled/>';
		} elseif ((float)$pourcentage > 100) {
			$champ_html = '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="Erreur" disabled/>';
		} else {
			$champ_html = '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="0,00 %" disabled/>';
		}

		return $champ_html;
	}

	/**
	 * @access private
	 * @name getChampText()
	 * Fonction qui génere l'affichage des champs de type text
	 *
	 *  @param array			$champ: Champ de type "text"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampText($champ)
	{

		$champ_html = false;
		$name = $champ['name'];
		$value = $this->getValueChamp($champ['name'], $champ['description']);
		if (!empty($name) && $value !== false) {
			$champ_html = '<input class="ode_input_hidden" id="value_champ_' . $name . '" style="display:block;" value="' . html_entity_decode($value) . '" disabled/>';
		}
		return $champ_html;
	}

	/**
	 * @access private
	 * @name getChampMontant()
	 * Fonction qui génere l'affichage des champs de type adresse
	 *
	 *  @param array			$champ: Champ de type "adresse"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampMontant($champ)
	{

		$champ_html = false;

		$value = $this->getValueChamp($champ['name'], $champ['description']);
		$montant = number_format(floatval($value), 2, ',', ' ');

		if ((float)$montant > 0) {
			$champ_html = '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="' . $montant . ' €" disabled/>';
		} else {
			$champ_html = '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="0,00 €" disabled/>';
		}

		return $champ_html;
	}


	/**
	 * @access private
	 * @name getChampAdresse()
	 * Fonction qui génere l'affichage des champs de type adresse
	 *
	 *  @param array			$champ: Champ de type "adresse"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampAdresse($champ)
	{
		return false;
	}

	/**
	 * @access private
	 * @name getChampBoutonRadio()
	 * Fonction qui génere l'affichage des champs de type bouton_radio
	 *
	 *  @param array			$champ: Champ de type "bouton_radio"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampBoutonRadio($champ)
	{
		return false;
	}

	/**
	 * @access private
	 * @name getChampCaseCocher()
	 * Fonction qui génere l'affichage des champs de type case_cocher
	 *
	 *  @param array			$champ: Champ de type "case_cocher"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampCheckbox($champ)
	{


		$champ_html = false;
		$value = $this->getValueChamp($champ['name'], $champ['description']);
		$checked = ($value == "0") ? '' : ' checked="checked" ';


		if (!empty($champ['name'])) {
			$champ_html   = '<div class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block; font-weight: unset; ">';
			$champ_html  .= '<input type="checkbox" ' . $checked . ' disabled="disabled">';
			$champ_html  .= '</div>';
		}
		return $champ_html;
	}

	/**
	 * @access private
	 * @name getChampChaineLibre()
	 * Fonction qui génere l'affichage des champs de type chaine_libre
	 *
	 *  @param array			$champ: Champ de type "chaine_libre"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampChaineLibre($champ)
	{
		return false;
	}

	/**
	 * @access private
	 * @name getChampDate()
	 * Fonction qui génere l'affichage des champs de type date
	 *
	 *  @param array			$champ: Champ de type "date"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampDate($champ)
	{

		// On récupère la valeur de la date stockée en base de données
		$value = $this->getValueChamp($champ['name'], $champ['description']);

		// Si non vide, on formate la date au format HTML sinon vide
		$value_display = (!empty($value)) ? OdeDate::toHTML($value) : "";

		// On retourne un input de type text avec la date formatée
		$champ_html = '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="' . $value_display . '" disabled/>';

		return $champ_html;
	}

	/**
	 * @access private
	 * @name getChampListe()
	 * Fonction qui génere l'affichage des champs de type choix_simple
	 *
	 *  @param array			$champ: Champ de type "choix_simple"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampListe($champ)
	{

		global $app_list_strings;
		$champ_html = false;

		$value = $this->getValueChamp($champ['name'], $champ['description']);
		$json_formated = base64_decode($champ['params']);
		$retour_json_to_array = ODE::jsonToArray($json_formated);

		if ($retour_json_to_array['statut'] == "ok") {
			$params = $retour_json_to_array['data'];
			if (!empty($params['liste_name'])) {
				$liste_name = $params['liste_name'];
				if (array_key_exists($liste_name, $app_list_strings) && !empty($value)) {
					$liste = $app_list_strings[$liste_name];
					if (is_array($liste) && count($liste) > 0) {
						$liste_value = $liste[$value];
					}
				}
			}
		}

		if (isset($liste_value) && !empty($liste_value)) {
			$champ_html = '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="' . $liste_value . '" disabled/>';
		}

		return $champ_html;
	}
	/**
	 * @access private
	 * @name getChampListeChoixMulti()
	 * Fonction qui génere l'affichage des champs de type choix_simple
	 *
	 *  @param array			$champ: Champ de type "choix_simple"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampListeChoixMulti($champ)
	{
		$champ_value = $this->getValueChamp($champ['name'], $champ['description']);
		$fieldFactory = new OdeFieldFactory(false);
		return $fieldFactory->getHtml($champ, $champ_value);
	}
	/**
	 * @access private
	 * @name getChampListeChoixSimple()
	 * Fonction qui génere l'affichage des champs de type choix_simple
	 *
	 *  @param array			$champ: Champ de type "choix_simple"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampListeChoixSimple($champ)
	{

		global $app_list_strings;
		$champ_html = false;

		$value = $this->getValueChamp($champ['name'], $champ['description']);
		$json_formated = base64_decode($champ['params']);
		$retour_json_to_array = ODE::jsonToArray($json_formated);

		if ($retour_json_to_array['statut'] == "ok") {
			$params = $retour_json_to_array['data'];
			if (!empty($params['liste'])) {
				$liste = $params['liste'];
				if (is_array($liste) && count($liste) > 0) {
					if (array_key_exists($value, $liste)) {
						$liste_value = $liste[$value];
					} else {
						$liste_value = "";
					}
				}
			}
		}

		if (isset($liste_value) && !empty($liste_value)) {
			$champ_html = '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="' . $liste_value . '" disabled/>';
		}

		return $champ_html;
	}

	/**
	 * @access private
	 * @name getChampNumerique()
	 * Fonction qui génere l'affichage des champs de type numerique
	 *
	 *  @param array			$champ: Champ de type "numerique"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampNumerique($champ)
	{
		return false;
	}

	/**
	 * @access private
	 * @name getChampTelephone()
	 * Fonction qui génere l'affichage des champs de type telephone
	 *
	 *  @param array			$champ: Champ de type "telephone"
	 *  @return string      $champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampTelephone($champ)
	{
		return false;
	}

	/**
	 * @access private
	 * @name getValueChamp()
	 * Fonction qui génere les lignes d'un onglet
	 *
	 *  @param array			$onglet: les informations de l'onglet sous forme de tableau
	 *  @return string      $lignes: retour les lignes et les champs qu'ils contiennent sous forme html 
	 */
	private function getValueChamp($champ, $description)
	{
		return ($description == "basic") ? $this->getValueChampBasic($champ) : $this->getValueChampCustom($champ);
	}

	/**
	 * @access private
	 * @name getValueChampBasic()
	 * Fonction qui génere les lignes d'un onglet
	 *
	 *  @param array			$onglet: les informations de l'onglet sous forme de tableau
	 *  @return string      $lignes: retour les lignes et les champs qu'ils contiennent sous forme html 
	 */
	private function getValueChampBasic($champ)
	{
		return (isset($this->bean->$champ)) ? $this->bean->$champ : false;
	}

	/**
	 * @access private
	 * @name getValueChampCustom()
	 * Fonction qui génere les lignes d'un onglet
	 *
	 *  @param array			$onglet: les informations de l'onglet sous forme de tableau
	 *  @return string      $lignes: retour les lignes et les champs qu'ils contiennent sous forme html 
	 */
	private function getValueChampCustom($champ)
	{
		$value = false;
		$champs_custom = (isset($this->bean->champs_custom) && !empty($this->bean->champs_custom)) ? json_decode(base64_decode($this->bean->champs_custom), true) : array();

		if (is_array($champs_custom) && count($champs_custom) > 0) {
			if (array_key_exists($champ, $champs_custom) === true) {
				$value = htmlspecialchars($champs_custom[$champ], ENT_COMPAT);
			}
		}
		return $value;
		//return (isset($this->bean->$champ)) ? $this->bean->$champ : false;
	}

	/**
	 * @access private
	 * @name isValidChamp()
	 * Fonction qui vérifie si un champ est valide
	 *
	 *  @return boolean		 - $valid : true si le champ est valide, false si le champ est vide ou incomplet 
	 */
	private function isValidChamp($champ)
	{
		return (is_array($champ) && count($champ) > 0 && !empty($champ['type'])) ? true : false;
	}
	/* ********************************************************************************************************************************************************** */
	/* ************************************************************************ Astre ************************************************************************ */
	/* ********************************************************************************************************************************************************** */

	/**
	 * @access private
	 * @name getChampAstre()
	 * Fonction qui génere l'affichage des champs de type astre
	 *
	 *  @param array				$champ: Champ de type "astre"
	 *  @return string      		$champ_html: retourne la valeur du champ ( Format html )
	 */
	private function getChampAstre($champ)
	{

		if ($champ['name'] === "astre_tiers_appairage") {
			$tiers_appairage = ($this->dossierEntity->tiers_appairage !== false && $this->dossierEntity->tiers_appairage->statut === 'ok')
				? $this->dossierEntity->tiers_appairage->name
				: 'Non appairé';
			$champ_html = '<div id="titre_champ_' . $champ['name'] . '" style="white-space: initial;width:25%;height: 24px;">' . $champ['libelle'] . ':</div>';
			$champ_html .= '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="' . $tiers_appairage . '" disabled/>';
		}

		if ($champ['name'] === "astre_tiers_raison_sociale") {
			$tiers_raison_sociale = ($this->dossierEntity->tiers_appairage !== false && $this->dossierEntity->tiers_appairage->statut === 'ok')
				? $this->dossierEntity->tiers_appairage->raison_sociale_astre
				: 'Non appairé';
			$champ_html = '<div id="titre_champ_' . $champ['name'] . '" style="white-space: initial;width:25%;height: 24px;">' . $champ['libelle'] . ':</div>';
			$champ_html .= '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="' . $tiers_raison_sociale . '" disabled/>';
		}

		if ($champ['name'] === "astre_domiciliation") {
			$domiciliation = ($this->dossierEntity->domiciliation !== false)
				? $this->dossierEntity->domiciliation->name_display
				: 'Aucune domiciliation séléctionnée';
			$champ_html = '<div id="titre_champ_' . $champ['name'] . '" style="white-space: initial;width:25%;height: 24px;">' . $champ['libelle'] . ':</div>';
			$champ_html .= '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="' . $domiciliation . '" disabled/>';
		}

		if ($champ['name'] === "astre_domiciliation_appairage") {
			$domiciliation_appairage = ($this->dossierEntity->domiciliation_appairage !== false && $this->dossierEntity->domiciliation_appairage->statut === 'ok')
				? $this->dossierEntity->domiciliation_appairage->name
				: 'Non appairé';
			$champ_html = '<div id="titre_champ_' . $champ['name'] . '" style="white-space: initial;width:25%;height: 24px;">' . $champ['libelle'] . ':</div>';
			$champ_html .= '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="' . $domiciliation_appairage . '" disabled/>';
		}

		return $champ_html;
	}
}
