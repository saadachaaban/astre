<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once 'custom/include/helpers/ode_helper.php';
require_once 'custom/include/helpers/ode_date.php';
require_once 'custom/include/helpers/ode_date.php';
require_once 'custom/include/ode_generateur/OdeFieldFactory.php';
require_once 'custom/include/Astre/Entity/DossierEntity.php';

class ViewModifier extends SugarView
{

    private $dossier;
    public $dossierEntity;

    public function __construct($bean = null, $view_object_map = array())
    {
        parent::SugarView($bean, $view_object_map);
    }

    public function preDisplay()
    {
        if (!$this->bean->ACLAccess('edit')) {
            ACLController::displayNoAccess();
            sugar_die('Access Denied Modifier dossier');
        }
    }

    /**
     * @see SugarView::display()
     */
    public function display()
    {
        $smarty = new Sugar_Smarty();
        $current_template = "modules/OPS_dossier/tpls/modifier.tpl";

        parent::display();
        $this->displayTMCE();

        $this->dossierEntity = new DossierEntity($_REQUEST['id_dossier']);

        // Initialisation de la vue séléctionnée
        $this->dossier = new OPS_dossier;
        $this->dossier->retrieve($_REQUEST['id_dossier']);

        $smarty->assign("dossier_titre",  $this->dossier->name);
        $smarty->assign("dossier_id",  $this->dossier->id);

        $onglets = $this->dossier->getOnglets();

        // Génération des bouttons onglets 
        if ($onglets !== false) {
            // Initialisation des boutons onglets 
            $smarty->assign("BOUTONS_ONGLETS", $this->getInitBoutonsOnglets($onglets));
            // Initialisation des layout onglets 
            $smarty->assign("LAYOUTS_ONGLETS", $this->getInitLayoutsOnglets($onglets));
        }

        $smarty->display($current_template);
    }

    /**
     * @access private
     * @name getInitBoutonsOnglets()
     * Fonction qui génére les boutons d'onglets 
     * 
     *  @param array            $onglets : la liste des onglets rattaché à la vue
     *  @return string          $btn_onglets_html : code html , les boutons sont groupés dans une div
     */
    private function getInitBoutonsOnglets($onglets)
    {

        $btn_onglets_html = "";
        $count = 0;
        foreach ($onglets as $ordre => $onglet) {
            $btn_onglets_html .= '<div class="div_button_onglet" >';
            $btn_onglets_html .=    '<div role="onglet" id="btn_onglet_' . $onglet['id'] . '" title="' . $onglet['libelle'] . '" data-id="' . $onglet['id'] . '" data-libelle="' . $onglet['libelle'] . '" data-cle="' . $onglet['cle'] . '" onclick="displayOnglet(\'' . $onglet['id'] . '\')" ';
            $btn_onglets_html .=    ($count == 0) ? 'class="btn_onglet btn_onglet_selected" data-selected="1" >' : 'class="btn_onglet btn_onglet_not_selected" data-selected="0" >';
            $btn_onglets_html .=        '<div class="div_libelle_button_onglet" id="titre_onglet_' . $onglet['id'] . '">';
            $btn_onglets_html .=           $onglet['libelle'];
            $btn_onglets_html .=        '</div>';
            $btn_onglets_html .=    '</div>';
            $btn_onglets_html .= '</div>';
            $count++;
        }
        return $btn_onglets_html;
    }

    /**
     * @access private
     * @name getInitLayoutsOnglets()
     * Fonction qui génére les layouts d'onglets, en d'autre terme le container qui contient les lignes  
     * 
     *  @param array            $onglets : la liste des onglets rattaché à la vue
     *  @return string          $layout_onglets_html : code html , une div par layout, le premier est affiché par défaut
     */
    private function getInitLayoutsOnglets($onglets)
    {
        $layout_onglets_html = "";
        $count = 0;
        foreach ($onglets as $ordre => $onglet) {
            $layout_onglets_html .= '<div id="layout_onglet_' . $onglet['id'] . '" class="div_layout_onglet" ';
            if ($count == 0) {
                $layout_onglets_html .= 'style="display:block;" >';
            } else {
                $layout_onglets_html .= 'style="display:none;" >';
            }
            $layout_onglets_html .= $this->getLignesOnglet($onglet);
            $layout_onglets_html .= '</div>';
            $count++;
        }
        return $layout_onglets_html;
    }

    /**
     * @access private
     * @name getLignesOnglet()
     * Fonction qui génere les lignes d'un onglet
     *
     *  @param array            $onglet: les informations de l'onglet sous forme de tableau
     *  @return string          $lignes: retour les lignes et les champs qu'ils contiennent sous forme html 
     */
    private function getLignesOnglet($onglet)
    {

        $lignes = '';
        if (is_array($onglet['champs']) && count($onglet['champs']) > 0) {
            foreach ($onglet['champs'] as $num_ligne => $ligne) {
                if (is_array($ligne)) {
                    ksort($ligne);
                }
                $lignes .= '<div class="row" style="background: white; margin: 0px; padding: 5px;">';
                foreach ($ligne as $pos => $champ) {
                    if ($this->isValidChamp($champ) === true) {
                        $lignes .= '<div id="champ_' . $champ['name'] . '" class="col-md-6 champ_referentiel champ_panel"';
                        $lignes .= ($champ['type'] != "relation") ? 'style="padding-right: 4%;">' : '>';
                        if ($champ['type'] != "vide") {
                            $lignes .= '<div id="titre_champ_' . $champ['name'] . '" style="white-space: initial; padding-top: 15px;"';
                            $lignes .= (!empty($champ['aide'])) ? 'title="' . $champ['aide'] . '"' : '';
                            $lignes .= 'class="col-md-4">' . $champ['libelle'] . ':';
                            $lignes .= ($champ['obligatoire'] == 1) ? '<span style="color:red;">*</span>' : '';
                            $lignes .= (!empty($champ['aide'])) ? ' <i class="fa fa-info-circle" style="color: #35B3C5;"></i>' : '';
                            if ($champ['type'] === "relation") {
                                $json_params = base64_decode($champ['params']);
                                $retour_json_to_array = ODE::jsonToArray($json_params);
                                if ($retour_json_to_array['statut'] == "ok") {
                                    $params = $retour_json_to_array['data'];
                                    if ($params['unique'] === "multi") {
                                        $lignes .= '<i class="fa fa-plus" style="background: #35B3C5;color: white;border-radius: 16px;padding: 4px;margin-left: 9px;cursor: pointer;" onclick="addLigneRelation(\'' . $champ['name'] . '\',\'' . $params['module_name'] . '\',\'' . $params['type'] . '\',\'' . $params['unique'] . '\',\'' . $params['condition'] . '\')"></i>';
                                    }
                                }
                            }
                            $lignes .= '<small style="color:red;display:none;">Message d\'erreur</small>';
                            $lignes .= '</div>';
                            $lignes .= $this->getChampHtml($champ);
                        }
                        $lignes .= '</div>';
                    }
                }
                $lignes .= '</div>';
            }
        }

        return $lignes;
    }

    /**
     * @access private
     * @name getChampHtml()
     * Fonction qui génere les lignes d'un onglet
     *
     *  @param array            $onglet: les informations de l'onglet sous forme de tableau
     *  @return string          $lignes: retour les lignes et les champs qu'ils contiennent sous forme html 
     */
    private function getChampHtml($champ)
    {

        $html = false;
        $default_html = '<input type="text" value="Cas « ' . $champ['type'] . ' » non traité !" style="display: block;color: #f08377;background: #f8f8f8 !important;border: 1px solid red;" class="ode_input_hidden col-md-8" />';

        switch ($champ['type']) {

            case 'text': {
                    $html = $this->getChampText($champ);
                    break;
                }
            case 'text_long': {
                    $html = $this->getChampTextLong($champ);
                    break;
                }
            case 'relation': {
                    $html = $this->getChampRelation($champ);
                    break;
                }
            case 'liste': {
                    $html = $this->getChampListe($champ);
                    break;
                }
            case 'liste_choix_simple': {
                    $html = $this->getChampListeChoixSimple($champ);
                    break;
                }
            case 'montant': {
                    $html = $this->getChampMontant($champ);
                    break;
                }
            case 'date': {
                    $html = $this->getChampDate($champ);
                    break;
                }
            case 'checkbox': {
                    $html = $this->getChampCheckbox($champ);
                    break;
                }
            case 'lien': {
                    $html = $this->getChampLien($champ);
                    break;
                }
            case 'pourcentage': {
                    $html = $this->getChampPourcentage($champ);
                    break;
                }
            case 'liste_choix_multi': {
                    $html = $this->getChampListeChoixMulti($champ);
                    break;
                }
            case 'wysiwyg': {
                    $html = $this->getChampWysiwyg($champ);
                    break;
                }
            case 'astre': {
                    $html = $this->getChampAstre($champ);
                    break;
                }
                /*
            case 'adresse': {  $html = $this->getChampAdresse($champ); break; } 
            case 'bouton_radio': { $html = $this->getChampBoutonRadio($champ); break; }
            case 'chaine_libre': { $html = $this->getChampChaineLibre($champ); break; } 
            case 'numerique': { $html = $this->getChampNumerique($champ); break; } 
            case 'telephone': { $html = $this->getChampTelephone($champ); break; }
            */
        }

        if ($champ['name'] == "thematique") {
            // Récupération du dispositif lié au dossier 
            $dispositifs = $this->dossier->get_linked_beans('ops_dispositif_ops_dossier', 'OPS_dispositif');
            $dispositif = (is_array($dispositifs) && count($dispositifs) == 1) ? $dispositifs[0] : false;
            $liste = (!empty($dispositif->id)) ? json_decode(base64_decode(OPS_dispositif::getListeThematique($dispositif->thematique)), true) : false;
            $required = ($champ['obligatoire'] == 1) ? 'required="true"' : '';
            $value = $this->getValueChamp($champ['name'], $champ['description']);
            if (is_array($liste) && count($liste) > 0) {
                $champ_thematique_html = '<select class="col-md-8" ' . $required . ' id="value_champ_' . $champ['name'] . '">';
                foreach ($liste as $index => $valeur) {
                    $selected = ($index == $value) ? " selected" : "";
                    $champ_thematique_html .= '<option value="' . $index . '"  ' . $selected . '>' . $valeur . '</option>';
                }
                $champ_thematique_html .= '</select>';
            }
            $html = (!empty($champ_thematique_html)) ? $champ_thematique_html : false;
        }

        if ($html === false) {
            $GLOBALS['log']->fatal(" modules/OPS_dossier/views/view.modifier.php :: getChampHtml() => Erreur de récupération pour le champ " . $champ['name']);
        }

        return ($html !== false) ? $html : $default_html;
    }

    /**
     * @access private
     * @name getChampRelation()
     * Fonction qui génere l'affichage des champs de type relation
     *
     *  @param array            $champ: Champ de type "relation"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampRelation($champ)
    {

        $champ_html = false;

        $array_champs = array("assigned_user_id", "created_by", "modified_user_id");
        $value = (in_array($champ['name'], $array_champs)) ? "" : $this->getValueChamp($champ['name'], $champ['description']);

        $json_formated = base64_decode($champ['params']);
        $retour_json_to_array = ODE::jsonToArray($json_formated);

        if ($retour_json_to_array['statut'] == "ok") {
            if (in_array($champ['name'], $array_champs)) {

                if ($champ['name'] === "assigned_user_id") {
                    $enregistrement_id = $this->dossier->assigned_user_id;
                    $enregistrement_name = $this->dossier->assigned_user_name;
                }
                if ($champ['name'] === "created_by") {
                    $enregistrement_id = $this->dossier->created_by;
                    $enregistrement_name = $this->dossier->created_by_name;
                }
                if ($champ['name'] === "modified_user_id") {
                    $enregistrement_id = $this->dossier->modified_user_id;
                    $enregistrement_name = $this->dossier->modified_by_name;
                }
                if (!empty($enregistrement_id) &&  !empty($enregistrement_name)) {
                    $enregistrements[0] = array(
                        'id' => $enregistrement_id,
                        'name' => $enregistrement_name
                    );
                } else {
                    $enregistrements = array();
                }
                $data_module = array(
                    'unique' => 'unique',
                    'type' => '',
                    'condition' => '',
                );
                $module = array(
                    'module_name' => 'Users',
                    'enregistrements' => $enregistrements,
                    'nb_enregistrement' => count($enregistrements),
                );
            } else {
                $data_module = $retour_json_to_array['data'];
                $module = $this->getRelatedModule($data_module['module_name'], $data_module['relation_name'], $data_module['unique'], $data_module['type'], $data_module['condition'],  $value);
            }

            if ($data_module['unique'] == 'unique' || $data_module['unique'] == 'multi' && $module['nb_enregistrement'] < 2) {

                $name_display = ($module['nb_enregistrement'] > 0) ? $module['enregistrements'][0]['name'] : "";
                $id_display = ($module['nb_enregistrement'] > 0) ? $module['enregistrements'][0]['id'] : "";
                $is_disabled = ($champ['ineditable'] == 1) ? 'disabled' : '';
                $background = ($champ['ineditable'] == 1) ? 'background: #F8F8F8 !important;' : 'background: #c3e7fd none repeat scroll 0 0 !important;';
                $required = ($champ['obligatoire'] == 1) ? 'required="true"' : '';
                $data_module_condition = ($data_module['unique'] !== 'multi') ? $data_module['condition'] : "";
                $champ_html = '<div style="margin-left: -3%;" class="col-md-8">
                        <div id="display_value_champ_' . $champ['name'] . '" class="col-md-9" style="overflow: hidden; display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;border-radius: 3px; height: 47px; ' . $background . ' border: 1px solid #b8daef; font-size: 14px; font-weight: 400; color: #534d64; padding: 12px !important;">
                        ' . $name_display . '
                        </div>
                        <div class="col-md-3" style="text-align: center;">
                            <span class="id-ff multiple">
                                <button type="button" ' . $is_disabled . ' id="btn_statut_initial" title="Sélectionner" onclick="selectRelatedModule(\'' . $champ['name'] . '\',\'' . $module['module_name'] . '\',\'' . $data_module['type'] . '\',\'' . $data_module['unique'] . '\',\'' . $data_module_condition . '\')" class="button firstChild" value="Sélectionner">
                                    <img src="themes/SuiteP/images/id-ff-select.png?v=9PUULME3DHwNvxEgZrpwig" />
                                </button>
                                <button type="button" ' . $is_disabled . ' id="btn_clr_statut_initial" title="Clear Selection" onclick="deleteRelatedModule(\'' . $champ['name'] . '\',\'' . $data_module['unique'] . '\',\'\')" class="button lastChild" value="Supprimer la selection">
                                    <img src="themes/SuiteP/images/id-ff-clear.png?v=9PUULME3DHwNvxEgZrpwig" />
                                </button>
                            </span>
                        </div>
                        <input type="hidden" ' . $required . ' id="value_champ_' . $champ['name'] . '" value="' . $id_display . '" />
                    </div>';
            } else {

                $champ_html = '';

                foreach ($module['enregistrements'] as $key => $enregistrement) {
                    $module_ids[] = $enregistrement['id'];
                }

                foreach ($module['enregistrements'] as $key => $enregistrement) {

                    $name_display = $enregistrement['name'];
                    $id_display = $enregistrement['id'];
                    $is_disabled = ($champ['ineditable'] == 1) ? 'disabled' : '';
                    $background = ($champ['ineditable'] == 1) ? 'background: #F8F8F8 !important;' : 'background: #c3e7fd none repeat scroll 0 0 !important;';
                    $required = ($champ['obligatoire'] == 1) ? 'required="true"' : '';

                    if ($key === 0) {
                        $champ_html .= '<div style="margin-left: -3%;" class="col-md-8">';
                        $champ_html .=  '<div id="display_value_champ_' . $champ['name'] . '" data-id-selected="' . $id_display . '" class="col-md-9" style="overflow: hidden; display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;border-radius: 3px; height: 47px; ' . $background . ' border: 1px solid #b8daef; font-size: 14px; font-weight: 400; color: #534d64; padding: 12px !important;">
                            ' . $name_display . '
                            </div>
                            <div class="col-md-3" style="text-align: center;">
                                <span class="id-ff multiple">
                                    <button type="button" ' . $is_disabled . ' id="btn_statut_initial" title="Sélectionner" onclick="selectRelatedModule(\'' . $champ['name'] . '\',\'' . $module['module_name'] . '\',\'' . $data_module['type'] . '\',\'' . $data_module['unique'] . '\',\'\')" class="button firstChild" value="Sélectionner">
                                        <img src="themes/SuiteP/images/id-ff-select.png?v=9PUULME3DHwNvxEgZrpwig" />
                                    </button>
                                    <button type="button" ' . $is_disabled . ' id="btn_clr_statut_initial" title="Clear Selection" onclick="deleteRelatedModule(\'' . $champ['name'] . '\',\'' . $data_module['unique'] . '\',\'\')" class="button lastChild" value="Supprimer la selection">
                                        <img src="themes/SuiteP/images/id-ff-clear.png?v=9PUULME3DHwNvxEgZrpwig" />
                                    </button>
                                </span>
                            </div>
                            <input type="hidden" ' . $required . ' id="value_champ_' . $champ['name'] . '" value="' . implode("|", $module_ids) . '" />';
                        $champ_html .= '</div>';
                    } else {

                        $champ_html .= '<div style="margin: 1% 0% 1% 30%;" class="col-md-8">';
                        $champ_html .=    '<div id="display_value_champ_' . $champ['name'] . '_' . $key . '" data-id-selected="' . $id_display . '" class="col-md-9" style="overflow: hidden; display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;border-radius: 3px; height: 47px; background: #c3e7fd none repeat scroll 0 0 !important; border: 1px solid #b8daef; font-size: 14px; font-weight: 400; color: #534d64; padding: 12px !important;">' . $name_display . '</div>';
                        $champ_html .=    '<div class="col-md-3" style="text-align: center;">';
                        $champ_html .=        '<span class="id-ff multiple">';
                        $champ_html .=            '<button type="button" id="btn_statut_initial" title="Sélectionner" onclick="selectRelatedModule(\'' . $champ['name'] . '_' . $key . '\',\'' . $module['module_name'] . '\',\'' . $data_module['type'] . '\',\'' . $data_module['unique'] . '\',\'data-parent=' . $champ['name'] . '\')" class="button firstChild" value="Sélectionner">';
                        $champ_html .=                '<img src="themes/SuiteP/images/id-ff-select.png?v=9PUULME3DHwNvxEgZrpwig">';
                        $champ_html .=            '</button>';
                        $champ_html .=            '<button type="button" id="btn_clr_statut_initial" title="Clear Selection" onclick="deleteRelatedModule(\'' . $champ['name'] . '_' . $key . '\',\'' . $data_module['unique'] . '\',\'data-parent=' . $champ['name'] . '\')" class="button lastChild" value="Supprimer la selection">';
                        $champ_html .=                '<img src="themes/SuiteP/images/id-ff-clear.png?v=9PUULME3DHwNvxEgZrpwig">';
                        $champ_html .=            '</button>';
                        $champ_html .=        '</span>';
                        $champ_html .=    '</div>';
                        $champ_html .= '</div>';
                    }
                }
            }
        }


        return $champ_html;
    }

    /**
     * @access private
     * @name getRelatedModuleTable()
     * Fonction qui génere l'affichage des champs de type relation
     *
     *  @param array            $champ: Champ de type "relation"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getRelatedModuleTable($module_name, $relation_name, $unique)
    {

        $retour_enregistrements = array();
        $enregistrements =  $this->dossier->get_linked_beans($relation_name, $module_name);

        if (is_array($enregistrements) && count($enregistrements) > 0) {
            if ($unique === true) {
                if (!empty($enregistrements[0]->id) && !empty($enregistrements[0]->name)) {
                    $retour_enregistrements[0] = array(
                        "id" => $enregistrements[0]->id,
                        "name" => $enregistrements[0]->name,
                    );
                }
            } else {
                foreach ($enregistrements as $key => $enregistrement) {
                    if (!empty($enregistrements[$key]->id) && !empty($enregistrements[$key]->name)) {
                        $retour_enregistrements[$key] = array(
                            "id" => $enregistrements[$key]->id,
                            "name" => $enregistrements[$key]->name,
                        );
                    }
                }
            }
        }

        return $retour_enregistrements;
    }

    /**
     * @access private
     * @name getRelatedModuleParentID()
     * Fonction qui génere l'affichage des champs de type relation
     *
     *  @param array            $champ: Champ de type "relation"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getRelatedModuleParentID($module_name, $enregistrement_id)
    {

        $enregistrement = array();
        $module_liste = array_intersect($GLOBALS['moduleList'], array_keys($GLOBALS['beanList']));
        if (in_array($module_name, $module_liste, TRUE)) {
            $obj_module = BeanFactory::getBean($module_name, $enregistrement_id);
            if (!empty($obj_module->id)) {
                $enregistrement[0] = array(
                    "id" => $obj_module->id,
                    "name" => $obj_module->name,
                );
            } else {
                if ($enregistrement_id == "1" || $enregistrement_id == 1) {
                    $enregistrement[0] = array(
                        "id" => "1",
                        "name" => "Administrator",
                    );
                }
            }
        } else {
            $GLOBALS['log']->fatal(" view.detail.php :: getRelatedModuleParentID() => Module $module_name non déclaré dans la liste des modules.");
        }
        return $enregistrement;
    }

    /**
     * @access private
     * @name getChampRelation()
     * Fonction qui génere l'affichage des champs de type relation
     *
     *  @param array            $champ: Champ de type "relation"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getRelatedModule($_module_name, $_relation_name, $unique, $type, $condition, $champ_value)
    {

        $retour_enregistrements = array();

        if ($type == "table") {
            if (!empty($condition)) {
                if (!empty($this->dossier->$condition)) {
                    foreach ($_module_name as $module) {
                        if ($module['value'] == $this->dossier->$condition) {
                            $module_name = $module['name'];
                        }
                    }
                    foreach ($_relation_name as $relation) {
                        if ($relation['value'] == $this->dossier->$condition) {
                            $relation_name = $relation['name'];
                        }
                    }
                    $retour_enregistrements = $this->getRelatedModuleTable($module_name, $relation_name, $unique);
                } else {
                    $GLOBALS['log']->fatal(" view.detail.php :: getRelatedModule() => champs référentiel le champ $condition du dossier est null ");
                }
            } else {
                $module_name = $_module_name;
                $relation_name = $_relation_name;
                $retour_enregistrements = $this->getRelatedModuleTable($_module_name, $_relation_name, $unique);
            }
        }

        if ($type == "parent_id") {
            // Avec l'id le parent un seul enregistrement est retourné
            $module_name = $_module_name;
            if (!empty($champ_value)) {
                $retour_enregistrements = $this->getRelatedModuleParentID($_module_name, $champ_value);
            }
        }

        $module = array(
            "module_name" => $module_name,
            "enregistrements" => $retour_enregistrements,
            "nb_enregistrement" => ($unique === true) ? 1 : count($retour_enregistrements),
        );
        return $module;
    }

    /**
     * @access private
     * @name getChampTextLong()
     * Fonction qui génere l'affichage des champs de type text
     *
     *  @param array            $champ: Champ de type "text"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampTextLong($champ)
    {
        $champ_html = false;
        $value = $this->getValueChamp($champ['name'], $champ['description']);
        $required = ($champ['obligatoire'] == 1) ? 'required="true"' : '';
        if (!empty($champ['name'])) {
            $value_display = ($value !== false) ? $value : "";
            if ($champ['ineditable'] == 0) {
                $champ_html = '<textarea class="ode_input_hidden" ' . $required . ' id="value_champ_' . $champ['name'] . '" style="display: block;height: 11em;resize: none;overflow-y: scroll;">';
                $champ_html .= html_entity_decode($value_display);
                $champ_html .= '</textarea>';
            } else {
                $champ_html = '<textarea class="ode_input_hidden" disabled style="display: block;height: 11em;resize: none;overflow-y: scroll;background: #F8F8F8 !important;">';
                $champ_html .= html_entity_decode($value_display);
                $champ_html .= '</textarea>';
            }
        }
        return $champ_html;
    }

    /**
     * @access private
     * @name getChampWysiwyg()
     * Fonction qui génere l'affichage des champs de type text
     *
     *  @param array            $champ: Champ de type "text"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampWysiwyg($champ)
    {
        $champ_html = false;
        $value = $this->getValueChamp($champ['name'], $champ['description']);
        $required = ($champ['obligatoire'] == 1) ? 'required="true"' : '';
        if (!empty($champ['name'])) {
            $value_display = ($value !== false) ? $value : "";
            if ($champ['ineditable'] == 0) {
                $champ_html = '<textarea class="ode_input_hidden" ' . $required . ' id="value_champ_' . $champ['name'] . '" style="display: block;height: 11em;resize: none;overflow-y: scroll;">';
                $value_display = preg_replace('/\s|-/i', '+', $value_display);
                $champ_html .= utf8_encode(base64_decode($value_display));
                $champ_html .= '</textarea>';
                $champ_html .= '<script type="text/javascript">wysiwyg("value_champ_' . $champ['name'] . '");</script>';
            } else {
                $champ_html = '<textarea  class="ode_input_hidden" disabled style="display: block;height: 11em;resize: none;overflow-y: scroll;background: #F8F8F8 !important;">';
                $champ_html .= html_entity_decode($value_display);
                $champ_html .= '</textarea>';
            }
        }
        return $champ_html;
    }

    /**
     * @access private
     * @name getChampLien()
     * Fonction qui génere l'affichage des champs de type text
     *
     *  @param array            $champ: Champ de type "text"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampLien($champ)
    {

        $champ_html = false;
        $value = $this->getValueChamp($champ['name'], $champ['description']);
        $required = ($champ['obligatoire'] == 1) ? 'required="true"' : '';
        if (!empty($champ['name'])) {
            $value_display = ($value !== false) ? $value : "";
            if ($champ['ineditable'] == 0) {
                $champ_html = '<input id="value_champ_' . $champ['name'] . '" type="text" ' . $required . ' value="' . $value_display . '" style="display: block;" class="ode_input_hidden col-md-8" />';
            } else {
                $champ_html = '<input type="text" disabled value="' . $value_display . '" style="display: block;background: #F8F8F8 !important;" class="ode_input_hidden col-md-8" />';
            }
        }
        return $champ_html;
    }

    /**
     * @access private
     * @name getChampPourcentage()
     * Fonction qui génere l'affichage des champs de type text
     *
     *  @param array            $champ: Champ de type "text"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampPourcentage($champ)
    {
        $champ_html = false;

        $value = $this->getValueChamp($champ['name'], $champ['description']);
        $pourcentage = number_format(floatval($value), 2, '.', ' ');
        $required = ($champ['obligatoire'] == 1) ? 'required="true"' : '';

        if (gettype($pourcentage) === "integer" && gettype($pourcentage) === "double" || gettype($pourcentage) === "string" && intval($pourcentage) !== 0) {
            $pourcentage_display =  str_replace(" ", "", $pourcentage);
        } else {
            $pourcentage_display = "0.00";
        }

        if ($champ['ineditable'] == 0) {
            $champ_html = '<input type="number" min="0" max="100" step="0.01" ' . $required . ' class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;" value="' . $pourcentage_display . '" />';
        } else {
            $champ_html = '<input type="number" min="0" max="100" step="0.01" id="value_champ_' . $champ['name'] . '" class="ode_input_hidden" style="display:block;background: #F8F8F8 !important;" value="' . $pourcentage_display . '" disabled/>';
        }

        return $champ_html;
    }

    /**
     * @access private
     * @name getChampText()
     * Fonction qui génere l'affichage des champs de type text
     *
     *  @param array            $champ: Champ de type "text"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampText($champ)
    {
        $champ_html = false;
        $value = $this->getValueChamp($champ['name'], $champ['description']);
        $required = ($champ['obligatoire'] == 1) ? 'required="true"' : '';
        if (!empty($champ['name'])) {
            $value_display = ($value !== false) ? $value : "";
            if ($champ['ineditable'] == 0) {
                $champ_html = '<input id="value_champ_' . $champ['name'] . '" type="text" ' . $required . ' value="' . html_entity_decode($value_display) . '" style="display: block;" class="ode_input_hidden col-md-8" />';
            } else {
                $champ_html = '<input type="text" disabled value="' . html_entity_decode($value_display) . '" style="display: block;background: #F8F8F8 !important;" class="ode_input_hidden col-md-8" />';
            }
        }
        return $champ_html;
    }

    /**
     * @access private
     * @name getChampMontant()
     * Fonction qui génere l'affichage des champs de type adresse
     *
     *  @param array            $champ: Champ de type "adresse"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampMontant($champ)
    {

        $champ_html = false;
        $value = $this->getValueChamp($champ['name'], $champ['description']);
        $montant = number_format(floatval($value), 2, '.', '');
        $montant_display = ((float) $montant > 0) ? $montant : "0.00";
        $required = ($champ['obligatoire'] == 1) ? 'required="true"' : '';

        if ($champ['ineditable'] == 0) {
            $champ_html = '<input id="value_champ_' . $champ['name'] . '" type="number" ' . $required . ' value="' . $montant . '" step=".01" min="0" style="display: block;" class="ode_input_hidden col-md-8">';
        } else {
            $champ_html = '<input id="value_champ_' . $champ['name'] . '" type="number" disabled value="' . $montant . '" step=".01" min="0" style="display: block;background: #F8F8F8 !important;" class="ode_input_hidden col-md-8">';
        }

        return $champ_html;
    }


    /**
     * @access private
     * @name getChampAdresse()
     * Fonction qui génere l'affichage des champs de type adresse
     *
     *  @param array            $champ: Champ de type "adresse"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampAdresse($champ)
    {
        return false;
    }

    /**
     * @access private
     * @name getChampBoutonRadio()
     * Fonction qui génere l'affichage des champs de type bouton_radio
     *
     *  @param array            $champ: Champ de type "bouton_radio"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampBoutonRadio($champ)
    {
        return false;
    }

    /**
     * @access private
     * @name getChampCaseCocher()
     * Fonction qui génere l'affichage des champs de type case_cocher
     *
     *  @param array            $champ: Champ de type "case_cocher"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampCheckbox($champ)
    {

        $champ_html = false;
        $value = $this->getValueChamp($champ['name'], $champ['description']);
        $checked = ($value == "0") ? "" : "checked";
        $required = (intval($champ['obligatoire']) == 1) ? ' required="required" ' : '';
        $disabled = (intval($champ['ineditable'])  == 1) ? ' disabled="disabled" ' : '';


        if (!empty($champ['name'])) {
            $champ_html   = '<div class="ode_input_hidden" style="display:block; font-weight: unset;">';
            $champ_html  .= '<input id="value_champ_' . $champ['name'] . '" type="checkbox" ' . $checked . $required . $disabled . ' style="margin-left: 2%;">';
            $champ_html  .= '</div>';
        }
        return $champ_html;
    }

    /**
     * @access private
     * @name getChampChaineLibre()
     * Fonction qui génere l'affichage des champs de type chaine_libre
     *
     *  @param array            $champ: Champ de type "chaine_libre"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampChaineLibre($champ)
    {
        return false;
    }

    /**
     * @access private
     * @name getChampDate()
     * Fonction qui génere l'affichage des champs de type date
     *
     *  @param array            $champ: Champ de type "date"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampDate($champ)
    {

        $champ_html = false;

        // On récupère la valeur de la date stockée en base de données
        $value = $this->getValueChamp($champ['name'], $champ['description']);

        // Si non vide, on formate la date au format HTML sinon vide
        $value_display = (!empty($value)) ? OdeDate::toHTML($value) : "";

        $required = ($champ['obligatoire'] == 1) ? 'required="true"' : '';

        if (!empty($champ['name'])) {
            if ($champ['ineditable'] == 0) {
                $champ_html = '<input id="value_champ_' . $champ['name'] . '" type="text" ' . $required . ' autocomplete="off" style="display: block;" class="ode_input_hidden col-md-6">';
                $champ_html .= '<script> 
                                    $( "#value_champ_' . $champ['name'] . '" ).datepicker({
                                        altField: "#datepicker",
                                        closeText: "Fermer",
                                        firstDay: 1 ,
                                        dateFormat: "dd/mm/yy"
                                    });
                                    document.getElementById("value_champ_' . $champ['name'] . '").value = "' . $value_display . '";
                                    $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );  
                                </script>';
            } else {
                $champ_html = '<input disabled type="text" autocomplete="off" style="display: block;background: #F8F8F8 !important;" class="ode_input_hidden col-md-6" value="' . $value_display . '">';
            }
        }

        return $champ_html;
    }

    /**
     * @access private
     * @name getChampListe()
     * Fonction qui génere l'affichage des champs de type liste
     *
     *  @param array            $champ: Champ de type "liste"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampListe($champ)
    {

        global $app_list_strings;
        $champ_html = false;

        $value = $this->getValueChamp($champ['name'], $champ['description']);
        $required = ($champ['obligatoire'] == 1) ? 'required="true"' : '';

        $value_display = (!empty($value)) ? $value : "";
        $json_formated = base64_decode($champ['params']);
        $retour_json_to_array = ODE::jsonToArray($json_formated);
        if ($retour_json_to_array['statut'] == "ok") {
            $params = $retour_json_to_array['data'];
            if (!empty($params['liste_name'])) {
                $liste_name = $params['liste_name'];
                if (array_key_exists($liste_name, $app_list_strings)) {
                    $liste = $app_list_strings[$liste_name];
                }
            }
        }
        if (is_array($liste) && count($liste) > 0) {
            $champ_html  = '<select class="col-md-8" ' . $required . ' id="value_champ_' . $champ['name'] . '"';
            $champ_html .=  ($champ['ineditable'] == 0) ? '>' : ' disabled >';
            foreach ($liste as $index => $valeur) {
                $selected = ($index == $value_display) ? " selected" : "";
                $champ_html .= '<option value="' . $index . '"  ' . $selected . '>' . $valeur . '</option>';
            }
            $champ_html .= '</select>';
        }

        return $champ_html;
    }


    /**
     * @access private
     * @name getChampListeChoixMulti()
     * Fonction qui génere l'affichage des champs de type choix_simple
     *
     *  @param array            $champ: Champ de type "choix_simple"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampListeChoixMulti($champ)
    {

        $champ_value = $this->getValueChamp($champ['name'], $champ['description']);
        $fieldFactory = new OdeFieldFactory(true);
        return $fieldFactory->getHtml($champ, $champ_value);
    }

    /**
     * @access private
     * @name getChampListeChoixSimple()
     * Fonction qui génere l'affichage des champs de type choix_simple
     *
     *  @param array            $champ: Champ de type "choix_simple"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampListeChoixSimple($champ)
    {

        global $app_list_strings;
        $champ_html = false;

        $value = $this->getValueChamp($champ['name'], $champ['description']);
        $required = ($champ['obligatoire'] == 1) ? 'required="true"' : '';
        $value_display = (!empty($value)) ? $value : "";
        $json_formated = base64_decode($champ['params']);
        $retour_json_to_array = ODE::jsonToArray($json_formated);
        if ($retour_json_to_array['statut'] == "ok") {
            $params = $retour_json_to_array['data'];
            if (!empty($params['liste'])) {
                $liste = $params['liste'];
            }
        }
        if (is_array($liste) && count($liste) > 0) {
            $champ_html  = '<select class="col-md-8" ' . $required . ' id="value_champ_' . $champ['name'] . '"';
            $champ_html .=  ($champ['ineditable'] == 0) ? '>' : ' disabled >';
            $champ_html .= '<option value=""></option>';
            foreach ($liste as $index => $valeur) {
                $selected = ($index == $value_display) ? " selected" : "";
                $champ_html .= '<option value="' . $index . '"  ' . $selected . '>' . $valeur . '</option>';
            }
            $champ_html .= '</select>';
        }

        return $champ_html;
    }

    /**
     * @access private
     * @name getChampNumerique()
     * Fonction qui génere l'affichage des champs de type numerique
     *
     *  @param array            $champ: Champ de type "numerique"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampNumerique($champ)
    {
        return false;
    }

    /**
     * @access private
     * @name getChampTelephone()
     * Fonction qui génere l'affichage des champs de type telephone
     *
     *  @param array            $champ: Champ de type "telephone"
     *  @return string          $champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampTelephone($champ)
    {
        return false;
    }

    /**
     * @access private
     * @name getValueChamp()
     * Fonction qui génere les lignes d'un onglet
     *
     *  @param array            $onglet: les informations de l'onglet sous forme de tableau
     *  @return string          $lignes: retour les lignes et les champs qu'ils contiennent sous forme html 
     */
    private function getValueChamp($champ, $description)
    {
        return ($description == "basic") ? $this->getValueChampBasic($champ) : $this->getValueChampCustom($champ);
    }

    /**
     * @access private
     * @name getValueChampBasic()
     * Fonction qui génere les lignes d'un onglet
     *
     *  @param array            $onglet: les informations de l'onglet sous forme de tableau
     *  @return string          $lignes: retour les lignes et les champs qu'ils contiennent sous forme html 
     */
    private function getValueChampBasic($champ)
    {
        return (isset($this->dossier->$champ)) ? $this->dossier->$champ : false;
    }

    /**
     * @access private
     * @name getValueChampCustom()
     * Fonction qui génere les lignes d'un onglet
     *
     *  @param array            $onglet: les informations de l'onglet sous forme de tableau
     *  @return string          $lignes: retour les lignes et les champs qu'ils contiennent sous forme html 
     */
    private function getValueChampCustom($champ)
    {
        $value = false;
        $champs_custom = (isset($this->dossier->champs_custom) && !empty($this->dossier->champs_custom)) ? json_decode(base64_decode($this->dossier->champs_custom), true) : array();
        if (is_array($champs_custom) && count($champs_custom) > 0) {
            if (array_key_exists($champ, $champs_custom) === true) {
                $value = $value = htmlspecialchars($champs_custom[$champ], ENT_COMPAT);
            }
        }
        return $value;
    }

    /**
     * @access private
     * @name isValidChamp()
     * Fonction qui vérifie si un champ est valide
     *
     *  @return boolean               - $valid : true si le champ est valide, false si le champ est vide ou incomplet 
     */
    private function isValidChamp($champ)
    {
        return (is_array($champ) && count($champ) > 0 && !empty($champ['name']) && !empty($champ['libelle']) && !empty($champ['type'])) ? true : false;
    }

    /**
     * @access private
     * @name getChampAstre()
     * Fonction qui génere l'affichage des champs de type astre
     *
     *  @param array				$champ: Champ de type "astre"
     *  @return string      		$champ_html: retourne la valeur du champ ( Format html )
     */
    private function getChampAstre($champ)
    {
        $champ_html = '';

        if ($champ['name'] === "astre_tiers_appairage") {
            $tiers_appairage = ($this->dossierEntity->tiers_appairage !== false && $this->dossierEntity->tiers_appairage->statut === 'ok')
                ? $this->dossierEntity->tiers_appairage->name
                : 'Non appairé';

            $champ_html = '<input disabled="disabled" class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;background: #F8F8F8 !important;" value="' . $tiers_appairage . '" disabled/>';
        }

        if ($champ['name'] === "astre_tiers_raison_sociale") {
            $tiers_raison_sociale = ($this->dossierEntity->tiers_appairage !== false && $this->dossierEntity->tiers_appairage->statut === 'ok')
                ? $this->dossierEntity->tiers_appairage->raison_sociale_astre
                : 'Non appairé';
            $champ_html = '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;background: #F8F8F8 !important;" value="' . $tiers_raison_sociale . '" disabled/>';
        }

        if ($champ['name'] === "astre_domiciliation") {
            $dossier_domiciliation_id = ($this->dossierEntity->domiciliation !== false) ? $this->dossierEntity->domiciliation->id : '';
            $tiers_domiciliations = ($this->dossierEntity->tiers_domiciliations !== false) ? $this->dossierEntity->tiers_domiciliations : [];
            if (is_array($tiers_domiciliations) && count($tiers_domiciliations) > 0) {
                $domiciliation_options = '';
                foreach ($tiers_domiciliations as $domiciliation) {
                    $selected = (!empty($dossier_domiciliation_id) && $domiciliation->id === $dossier_domiciliation_id) ? ' selected="selected" ' : '';
                    $disabled = ($domiciliation->actif == '1') ? '' : ' disabled="disabled" ';
                    $domiciliation_options .= '<option ' . $selected . $disabled . ' value="' . $domiciliation->id . '"> ' . $domiciliation->name_display . ' </option>';
                }
            } else {
                $domiciliation_options = '<option value=""> Aucune domiciliation </option>';
            }
            $champ_html = '<select id="value_champ_' . $champ['name'] . '" style="display:block;" >';
            $champ_html .= $domiciliation_options;
            $champ_html .= '</select>';
        }

        if ($champ['name'] === "astre_domiciliation_appairage") {
            $domiciliation_appairage = ($this->dossierEntity->domiciliation_appairage !== false && $this->dossierEntity->domiciliation_appairage->statut === 'ok')
                ? $this->dossierEntity->domiciliation_appairage->name
                : 'Non appairé';
            $champ_html = '<input class="ode_input_hidden" id="value_champ_' . $champ['name'] . '" style="display:block;background: #F8F8F8 !important;" value="' . $domiciliation_appairage . '" disabled/>';
        }

        return $champ_html;
    }

    function displayTMCE()
    {
        require_once("include/SugarTinyMCE.php");
        global $locale;

        $tiny = new SugarTinyMCE();
        $tinyMCE = $tiny->getConfig();

        $js = '<script src="include/javascript/tiny_mce/tiny_mce.js"></script>';
        $js .= <<<JS
        <script language="javascript" type="text/javascript">
        function wysiwyg(id_textarea){

        $tinyMCE
        var df = '{$locale->getPrecedentPreference('default_date_format')}';

        tinyMCE.init({
            theme : "advanced",
            theme_advanced_toolbar_align : "left",
            mode: "exact",
            onchange_callback : "myCustomOnChangeHandler",
            elements : id_textarea,
            theme_advanced_toolbar_location : "top",
            theme_advanced_buttons1: "code,help,separator,bold,italic,underline,strikethrough,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,forecolor,backcolor,separator,styleprops,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,selectall,separator,search,replace,separator,bullist,numlist,separator,outdent,indent,separator,ltr,rtl,separator,undo,redo,separator, link,unlink,anchor,separator,sub,sup,separator,charmap,visualaid",
            theme_advanced_buttons3: "tablecontrols,separator,advhr,hr,removeformat,separator,insertdate,pagebreak",
            theme_advanced_fonts:"Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Helvetica Neu=helveticaneue,sans-serif;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats",
            plugins : "advhr,insertdatetime,table,paste,searchreplace,directionality,style,pagebreak",
            height:"auto",
            width: "auto",
            inline_styles : true,
            directionality : "ltr",
            remove_redundant_brs : true,
            entity_encoding: 'raw',
            cleanup_on_startup : true,
            strict_loading_mode : true,
            convert_urls : false,
            plugin_insertdate_dateFormat : '{DATE '+df+'}',
            pagebreak_separator : "<pagebreak />",
            extended_valid_elements : "textblock",
            custom_elements: "textblock",
        });

        setTimeout(function(){
            var cont = window.btoa($("#" + id_textarea).val());
        const regex = /(\s|\+)/ig;
        pcont = cont.replaceAll(regex, '-');
        $("#" + id_textarea).val( pcont );
            },
            1000);
    }
        </script>

JS;
        echo $js;
    }
}
