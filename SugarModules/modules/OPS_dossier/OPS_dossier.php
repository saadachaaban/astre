<?php

require_once 'custom/include/helpers/ode_helper.php';
require_once 'custom/include/helpers/ode_date.php';
include_once 'custom/include/models/Autoloader.php';

class OPS_dossier extends Basic
{


    public $new_schema = true;
    public $module_dir = 'OPS_dossier';
    public $object_name = 'OPS_dossier';
    public $table_name = 'ops_dossier';
    public $importable = true;

    public $id;
    public $name;
    public $date_entered;
    public $date_modified;
    public $modified_user_id;
    public $modified_by_name;
    public $created_by;
    public $created_by_name;
    public $description;
    public $deleted;
    public $created_by_link;
    public $modified_user_link;
    public $assigned_user_id;
    public $assigned_user_name;
    public $assigned_user_link;
    public $SecurityGroups;
    public $num_dossier;
    public $type_tiers;
    public $canal;


    public function bean_implements($interface)
    {
        switch ($interface) {
            case 'ACL':
                return true;
        }

        return false;
    }

    /**
     * @access public
     * @name getLastNumDossier()
     * Fonction qui retourne le dernier num_dossier sinon 0
     * 
     *  @return int		  $last_num_dossier : le dernier numéro dossier
     */
    public function getLastNumDossier()
    {

        global $db;
        $last_num_dossier = 0;

        $requete = 'SELECT num_dossier FROM ops_dossier WHERE deleted = 0 AND num_dossier != "" ORDER BY num_dossier DESC LIMIT 1';
        $resultat = $db->query($requete);

        if ($resultat) {
            if ($db->getRowCount($resultat) !== 0) {
                $dossier = $db->fetchByAssoc($resultat);
                if (!empty($dossier['num_dossier'])) {
                    $last_num_dossier = $dossier['num_dossier'];
                }
            }
        } else {
            $GLOBALS['log']->fatal(" OPS_dossier :: initNumDossier => Erreur requete SQL = " . print_r($db->lastError(), true));
        }

        return $last_num_dossier;
    }

    /**
     * @access public
     * @name getVue()
     * Fonction qui retourne la vue associé au dossier sinon false
     * 
     *  @return bean|boolean		  $obj_generateur_vue : objet OPS_generateur_vue
     */
    public function getOnglets()
    {

        $vue =  $this->getVue();
        return ($vue !== false) ? $vue->getListOnglets() : false;
    }

    /**
     * @access public
     * @name getListeChamp()
     * Fonction qui retourne la vue associé au dossier sinon false
     * 
     *  @return bean|boolean		  $obj_generateur_vue : objet OPS_generateur_vue
     */
    public function getListeChamp()
    {
        $vue =  $this->getVue();
        if (!empty($vue->id)) {
            $OPS_generateur_vueModel = new OPS_generateur_vueModel($vue->id);
            $champs_vue = $OPS_generateur_vueModel->getChamps();
        }
        return (is_array($champs_vue) && count($champs_vue) > 0) ? $champs_vue : array();
    }

    /**
     * @access public
     * @name getVue()
     * Fonction qui retourne la vue associé au dossier sinon false
     * 
     *  @return bean|boolean		  $obj_generateur_vue : objet OPS_generateur_vue
     */
    public function getVue()
    {

        $dispositif =  $this->getDispositif();
        $vues = ($dispositif !== false) ? $dispositif->get_linked_beans("ops_generateur_vue_ops_dispositif", "OPS_generateur_vue") : array();
        return (is_array($vues) && count($vues) == 1) ? $vues[0] : false;
    }

    /**
     * @access public
     * @name getDispositif()
     * Fonction qui retourne le dispositif associé au dossier sinon false
     * 
     *  @return bean|boolean		  $obj_dispositif : objet OPS_dispositif
     */
    public function getDispositif()
    {

        $dispositifs = $this->get_linked_beans("ops_dispositif_ops_dossier", "OPS_dispositif");
        return (is_array($dispositifs) && count($dispositifs) == 1) ? $dispositifs[0] : false;
    }

    /**
     * @access private
     * @name setFirstStatut()
     * Fonction qui défini le premier statut (initialisation), crée une historisation et déclenche les actions statut
     * 
     *  
     *  @return boolean	   true / false
     */
    private function setFirstStatut($nouveau_statut = null)
    {

        global $current_user;

        $statut_updated = false;

        // On récupere l'objet OPS_statut
        $obj_statut = ($nouveau_statut !== null) ? BeanFactory::getBean('OPS_statut', $nouveau_statut) : false;

        // On récupere l'id de la nouvelle étape associé au statut de démarrage
        $liste_etapes = (!empty($obj_statut->id)) ? $obj_statut->get_linked_beans('ops_etape_ops_statut', 'OPS_etape') : false;
        $obj_etape = (is_array($liste_etapes) && count($liste_etapes) == 1) ? $liste_etapes[0] : false;

        if (!empty($this->id)) {
            if (!empty($obj_statut->id) && !empty($obj_etape->id)) {

                // On déclenche les actions statut associées
                $obj_statut->action_statut($this);

                // On met à jour le statut et l'étape du dossier
                $this->ops_etape_id = $obj_etape->id;
                $this->ops_statut_id = $obj_statut->id;

                // On met à jour la date d'avancement du dossier
                $timedate = new TimeDate($current_user);
                $date = $timedate->getNow(true);
                $date = $date->format("Y-m-d");
                $this->date_avancement = $date;

                // Historisation du nouveau statut 
                $histo                    = new OPS_historisation();
                $histo->canal            = "action_unitaire";
                $histo->ops_etape_id    = $obj_etape->id;
                $histo->ops_statut_id   = $obj_statut->id;
                $histo->auteur          = $current_user->name;
                $histo->flag_retour     = '1';
                $id_histo               = $histo->save(false);

                if (!empty($id_histo)) {
                    $this->load_relationship('ops_historisation_ops_dossier');
                    $this->ops_historisation_ops_dossier->add($id_histo);
                    if ($this->save()) {
                        $statut_updated = true;
                    } else {
                        $GLOBALS['log']->fatal(" OPS_dossier :: setFirstStatut => Echec de la modification du dossier id = " . print_r($this->id, true));
                    }
                } else {
                    $GLOBALS['log']->fatal(" OPS_dossier :: setFirstStatut => Echec de création historisation sur dossier id = " . print_r($this->id, true));
                }
            } else {
                $GLOBALS['log']->fatal(" OPS_dossier :: setFirstStatut => Echec de l'initialisation de l'étape et du statut du dossier id = " . print_r($this->id, true));
            }
        } else {
            $GLOBALS['log']->fatal(" OPS_dossier :: setFirstStatut = >Dossier introuvable, Initialiser le dossier avant d'appeler la fonction :: dossier->setFirstStatut( Id du nouveau statut ) ");
        }

        return $statut_updated;
    }


    /**
     * @access public
     * @name updateStatut()
     * Fonction qui met à jour le statut , crée une historisation et déclenche les actions statut
     * 
     *  
     *  @return boolean	   true / false
     */

    public function updateStatut($nouveau_statut = null, $type_modification = null)
    {

        global $current_user, $beanFiles;

        $module = $this->module_dir;
        $bean = $this;
        require_once($beanFiles["OPS_etape"]);
        require_once($beanFiles["OPS_statut"]);
        require_once($beanFiles["OPS_historisation"]);

        $statut_updated = false;

        if (!empty($nouveau_statut)) {

            $timedate = new TimeDate($current_user);
            $date = $timedate->getNow(true);
            $date = $date->format("Y-m-d");

            if ($nouveau_statut !== "precedent") {

                // Initialisation du nouveau statut
                $this->ops_statut_id  = $nouveau_statut;
                $statut_updated = true;
            } else {

                // Récupération des status passés 
                $tab_historiques = $this->get_linked_beans('ops_historisation_ops_dossier', 'OPS_historisation', '', '', '', '', "flag_retour = '1'");

                foreach ($tab_historiques as $key => $historisation) {

                    $obj_historisation = new OPS_historisation();
                    $obj_historisation->retrieve($historisation->id);


                    $uneEtape = new OPS_etape();
                    $uneEtape->retrieve($obj_historisation->ops_etape_id);

                    $etapeCourante = new OPS_etape();
                    $etapeCourante->retrieve($this->ops_etape_id);

                    // On exclu du tableau les statuts des étapes suivantes ainsi que le statut actuel  
                    if ($this->ops_statut_id != $obj_historisation->ops_statut_id && $uneEtape->ordre <= $etapeCourante->ordre) {

                        $date                      = $obj_historisation->date_entered;
                        $formDate                  = substr($date, 6, 4) . "-" . substr($date, 3, 2) . "-" . substr($date, 0, 2) . " " . substr($date, 11, 7);
                        $tab_statut_prec[$formDate]['id_statut'] = $obj_historisation->ops_statut_id;
                        $tab_statut_prec[$formDate]['id_histo']  = $obj_historisation->id;
                    }

                    if ($this->ops_statut_id == $obj_historisation->ops_statut_id) {

                        $obj_historisation->flag_retour = "0";
                        $obj_historisation->save();
                    }
                }

                ksort($tab_statut_prec);

                foreach ($tab_statut_prec as $key => $value) {

                    $ancienHisto = $value;
                }

                // Flag sur l'ancien histo du statut précédent 
                $obj_historisation = new OPS_historisation();
                $obj_historisation->retrieve($ancienHisto['id_histo']);
                $obj_historisation->flag_retour = "0";
                $obj_historisation->save();

                // Initialisation du nouveau statut
                $this->ops_statut_id  = $ancienHisto['id_statut'];
            }

            $statut = new OPS_statut();
            $unStatut = $statut->retrieve($this->ops_statut_id);

            $etapes = $unStatut->get_linked_beans('ops_etape_ops_statut', 'OPS_etape');
            foreach ($etapes as $etape) { // Actuellement 1 seule etape possible
            }

            // Déclenchement de l'action au changement de statut, action_statut dans OPS_statut
            $obj_statut = new OPS_statut();
            $obj_statut->retrieve($this->ops_statut_id);
            $test = $obj_statut->action_statut($this);

            $this->ops_etape_id = $etape->id;
            $this->date_avancement   =  $date;
            $testy = $this->save();

            // Historisation du nouveau statut 
            $uneHistorisation                 = new OPS_historisation();
            $uneHistorisation->canal           = ($type_modification === "mass") ? "action_masse" : "action_unitaire";
            $uneHistorisation->ops_etape_id  = $etape->id;
            $uneHistorisation->ops_statut_id = $this->ops_statut_id;
            $uneHistorisation->auteur         = $current_user->name;
            $uneHistorisation->flag_retour   = '1';

            // Création de la relation avec le module OPS_Subvention
            $uneHistorisation->ops_historisation_ops_dossier_name = $this->name;
            $uneHistorisation->ops_dossier_id = $this->id;

            // Flagé l'historisation si elle existe deja pour la meme etape et meme statut 
            $tabs_historiques = $this->get_linked_beans('ops_historisation_ops_dossier', 'OPS_historisation', '', '', '', '', "flag_retour = '1'");
            foreach ($tabs_historiques as $key => $value) {

                $ancienHisto = $value;
            }
            #	$ancienHisto = array_pop( $tabs_historiques ); // 

            $old_historisation = new OPS_historisation();
            $old_historisation->retrieve($ancienHisto->id);

            if ($uneHistorisation->ops_etape_id === $old_historisation->ops_etape_id && $uneHistorisation->ops_statut_id === $old_historisation->ops_statut_id) {
                $old_historisation->flag_retour = "0";
                $old_historisation->save();
            }

            // Sauvegarde de l'historisation
            $uneHistorisation->save();

            $statut_updated = true;
        }

        return $statut_updated;
    }


    /**
     * @access public
     * @name getChampsVue()
     * Fonction qui retourne les champs de la vue lié au dispositif
     * 
     *  @param array						   $obj_dispositif : Bean OPS_dispositif
     *  @return string|boolean				 $champs_vue : Tableau des champs du formulaire
     */
    public function getChampsVue($obj_dispositif)
    {

        // On récupere les vues associées au dispositif
        $vues = $obj_dispositif->get_linked_beans('ops_generateur_vue_ops_dispositif', 'OPS_generateur_vue');

        // On récupere la liste des champs de la vue 
        $onglets = (is_array($vues) && count($vues) == 1) ? $vues[0]->getListOnglets() : false;
        $champs_vue = ($onglets !== false) ? OPS_dossier::getChampsFromOnglets($onglets) : array();

        // Champs additionnels 
        $champs_additionnel_onglets = OPS_dossier::getChampsAdditionnelOnglets();

        return (is_array($champs_vue) && is_array($champs_additionnel_onglets)) ? array_merge($champs_vue, $champs_additionnel_onglets) : $champs_vue;
    }


    /**
     * @access public
     * @name createDossier()
     * Fonction pour créer un dossier
     * 
     *  @param array						   $champs_formulaire : Tableau des champs du formulaire
     *  @return string|boolean				 $dossier_id : retourne l'id du dossier crée sinon false
     */
    public function createDossier($champs_formulaire)
    {

        // Vérification des champs indispensable pour la création d'un dossier sur OpenSub
        $vérification_champs_formulaire = OPS_dossier::isValidChampsFormulaire($champs_formulaire);
        if ($vérification_champs_formulaire !== false) {
            $conditions = array(
                'type_tiers' => $champs_formulaire['type_tiers']
            );
            // On récupere le dispositif
            $obj_dispositif = BeanFactory::getBean('OPS_dispositif', $champs_formulaire['dispositif']);
            if (!empty($obj_dispositif->id)) {

                // On récupere les champs de la vue
                $champs_vue = OPS_dossier::getChampsVue($obj_dispositif);

                // On initialise les champs à créer ( Divise les champs en deux tableau basic / custom )
                $initialisation_dossier = OPS_dossier::initDataDossier($champs_formulaire, $champs_vue, $conditions);

                // On crée le dossier sur la base de champs effectif préremplis (fournis via formulaire vs champs disponible|possibles de la vue assiciée)
                $dossier_id = OPS_dossier::createBeanDossier($initialisation_dossier);
            } else {
                $GLOBALS['log']->fatal(" OPS_dossier :: createDossier => Erreur de récupération du dispositif. ");
            }
        } else {
            $GLOBALS['log']->fatal(" OPS_dossier :: createDossier => Erreur vérification des champs indispensable pour la création d'un dossier. ");
        }

        return (!empty($dossier_id)) ? $dossier_id : false;
    }

    /**
     * @access public
     * @name editDossier()
     * Fonction pour met à jour un dossier
     * 
     *  @param string						  $dossier_id : L'id du dossier à modifier
     *  @param array						   $champs_formulaire : Tableau des champs du formulaire
     *  @return string|boolean				 $dossier_id : retourne l'id du dossier crée sinon false
     */
    public function editDossier($dossier_id, $champs_formulaire)
    {

        $updated = false;
        if (is_array($champs_formulaire) && count($champs_formulaire) > 0 && !empty($dossier_id)) {

            // On récupere le dossier
            $obj_dossier = BeanFactory::getBean('OPS_dossier', $dossier_id);
            if (!empty($obj_dossier->id)) {

                // On récupere le dispositif ACTUEL et non le nouveau transmis (si il est modifié !).
                $obj_dispositif = BeanFactory::getBean('OPS_dispositif', $obj_dossier->ops_dispositif_id); // L'éventuel nouveau dispositif choisi : $champs_formulaire['dispositif']
                if (!empty($obj_dispositif->id)) {

                    // On récupere les champs de la vue lié au dispositif
                    $champs_vue = OPS_dossier::getChampsVue($obj_dispositif);


                    // On initialise les champs à créer ( Divise les champs en deux tableau basic / custom )
                    $champs_dossier = OPS_dossier::getDataDossier($champs_formulaire, $champs_vue, array('type_tiers' => $obj_dossier->type_tiers));


                    $updated = OPS_dossier::editBeanDossier($obj_dossier, $champs_dossier);
                } else {
                    $GLOBALS['log']->fatal(" OPS_dossier :: editDossier => Erreur de récupération du dispositif. ");
                }
            } else {
                $GLOBALS['log']->fatal(" OPS_dossier :: editDossier => Erreur de récupération du dossier. ");
            }
        } else {
            $GLOBALS['log']->fatal(" OPS_dossier :: editDossier => Erreur vérification des champs indispensable pour la création d'un dossier. ");
        }

        return $updated;
    }

    /**
     * @access private
     * @name editBeanDossier()
     * Fonction qui crée l'objet dossier
     * 
     *  @param array			$onglets : la liste des onglets rattaché à la vue
     *  @return string		  $btn_onglets_html : code html , les boutons sont groupés dans une div
     */
    private function editBeanDossier($obj_dossier, $champs_dossier)
    {

        $relations = array();
        // On parcours les champs basic 
        foreach ($champs_dossier['basic'] as $champ_name => $champ_value) {

            // Traite les champs de type 'relation' apparteneant au type basic.
            if (is_array($champ_value)) {

                if (!empty($champ_value['type']) && $champ_value['type'] == "relation") {

                    if (array_key_exists("parent_id", $champ_value) && $champ_value['parent_id'] == "true") {
                        // Cas de relation indirecte d'un module à un parent.
                        //
                        $obj_dossier->$champ_name = $champ_value['module_id']; // Re-associe le module_id au champ
                    } else {
                        // Autres cas spécifiques de ralation par liaison..
                        //
                        // Users
                        $insertion_direct = array("OPS_personne_morale", "OPS_individu", "OPS_dispositif", "OPS_campagne", "OPS_exercice", "OPS_elu");
                        if (in_array($champ_value['module_name'], $insertion_direct)) {
                            $obj_dossier->{strtolower($champ_value['module_name']) . "_id"} = $champ_value['module_id'];
                        }

                        // Elu
                        if ($champ_value['module_name'] === "OPS_elu") {

                            $elus = (!empty($obj_dossier->id)) ? $obj_dossier->get_linked_beans("ops_elu_ops_dossier", "OPS_elu") : array();
                            if (is_array($elus) && count($elus) > 0) {
                                foreach ($elus as $elu) {
                                    $obj_dossier->load_relationship("ops_elu_ops_dossier");
                                    $obj_dossier->ops_elu_ops_dossier->delete($obj_dossier->id, $elu);
                                }
                            }

                            $module_ids = array();
                            if ($champ_value['unique'] === "multi" && strpos($champ_value['module_id'], '|') !== false) {
                                $champ_module_ids = explode("|", $champ_value['module_id']);
                                foreach ($champ_module_ids as $champ_module_id) {
                                    if (!empty($champ_module_id)) {
                                        $module_ids[] = $champ_module_id;
                                    }
                                }
                            } else {
                                $module_ids[] = $champ_value['module_id'];
                            }

                            foreach ($module_ids as $module_id) {

                                $obj_dossier->load_relationship("ops_elu_ops_dossier");
                                $obj_dossier->ops_elu_ops_dossier->add($module_id);
                            }
                        }

                        // Réunion
                        if ($champ_value['module_name'] === "OPS_reunion") {
                            $relations[] = $champ_value;
                            /*
							$reunions = (!empty($obj_dossier->id)) ? $obj_dossier->get_linked_beans("ops_reunion_ops_dossier", "OPS_reunion") : array();
							if (is_array($reunions) && count($reunions) > 0) {
								foreach ($reunions as $reunion) {
									$obj_dossier->load_relationship("ops_reunion_ops_dossier");
									$obj_dossier->ops_reunion_ops_dossier->delete($obj_dossier->id, $reunion);
								}
							}
							$obj_dossier->load_relationship("ops_reunion_ops_dossier");
							$obj_dossier->ops_reunion_ops_dossier->add($champ_value['module_id']);
							*/
                        }

                        // Commision
                        if ($champ_value['module_name'] === "OPS_commission") {
                            $commissions = (!empty($obj_dossier->id)) ? $obj_dossier->get_linked_beans("ops_commission_ops_dossier", "OPS_commission") : array();
                            if (is_array($commissions) && count($commissions) > 0) {
                                foreach ($commissions as $commission) {
                                    $obj_dossier->load_relationship("ops_commission_ops_dossier");
                                    $obj_dossier->ops_commission_ops_dossier->delete($obj_dossier->id, $commission);
                                }
                            }
                            $obj_dossier->load_relationship("ops_commission_ops_dossier");
                            $obj_dossier->ops_commission_ops_dossier->add($champ_value['module_id']);
                        }
                    }


                    /*
					if ( array_key_exists( "parent_id", $champ_value) && $champ_value['parent_id'] == "true" ){
						$obj_dossier->$champ_name = $champ_value['module_id'];
					}else{
						if( array_key_exists( "unique", $champ_value) && $champ_value['unique'] == "unique" ){


							if( $champ_value['module_name'] === "OPS_personne_morale" || $champ_value['module_name'] === "OPS_individu" || $champ_value['module_name'] === "OPS_dispositif" ){
								$obj_dossier->{strtolower( $champ_value['module_name'] )."_id"} = $champ_value['module_id'] ;
							}
						  
							if ( $obj_dossier->isRelationExist( $champ_value ) === false ){
								// La relation n'existe pas, on supprime l'ancienne relation et on crée la nouvelle
								OPS_dossier::deleteOldRelations( $obj_dossier , $champ_value);
								$obj = BeanFactory::getBean( $champ_value['module_name'], $champ_value['module_id'] );
								if ( !empty($obj->id) ){
									$obj->load_relationship($champ_value['relation_name']);
									$obj->{$champ_value['relation_name']}->add($obj_dossier->id);
								}
							}
						  
						}else{
							// Les relations de type multi non géré
							$GLOBALS['log']->fatal(" ---------------------------------- Relation de type multi ---------------------------------- ");

						}
					}
					*/
                }
            }
            // Traite les champs normaux apparteneant au type basic.
            else {
                $obj_dossier->$champ_name = $champ_value;
            }
        }



        // On récpère les champs déjà existant avec leur valeurs, qu'on va merger avec les nouvelles valeurs transmises.
        $old_champs_custom = (isset($obj_dossier->champs_custom) && !empty($obj_dossier->champs_custom)) ? json_decode(base64_decode($obj_dossier->champs_custom), true) : array();
        $new_champs_custom = isset($champs_dossier['custom']) ? $champs_dossier['custom'] :  array();

        // Tous les champs custom du formulaire de la vue doivent être soit fournis soit remplis avec les valeurs existantes.
        $champs_custom = array_merge($old_champs_custom, $new_champs_custom);


        $obj_dossier->champs_custom = base64_encode(json_encode($champs_custom));

        $obj_dossier->name = (!empty($obj_dossier->libelle_dossier)) ? $obj_dossier->num_dossier . " - " . $obj_dossier->libelle_dossier : $obj_dossier->num_dossier;

        if ($obj_dossier->save()) {
            if (is_array($relations) && count($relations) > 0) {
                foreach ($relations as $relation) {
                    OPS_reunionModel::deleteRelations($obj_dossier->id);
                    if (!empty($relation['module_id'])) {
                        OPS_reunionModel::addRelationDossier($obj_dossier->id, $relation['module_id']);
                    }
                }
            }
            return $obj_dossier->id;
        } else {
            return false;
        }
    }


    /**
     * @access private
     * @name deleteOldRelations()  
     *				   
     * Fonction qui supprime les relations entre le module dossier et un autre module
     *
     *  @param array					$champ: La définition du champ dans le référentiel 
     *  @return boolean				 $statut: true ou false
     */
    private function deleteOldRelations($obj_dossier, $champ)
    {

        $liste_related_module = (!empty($obj_dossier->id)) ? $obj_dossier->get_linked_beans($champ['relation_name'], $champ['module_name']) : array();
        if (is_array($liste_related_module) && count($liste_related_module) > 0) {
            foreach ($liste_related_module as $related_module) {
                if ($related_module->load_relationship($champ['relation_name'])) {
                    $related_module->load_relationship($champ['relation_name']);
                    $related_module->{$champ['relation_name']}->delete($related_module->id, $obj_dossier);
                }
            }
        }
    }

    /**
     * @access private
     * @name isRelationExist()  
     *				   
     * Fonction qui vérifie si un module est associé au module OPS_dossier
     *
     *  @param array					$champ: Définition du champ dans le ref avec l'id du module concerné
     *  @return boolean				 $statut : true ou false
     */
    private function isRelationExist($champ)
    {

        $exist = false;
        $liste_related_module = (!empty($this->id)) ? $this->get_linked_beans($champ['relation_name'], $champ['module_name']) : false;
        if (is_array($liste_related_module) && count($liste_related_module) > 0) {
            foreach ($liste_related_module as $related_module) {
                if ($champ['module_id'] == $related_module->id) {
                    $exist = true;
                }
            }
        }

        return $exist;
    }

    /**
     * @access private
     * @name getChampsAdditionnelOnglets()  
     *				   
     * Fonction qui les champs additionel non afficher dans la vue dossier
     *
     *  @return array				 $champs_additionnel_onglets : Champs additionnels
     */
    private function getChampsAdditionnelOnglets()
    {
        $champs_additionnel_onglets['type_tiers'] = array(
            'name' => 'type_tiers',
            'libelle' => 'Type tiers',
            'obligatoire' => 1,
            'type' => 'text',
            'cle' => '',
            'ineditable' => 0,
            'aide' => '',
            'defaut' => '',
            'propriete_1' => '',
            'propriete_2' => '',
            'propriete_3' => '',
            'propriete_4' => '',
            'description' => 'basic',
            'params' => '',
        );
        return $champs_additionnel_onglets;
    }

    /**
     * @access private
     * @name isValidChampsFormulaire()  
     *				   
     * Fonction qui retourne apres vérification la valeur d'un champ de type "BoutonRadio"
     *
     *  @param array					$champs_formulaire: Les champs retournés par le formulaire
     *  @return boolean				 $statut_vérification : true ou false
     */
    private function isValidChampsFormulaire($champs_formulaire)
    {

        // Si le type de la variable $champs_formulaire est différent de array
        if (is_array($champs_formulaire) === false) {
            return false;
        }

        // Si le tableau $champs_formulaire est vide
        if (count($champs_formulaire) == 0) {
            return false;
        }

        // Si on ne dispose pas du champ type_tiers
        if (empty($champs_formulaire['type_tiers'])) {
            return false;
        }

        // Si on ne dispose pas du champ dispositif
        if (empty($champs_formulaire['dispositif'])) {
            return false;
        }

        return true;
    }

    /**
     * @access private
     * @name createBeanDossier()
     * Fonction qui crée l'objet dossier
     * 
     *  @param array			$onglets : la liste des onglets rattaché à la vue
     *  @return string		  $btn_onglets_html : code html , les boutons sont groupés dans une div
     */
    private function createBeanDossier($champs_dossier)
    {

        if (empty($champs_dossier['basic'])) {
            $GLOBALS['log']->fatal(" OPS_dossier :: createBeanDossier => Erreur d'initialisation des données le dossier n'a pas été crée " . print_r($champs_dossier, true));
            return false;
        }

        $obj_dossier = BeanFactory::newBean('OPS_dossier');
        $id = $obj_dossier->save(false);


        /*
$GLOBALS['log']->fatal( str_repeat("-",50)."\n". " OPS_dossier :: createBeanDossier =>  ".print_r( array(
	'champs_dossier' => $champs_dossier
), true));
*/

        // On parcours les champs basic (disponibles via le formulaire -alias Vue-)...
        foreach ($champs_dossier['basic'] as $champ_name => $champ_value) {

            if (!empty($champ_value)) {
                if (is_array($champ_value)) {
                    if (!empty($champ_value['type']) && $champ_value['type'] == "relation") {
                        if (!empty($champ_value['parent_id']) && $champ_value['parent_id'] == "true") {

                            $obj_dossier->$champ_name = $champ_value['module_id'];
                        } else {

                            $module_ids = array();
                            if ($champ_value['unique'] === "multi" && strpos($champ_value['module_id'], '|') !== false) {
                                $champ_module_ids = explode("|", $champ_value['module_id']);
                                foreach ($champ_module_ids as $champ_module_id) {
                                    if (!empty($champ_module_id)) {
                                        $module_ids[] = $champ_module_id;
                                    }
                                }
                            } else {
                                $module_ids[] = $champ_value['module_id'];
                            }

                            foreach ($module_ids as $module_id) {
                                $obj = BeanFactory::getBean($champ_value['module_name'], $module_id);
                                if (!empty($obj->id)) {
                                    $obj->load_relationship($champ_value['relation_name']);
                                    $obj->{$champ_value['relation_name']}->add($id);
                                }
                            }
                        }
                    }
                } else {
                    $obj_dossier->$champ_name = $champ_value;
                }
            }
        }

        // On récpère les champs déjà existant avec leur valeurs, qu'on va merger avec les nouvelles valeurs transmises.
        $old_champs_custom = (isset($obj_dossier->champs_custom) && !empty($obj_dossier->champs_custom)) ? json_decode(base64_decode($obj_dossier->champs_custom), true) : array();
        $new_champs_custom = isset($champs_dossier['custom']) ? $champs_dossier['custom'] :  array();

        // Tous les champs custom du formulaire de la vue doivent être soit fournis soit remplis avec les valeurs existantes.
        $champs_custom = array_merge($old_champs_custom, $new_champs_custom);


        // TODO MERGE : Anomalie #834 
        /*
			$GLOBALS['log']->fatal( str_repeat("-",50)."\n". " OPS_dossier :: createBeanDossier =>  ".print_r( array(
				'old_champs_custom' => $old_champs_custom,
				'new_champs_custom' => $new_champs_custom,
				'champs_dossier'    => $champs_custom,
			), true));
			*/

        // Extrait les champs custom de la vue demandée, et les préremplis avec leur valeur par défaut... Les surcharge avec les champs custom mergés.
        //
        // Attention Préremplir avec les mergés ou seulement les nouveau ? Etude technique en cours avant portage.


        $obj_dossier->champs_custom = base64_encode(json_encode($champs_custom));


        // On lance les autres traitements associé à la création du dossier...
        $obj_dossier = OPS_dossier::autreTraitementCreationDossier($obj_dossier, $champs_dossier);


        // On enregistre une fois l'objet dossier alimenté et les traitements annexes effectué avec succés.
        return ($obj_dossier->save()) ? $id : false;
    }

    /**
     * @access private
     * @name autreTraitementCreationDossier()
     * Fonction qui 
     * 
     *  @param bean			 $obj_dossier : objet OPS_dossier
     *  @param array			$dossier : tableau des données du dossier
     *  @return bean			$obj_dossier : objet OPS_dossier
     */
    private function autreTraitementCreationDossier($obj_dossier, $dossier)
    {

        // Initialisation du statut et de l'étape du dossier
        $obj_dossier = OPS_dossier::initStatutEtapeDossier($obj_dossier, $dossier);

        // Initialisation du numéro du dossier
        $obj_dossier = OPS_dossier::initNumAndNameDossier($obj_dossier, $dossier);

        return $obj_dossier;
    }

    /**
     * @access private
     * @name initStatutEtapeDossier()
     * Fonction qui initialise l'étape et le statut du dossier
     * 
     *  @param bean			 $obj_dossier : objet OPS_dossier
     *  @param array			$dossier : tableau des données du dossier
     *  @return bean			$obj_dossier : objet OPS_dossier
     */
    private function initStatutEtapeDossier($obj_dossier, $dossier)
    {

        // On récupere le dispositif
        $obj_dispositif = (!empty($dossier['basic']['dispositif']['module_id'])) ? BeanFactory::getBean('OPS_dispositif', $dossier['basic']['dispositif']['module_id']) : false;

        // On récupere le guide d'instruction associé au dispositif
        $liste_guide_instruction = (!empty($obj_dispositif->id)) ? $obj_dispositif->get_linked_beans('ops_guide_instruction_ops_dispositif', 'OPS_guide_instruction') : false;
        $guide_instruction = (is_array($liste_guide_instruction) && count($liste_guide_instruction) == 1) ? $liste_guide_instruction[0] : false;

        // On récupére le statut de démarrage du guide d'instruction
        $guide_instruction_statut_initial = ($guide_instruction !== false && !empty($guide_instruction->ops_statut_id)) ? $guide_instruction->ops_statut_id : false;

        // On initialise le premier statut du dossier (GI)
        $obj_dossier->setFirstStatut($guide_instruction_statut_initial);

        return $obj_dossier;
    }


    /**
     * @access private
     * @name initNumAndNameDossier()
     * Fonction qui initialise le numéro du dossier ( champ : num_dossier )
     * 
     *  @param bean			 $obj_dossier : objet OPS_dossier
     *  @param array			$dossier : tableau des données du dossier
     *  @return bean			$obj_dossier : objet OPS_dossier
     */
    private function initNumAndNameDossier($obj_dossier, $dossier)
    {

        // On récupere le numéro du dernier dossier
        $last_num_dossier = $obj_dossier->getLastNumDossier();

        // On incrémente le numéro si != de false sinon on initialise le champ à 1 ( La vérification se fait en amont )
        $obj_dossier->num_dossier = ($last_num_dossier !== false) ? $last_num_dossier + 1 : 1;

        // On initialise le nom du dossier 
        $obj_dossier->name = (!empty($obj_dossier->libelle_dossier)) ? $obj_dossier->num_dossier . " - " . $obj_dossier->libelle_dossier : (string)$obj_dossier->num_dossier;

        return $obj_dossier;
    }

    /**
     * @access private
     * @name getChampsFromOnglets()
     * Fonction qui retourne le tableau des champs de la vue
     * 
     *  @param array			$onglets : Tableau des onglets et des champs associé à la vue dossier 
     *  @return string		  $champs : définition des champs dans le vue dossier
     */
    private function getChampsFromOnglets($onglets)
    {
        $champs = array();
        foreach ($onglets as $id_onglet => $onglet) {
            foreach ($onglet['champs'] as $id_ligne => $ligne) {
                foreach ($ligne as $id_champ => $champ) {
                    if (!empty($champ['name'])) {
                        $champs[$champ['name']] =  $champ;
                    }
                }
            }
        }
        return $champs;
    }

    /**
     * @access private
     * @name initDataDossier()
     * Fonction qui génére les boutons d'onglets 
     * 
     *  @param array			$onglets : la liste des onglets rattaché à la vue
     *  @return string		  $btn_onglets_html : code html , les boutons sont groupés dans une div
     */
    //                                 $champs_formulaire, $champs_vue, $conditions
    private function initDataDossier($champs_formulaire, $champs_onglets, $conditions)
    {

        /*
$GLOBALS['log']->fatal( " OPS_dossier :: initDataDossier =>  ".print_r( array(
	'champs_formulaire' => $champs_formulaire,
	'champs_onglets'    => $champs_onglets
), true));
*/

        $errors         = array();
        $champs_dossier = array(
            'basic'  => array(),
            'custom' => array(),
            'unknow' => array()
        );


        // On parcours les champs de la vue du dispositif associée à ce dossier...
        foreach ($champs_onglets as $field_slug_id => $field_params) {

            // Définition du type de champ [basic | custom]
            $field_type = $field_params['description'];

            // On récupère la valeur par défaut du champ..
            $field_default_value = OPS_dossier::getDefautValueChamp($field_params);


            // Si le champ existe dans les champs du formulaire (envoyé), sinon affecte la valeur par défaut.
            if (array_key_exists($field_slug_id, $champs_formulaire)) {

                // La valeur transmise doit être conforme :
                // - Fournie si obligatoire, sinon on utilise la valeur par défaut 
                if (empty($champs_formulaire[$field_slug_id]) && $field_params['obligatoire'] === 1) {

                    $champs_dossier[$field_type][$field_slug_id] = $field_default_value;
                }

                // #911 Gestion du cas ou la valeur du champ de type "checkbox" est vide [ Création par le connecteur ]
                if (empty($champs_formulaire[$field_slug_id]) &&  $field_params['type'] === "checkbox") {
                    $champs_formulaire[$field_slug_id] = "0";
                }

                // On récupère la valeur issue du type de champ
                if (!empty($champs_formulaire[$field_slug_id]) || strval($champs_formulaire[$field_slug_id]) === "0") {

                    $field_correct_value = OPS_dossier::getValueChampByType($champs_formulaire[$field_slug_id], $field_params, $conditions);
                    //		$GLOBALS['log']->fatal( " OPS_dossier :: initDataDossier (Compare à la Vue) => Valeur testée: [". $champs_formulaire[$field_slug_id] . "] retournée: [" . $field_correct_value . "]" );

                    if ($field_correct_value !== false) { // 

                        $champs_dossier[$field_type][$field_slug_id] = $field_correct_value; // On effecte la nouvelle valeur récoltée.

                    } else {

                        $errors[] = "Echec lors de la récupération de la valeur du champ [" . $field_params['libelle'] . "] de type « " . $field_params['type'] . " »";
                    }
                } else {
                    $champs_dossier[$field_type][$field_slug_id] = $field_default_value;
                }
            }
            // Ce champ est absent (non envoyé), donc affetcte la valeur par défaut.
            else {

                $champs_dossier[$field_type][$field_slug_id] = $field_default_value;
            }
        }


        // A présent il faut néanmoins préserver les champs transmis absent dans la vue :
        // - Mais presents dans le référentiel -> a classer dans [basic | custom]
        // - Mais absents du référentiel -> a classer dans [unknow]

        $referentiel_fields = OPS_generateur_referentiel::getListReferentiel();
        $referentiel_fields_slugs = array_column($referentiel_fields, 'name');


        // On parcours les champs transmis via le formulaire, certains ne sont pas disponibles dans la vue associée...
        foreach ($champs_formulaire as $field_slug_id => $field_value) {

            // Ce champ existe t-il dans le référentiel ?
            $field_index = array_search($field_slug_id, $referentiel_fields_slugs);
            if ($field_index !== false) {

                // On se simplifie avec ses params...
                $field_params = $referentiel_fields[$field_index];

                // Définition du type de champ [basic | custom]
                $field_type = $field_params['description'];

                // On récupère la valeur par défaut du champ..
                $field_default_value = OPS_dossier::getDefautValueChamp($field_params);

                if (isset($champs_dossier[$field_type][$field_slug_id])) {
                    // Ce champ existe déjà dans le flow de champs, il a donc été mappé via la vue :)
                } else {

                    // A ce stade, le champ transmis via le formulaire n'est pas présent dans la composition du formulaire de vue, mais existe dans le référentiel...
                    // On va donc le récupérer correctement.

                    // La valeur transmise doit être conforme :
                    // - Fournie si obligatoire, sinon on utilise la valeur par défaut 
                    if (empty($champs_formulaire[$field_slug_id]) && $field_params['obligatoire'] === 1) {

                        $champs_dossier[$field_type][$field_slug_id] = $field_default_value;
                    }

                    // On récupère la valeur issue du type de champ
                    if (!empty($champs_formulaire[$field_slug_id]) || strval($champs_formulaire[$field_slug_id]) === "0") {

                        $field_correct_value = OPS_dossier::getValueChampByType($champs_formulaire[$field_slug_id], $field_params, $conditions);

                        if ($field_correct_value !== false) { // 

                            $champs_dossier[$field_type][$field_slug_id] = $field_correct_value; // On effecte la nouvelle valeur récoltée.

                        } else {

                            $errors[] = "Echec lors de la récupération de la valeur du champ [" . $field_params['libelle'] . "] de type « " . $field_params['type'] . " »";
                        }
                    } else {
                        $champs_dossier[$field_type][$field_slug_id] = $field_default_value;
                    }
                }
            }
        }



        /*
$GLOBALS['log']->fatal( str_repeat("-",50)."\n". " OPS_dossier :: initDataDossier => RETOUR ".print_r( array(
	'champs_dossier' => $champs_dossier,
	'errors' => $errors
), true));
*/


        return (sizeof($errors) > 0) ? $errors : $champs_dossier;
    }

    /**
     * @access private
     * @name getDefautValueChamp()
     * Fonction qui retourne la valeur par défaut du champ sinon empty
     * 
     *  @param array			$champ_onglet : Parametres du champ dans le générateur 
     *  @return string		  $valeur_defaut : la valeur par défaurt du champ sinon empty
     */
    private function getDefautValueChamp($champ_onglet)
    {

        $defaut_value = "";
        if (!empty($champ_onglet['defaut'])) {
            if ($champ_onglet['type'] === "date") {
                $date = new DateTime($champ_onglet['defaut']);
                $defaut_value = $date->format("Y-m-d");
            } else {
                $defaut_value = $champ_onglet['defaut'];
            }
        }
        return $defaut_value;;
    }

    /**
     * @access private
     * @name getDataDossier()
     * Fonction qui génére les boutons d'onglets 
     * 
     *  @param array			$onglets : la liste des onglets rattaché à la vue
     *  @return string		  $btn_onglets_html : code html , les boutons sont groupés dans une div
     */
    private function getDataDossier($champs_formulaire, $champs_onglets, $conditions)
    {

        $erreurs = array();
        $dossier = array();

        // On parcours les champs de la vue dossier
        foreach ($champs_onglets as $champ_onglet_id => $champ_onglet) {
            // Si le champ existe dans les champs du formulaire
            if (array_key_exists($champ_onglet_id, $champs_formulaire)) {
                // Si le champ du formulaire n'est pas vide
                if (!empty($champs_formulaire[$champ_onglet_id]) || $champs_formulaire[$champ_onglet_id] === "0") {
                    // On récupere la valeur du champ
                    $value = OPS_dossier::getValueChampByType($champs_formulaire[$champ_onglet_id], $champ_onglet, $conditions);
                    if ($value !== false) {
                        $dossier[$champ_onglet['description']][$champ_onglet_id] = $value;
                    } else {
                        $erreurs[] = "Echec de la récupération de la valeur du champ " . $champ_onglet['libelle'];
                    }
                } else {
                    // Si le champ est obligatoire et vide, on ajoute l'erreur au tableau d'erreurs
                    if ($champ_onglet['obligatoire'] != 1) {
                        $dossier[$champ_onglet['description']][$champ_onglet_id] = OPS_dossier::getChampVidebyType($champ_onglet, $conditions);
                    } else {
                        $erreurs[] = "Le champ " . $champ_onglet['libelle'] . " est obligatoire.";
                    }
                }
            }
        }

        return (is_array($erreurs) && count($erreurs) > 0) ? $erreurs : $dossier;
    }

    /**
     * @access private
     * @name getChampVidebyType()
     * Fonction qui retourne la valeur du champ selon le type
     * 
     *  @param array			$champ_onglet : Définiation du champ dans la vue dossier
     *  @return string|boolean|array			$value : valeur du champ 
     */
    private function getChampVidebyType($champ_onglet, $conditions)
    {

        $value = "";
        switch ($champ_onglet['type']) {
            case 'relation': {
                    $value = OPS_dossier::getValueChampRelation("", $champ_onglet, $conditions);
                    break;
                }
            case 'pourcentage': {
                    0;
                    break;
                }
            case 'bouton_radio': {
                    $value = 0;
                    break;
                }
            case 'checkbox': {
                    $value = 0;
                    break;
                }
            case 'date': {
                    $value = null;
                    break;
                }
            case 'numerique': {
                    $value = 0;
                    break;
                }
            case 'montant': {
                    $value = 0.00;
                    break;
                }
        }
        return $value;
    }

    /**
     * @access private
     * @name getValueChampByType()
     * Fonction qui retourne la valeur du champ selon le type
     * 
     *  @param array			$champ_formulaire_valeur : la valeur du champ dans le formulaire
     *  @param array			$champ_onglet : Définiation du champ dans la vue dossier
     *  @param array			$conditions : liste des conditions
     *  @return string|boolean|array			$value : valeur du champ 
     */
    private function getValueChampByType($champ_formulaire_valeur, $champ_onglet, $conditions)
    {

        $value = false;

        switch ($champ_onglet['type']) {

            case 'relation': {
                    $value = OPS_dossier::getValueChampRelation($champ_formulaire_valeur, $champ_onglet, $conditions);
                    break;
                }

            case 'text': {
                    $value = OPS_dossier::getValueChampText($champ_formulaire_valeur);
                    break;
                }
            case 'text_long': {
                    $value = OPS_dossier::getValueChampTextLong($champ_formulaire_valeur);
                    break;
                }
            case 'chaine_libre': {
                    $value = OPS_dossier::getValueChampChaineLibre($champ_formulaire_valeur);
                    break;
                }

            case 'numerique': {
                    $value = OPS_dossier::getValueChampNumerique($champ_formulaire_valeur);
                    break;
                }
            case 'montant': {
                    $value = OPS_dossier::getValueChampMontant($champ_formulaire_valeur);
                    break;
                }
            case 'pourcentage': {
                    $value = OPS_dossier::getValueChampPourcentage($champ_formulaire_valeur);
                    break;
                }
            case 'date': {
                    $value = OPS_dossier::getValueChampDate($champ_formulaire_valeur);
                    break;
                }
            case 'telephone': {
                    $value = OPS_dossier::getValueChampTelephone($champ_formulaire_valeur);
                    break;
                }
            case 'adresse': {
                    $value = OPS_dossier::getValueChampAdresse($champ_formulaire_valeur);
                    break;
                }
            case 'lien': {
                    $value = OPS_dossier::getValueChampLien($champ_formulaire_valeur);
                    break;
                }

            case 'bouton_radio': {
                    $value = OPS_dossier::getValueChampBoutonRadio($champ_formulaire_valeur);
                    break;
                }
            case 'checkbox': {
                    $value = OPS_dossier::getValueChampCheckbox($champ_formulaire_valeur);
                    break;
                }
            case 'liste': {
                    $value = OPS_dossier::getValueChampListe($champ_formulaire_valeur);
                    break;
                }
            case 'liste_choix_simple': {
                    $value = OPS_dossier::getValueChampListeChoixSimple($champ_formulaire_valeur);
                    break;
                }
            case 'liste_choix_multi': {
                    $value = OPS_dossier::getValueChampListeChoixSimple($champ_formulaire_valeur);
                    break;
                }
            case 'wysiwyg': {
                    $value = OPS_dossier::getValueChampWysiwyg($champ_formulaire_valeur);
                    break;
                }
            case 'astre': {
                    $value = OPS_dossier::getValueChampAstre($champ_formulaire_valeur);
                    break;
                }
        }

        return $value;
    }

    /**
     * @access private
     * @name getValueChampText()
     * Fonction qui retourne apres vérification la valeur d'un champ de type "text"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampText($champ_formulaire_valeur)
    {
        return (is_string($champ_formulaire_valeur) && !empty($champ_formulaire_valeur)) ? $champ_formulaire_valeur : false;
    }

    /**
     * @access private
     * @name getValueChampTextLong()
     * Fonction qui retourne apres vérification la valeur d'un champ de type "text long"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampTextLong($champ_formulaire_valeur)
    {
        return (is_string($champ_formulaire_valeur) && !empty($champ_formulaire_valeur)) ? $champ_formulaire_valeur : false;
    }

    /**
     * @access private
     * @name getValueChampWysiwyg()
     * Fonction qui retourne apres vérification la valeur d'un champ de type "wysiwyg"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampWysiwyg($champ_formulaire_valeur)
    {
        return (is_string($champ_formulaire_valeur) && !empty($champ_formulaire_valeur)) ? $champ_formulaire_valeur : false;
    }

    /**
     * @access private
     * @name getValueChampAstre()
     * Fonction qui retourne apres vérification la valeur d'un champ de type "astre"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampAstre($champ_formulaire_valeur)
    {
        return (is_string($champ_formulaire_valeur) && !empty($champ_formulaire_valeur)) ? $champ_formulaire_valeur : false;
    }

    /**
     * @access private
     * @name getValueChampRelation()
     * Fonction qui retourne apres vérification la valeur d'un champ de type "relation"
     *
     *  @param array						  $champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @param array						  $champ_onglet: La définition du champ dans la vue dossier
     *  @return string|array|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampRelation($champ_formulaire_valeur, $champ_onglet, $conditions)
    {

        $value = false;
        $json_formated = base64_decode($champ_onglet['params']);
        $retour_json_to_array = ODE::jsonToArray($json_formated);
        if ($retour_json_to_array['statut'] == "ok") {
            $data_module = $retour_json_to_array['data'];
            if ($data_module['type'] == "table") {
                if (!empty($data_module['condition'])) {
                    $condition = (array_key_exists($data_module['condition'], $conditions)) ? $conditions[$data_module['condition']] : false;
                    foreach ($data_module['module_name'] as $key => $module) {
                        if ($module['value'] == $condition) {
                            $value['module_name'] = $module['name'];
                        }
                    }
                    foreach ($data_module['relation_name'] as $key => $relation) {
                        if ($relation['value'] == $condition) {
                            $value['relation_name'] = $relation['name'];
                        }
                    }
                    $value['module_id'] = $champ_formulaire_valeur;
                    $value['type'] = "relation";
                    $value['unique'] = $data_module['unique'];
                } else {
                    $value['module_name'] = $data_module['module_name'];
                    $value['relation_name'] = $data_module['relation_name'];
                    $value['unique'] = $data_module['unique'];
                    $value['module_id'] = $champ_formulaire_valeur;
                    $value['type'] = "relation";
                }
            } else {
                // parent_id => insertion direct
                $value['module_name'] = $data_module['module_name'];
                $value['module_id'] = $champ_formulaire_valeur;
                $value['type'] = "relation";
                $value['parent_id'] = "true";
                $value['unique'] = $data_module['unique'];
            }
        }
        return $value;
    }

    /**
     * @access private
     * @name getValueChampAdresse()  
     * 
     * *********************************************************************
     * ************************ Aucune vérification ************************
     * *********************************************************************	 
     *				   
     * Fonction qui retourne apres vérification la valeur d'un champ de type "Adresse"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampAdresse($champ_formulaire_valeur)
    {
        return $champ_formulaire_valeur;
    }

    /**
     * @access private
     * @name getValueChampLien()  
     * 
     * *********************************************************************
     * ************************ Aucune vérification ************************
     * *********************************************************************	 
     *				   
     * Fonction qui retourne apres vérification la valeur d'un champ de type "Lien"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampLien($champ_formulaire_valeur)
    {
        return $champ_formulaire_valeur;
    }

    /**
     * @access private
     * @name getValueChampPourcentage()  
     * 
     * *********************************************************************
     * ************************ Aucune vérification ************************
     * *********************************************************************	 
     *				   
     * Fonction qui retourne apres vérification la valeur d'un champ de type "Pourcentage"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampPourcentage($champ_formulaire_valeur)
    {
        return $champ_formulaire_valeur;
    }

    /**
     * @access private
     * @name getValueChampBoutonRadio()  
     * 
     * *********************************************************************
     * ************************ Aucune vérification ************************
     * *********************************************************************	 
     *				   
     * Fonction qui retourne apres vérification la valeur d'un champ de type "BoutonRadio"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampBoutonRadio($champ_formulaire_valeur)
    {
        return $champ_formulaire_valeur;
    }

    /**
     * @access private
     * @name getValueChampCheckbox()  
     * 
     * *********************************************************************
     * ************************ Aucune vérification ************************
     * *********************************************************************	 
     *				   
     * Fonction qui retourne apres vérification la valeur d'un champ de type "Checkbox"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampCheckbox($champ_formulaire_valeur)
    {
        return $champ_formulaire_valeur;
    }

    /**
     * @access private
     * @name getValueChampChaineLibre()  
     * 
     * *********************************************************************
     * ************************ Aucune vérification ************************
     * *********************************************************************	 
     *				   
     * Fonction qui retourne apres vérification la valeur d'un champ de type "ChaineLibre"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampChaineLibre($champ_formulaire_valeur)
    {
        return $champ_formulaire_valeur;
    }

    /**
     * @access private
     * @name getValueChampDate()  
     *			
     * Fonction qui retourne apres vérification la valeur d'un champ de type "Date"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampDate($champ_formulaire_valeur)
    {
        // Si non vide, on formate la date au format SQL sinon vide
        return (!empty($champ_formulaire_valeur)) ? OdeDate::toSQL($champ_formulaire_valeur) : "";
    }

    /**
     * @access private
     * @name getValueChampListe()  
     * 
     * *********************************************************************
     * ************************ Aucune vérification ************************
     * *********************************************************************	 
     *				   
     * Fonction qui retourne apres vérification la valeur d'un champ de type "Liste"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampListe($champ_formulaire_valeur)
    {
        return $champ_formulaire_valeur;
    }

    /**
     * @access private
     * @name getValueChampListeChoixSimple()  
     * 
     * *********************************************************************
     * ************************ Aucune vérification ************************
     * *********************************************************************	 
     *				   
     * Fonction qui retourne apres vérification la valeur d'un champ de type "ListeChoixSimple"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampListeChoixSimple($champ_formulaire_valeur)
    {
        return $champ_formulaire_valeur;
    }

    /**
     * @access private
     * @name getValueChampNumerique()  
     * 
     * *********************************************************************
     * ************************ Aucune vérification ************************
     * *********************************************************************	 
     *				   
     * Fonction qui retourne apres vérification la valeur d'un champ de type "Numerique"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampNumerique($champ_formulaire_valeur)
    {
        return $champ_formulaire_valeur;
    }

    /**
     * @access private
     * @name getValueChampTelephone()  
     * 
     * *********************************************************************
     * ************************ Aucune vérification ************************
     * *********************************************************************	 
     *				   
     * Fonction qui retourne apres vérification la valeur d'un champ de type "Telephone"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampTelephone($champ_formulaire_valeur)
    {
        return $champ_formulaire_valeur;
    }

    /**
     * @access private
     * @name getValueChampMontant()  
     * 
     * *********************************************************************
     * ************************ Aucune vérification ************************
     * *********************************************************************	 
     *				   
     * Fonction qui retourne apres vérification la valeur d'un champ de type "Montant"
     *
     *  @param array					$champ_formulaire_valeur: La valeur du champ retourné par le formulaire
     *  @return string|boolean		  $champ_valeur: La valeur du champ vérifié ou false
     */
    private function getValueChampMontant($champ_formulaire_valeur)
    {
        $montant = str_replace(' ', '', $champ_formulaire_valeur);
        return str_replace(',', '.', $montant);
    }

    // Ajout des habilitations sur les vues listes DOSSIERS 
    function create_new_list_query($order_by, $where, $filter = array(), $params = array(), $show_deleted = 0, $join_type = '', $return_array = false, $parentbean = null, $singleSelect = false, $ifListForExport = false)
    {
        global $beanFiles, $current_user;

        $return_array = parent::create_new_list_query($order_by, $where, $filter, $params, $show_deleted, $join_type, $return_array, $parentbean, $singleSelect, $ifListForExport);


        $this->create_requete_dossier();

        // Préparation de la requete 
        $req_where = $_SESSION['ops_requete_dossier'];


        $getPreference = $current_user->getPreference("OPS_dossierQ");

        if ((!empty($_GET['filter']) &&  "nogps" == $_GET['filter'])  || ($getPreference['geoloc'] == "nogps" || $getPreference['filter'] == "nogps") && "" == $_GET['action']) {

            $getPreference["geoloc"] =  "npogps";
            unset($getPreference["filter"]);
            $current_user->setPreference("OPS_dossierQ", $getPreference);

            $getPreference = $current_user->getPreference("OPS_dossierQ");
            $return_array['where'] .= " AND (statut_geoloc = 'erreur' OR statut_geoloc = 'empty') ";
        } else {
            unset($getPreference["filter"]);
            $current_user->setPreference("OPS_dossierQ", $getPreference);
        }


        // On a joute le filtre pour la gestion des habilitation
        if (!empty($req_where)) {
            if (is_array($return_array)) {
                $return_array['where'] .= ' AND ' . $req_where;
            }
        }

        // Vérification si 'l'utilisateur a accès a la liste de dossiers 
        if (!$current_user->isAdmin() && $req_where == false) {
            $return_array['where'] = "  WHERE ops_dossier.id like '0' ";
        }

        if (empty($return_array['order_by'])) {
            $return_array['order_by'] =  " ORDER BY " . strtoupper('num_dossier') . ' DESC';
        }

        if (is_array($return_array)) {
            // On force l'ajout des champ utilise par la gestion de droit 
            $return_array['from']   .= ' LEFT JOIN ops_dispositif_ops_dossier ON ops_dispositif_ops_dossier.ops_dossier_id=ops_dossier.id AND ops_dispositif_ops_dossier.deleted=0';
        }

        return $return_array;
    }

    public function get_dossier_by_territoire($id_territoire)
    {

        global $db;


        if (!empty($id_territoire)) {

            $query = "SELECT DISTINCT(ops_dossier.id) 
				FROM ops_dossier 
				INNER JOIN ops_sous_territoire_ops_dossier sous_territoire_dossier ON ops_dossier.id = sous_territoire_dossier.ops_dossier_id AND sous_territoire_dossier.deleted=0
				INNER JOIN ops_sous_territoire sous_territoire ON sous_territoire_dossier.ops_sous_territoire_id = sous_territoire.id AND sous_territoire.deleted=0
				INNER JOIN ops_sous_territoire_ops_territoire sous_territoire_territoire ON sous_territoire.id = sous_territoire_territoire.ops_sous_territoire_id AND sous_territoire_territoire.deleted=0
				INNER JOIN ops_territoire territoire ON sous_territoire_territoire.ops_territoire_id = territoire.id AND territoire.deleted=0
				where ops_dossier.deleted = false
				AND territoire.id = '" . $id_territoire . "'";

            $liste_id_dossier = $db->query($query);

            while ($un_dossier = $db->fetchRow($liste_id_dossier)) {

                $tab[] = "'" . $un_dossier['id'] . "'";
            }

            $tab_id = implode(",", $tab);
        }

        return $tab_id;
    }

    public function get_dossier_without_territoire()
    {

        global $db;

        $query = "SELECT DISTINCT(ops_dossier.id) 
					FROM ops_dossier
					WHERE ops_dossier.id not in ( 
						Select  ops_sous_territoire_ops_dossier.ops_dossier_id
						from ops_sous_territoire_ops_dossier
						where ops_sous_territoire_ops_dossier.deleted = '0'
					)
					AND ops_dossier.deleted = '0'
					";

        $liste_id_dossier = $db->query($query);

        while ($un_dossier = $db->fetchRow($liste_id_dossier)) {

            $tab[] = "'" . $un_dossier['id'] . "'";
        }

        $tab_id = implode(",", $tab);


        return $tab_id;
    }


    /**
     * create_requete_dossier
     * Fonction du hook permettant les habilitations des localisations sur les utilisateurs 
     *
     * @param  mixed $bean
     * @param  mixed $event
     * @param  mixed $arguments
     *
     * @return void
     */
    function create_requete_dossier()
    {

        global $app_list_strings, $beanFiles, $current_user;
        require_once($beanFiles['OPS_dossier']);


        $isHabilite;

        // Préparation de la requete 
        $tab_localisation = $_SESSION['habilitation_localisation'];
        $nb_loca = count($tab_localisation);

        $tab_dispositif = $_SESSION['habilitation_dispositif'];
        $nb_dispo = count($tab_dispositif);

        //Création du where des dispositifs 
        if ($nb_dispo > 0) {

            $ids_dispo;

            foreach ($tab_dispositif as $id => $dispo) {

                // Vérification que l'utilisateur connecté est le droit de visualisation du dispositif 
                if ($dispo['visualisation']) {

                    // gestion du dispositif
                    $ids_dispo[] = "'" . $id . "'";
                }

                // gestion des étapes 
                if (array_key_exists("etapes", $dispo)) {

                    // Récupération des droits étapes 
                    foreach ($dispo['etapes'] as $id_etape => $etape) {

                        if ($etape['visualisation']) {

                            $ids_etape[]  = "'" . $id_etape . "'";

                            // Si l'étape correspond à un dispositif 
                            if (!in_array($id, $ids_dispo)) {

                                $ids_dispo[] = "'" . $id . "'";
                            }
                        }
                    }
                }
            }

            if (!empty($ids_dispo)) {

                $ids_dispo = implode(",", array_unique($ids_dispo));

                $where_dispo_dossier = " ops_dossier.id in ( select ops_dossier.id from ops_dossier LEFT JOIN ops_dispositif_ops_dossier ON ops_dispositif_ops_dossier.ops_dossier_id=ops_dossier.id AND ops_dispositif_ops_dossier.deleted=0 where  ops_dispositif_ops_dossier.ops_dispositif_id in (" . $ids_dispo . " ) )";

                // gestion des dispositifs 
                $where_dispo = " ops_dispositif.id in (" . $ids_dispo . " ) ";
            } else {

                // gestion des dispositifs 
                $where_dispo = " ops_dispositif.id = '0' ";
            }

            if (!empty($ids_etape)) {

                $ids_etape = implode(",", array_unique($ids_etape));
                $where_etape_dossier = "ops_dossier.ops_etape_id in (" . $ids_etape . " ) ";
            }
        }


        if (!empty($where_dispo_dossier) && !empty($where_etape_dossier)) {
            $isHabilite  = " ( " . $where_dispo_dossier . " and " . $where_etape_dossier . " ) ";
        } elseif (empty($where_dispo_dossier) && !empty($where_etape_dossier)) {
            $isHabilite  =  $where_etape_dossier;
        } elseif (!empty($where_dispo_dossier) && empty($where_etape_dossier)) {
            $isHabilite  =  $where_dispo_dossier;
        }

        // Création des dossiers localisés 

        // Si l'utilisateur n'a pas de territoire et qu'il n'a pas la case Dossiers non localisés coché alors il ne voit aucun dossier 
        if ($nb_loca == 0 &&  $current_user->dossiers_non_localises == false) {

            $isHabilite = false;
        } else { // L'utilisateur a des territoires alors on retreint les droits dans la req 


            foreach ($tab_localisation as $cle => $id_localisation) {


                $obj_dossier = new OPS_dossier();
                $id_dossiers .= $obj_dossier->get_dossier_by_territoire($id_localisation);

                $l++;
                if ($l < $nb_loca && !empty($id_dossiers)) {

                    $id_dossiers .= " , ";
                }
            }


            // L'utilisateur voit en plus les dossiers non localisés  
            if ($current_user->dossiers_non_localises == true) {

                if (!empty($id_dossiers)) {
                    $id_dossiers .= " , ";
                }

                // Récupération des dossiers non localisés
                $obj_dossier = new OPS_dossier();
                $id_dossiers .= $obj_dossier->get_dossier_without_territoire();
            }

            if (!empty($isHabilite)) {

                $isHabilite .= " and ";
            }

            $isHabilite .= " ops_dossier.id in ( '' " .  (!empty($id_dossiers) ? ',' : '') .  $id_dossiers . " ) ";
        }


        $_SESSION['ops_requete_dossier']  = $isHabilite;
        $_SESSION['ops_requete_dispositif']  = $where_dispo;

        // Règles permettant d'affiner la clause WHERE de la Query get_full_list{custom}()
        //
        // On ajoute les règles...
        $_SESSION['habilistations_rules'] = array(
            'sql_trigger' => 'ops_dossier.id', // Defines the master trigger that's ending the sql query.
            'sql_rules'   => array()
        );


        $_id_dossiers = preg_replace('/\s+/', '', $id_dossiers);
        $_ids_dispo   = empty($_id_dossiers) ?  '' : /**/ preg_replace('/\s+/', '', $ids_dispo);
        $_ids_etape   = empty($_id_dossiers) ?  '' : /**/  preg_replace('/\s+/', '', $ids_etape);

        $_SESSION['habilistations_rules']['sql_rules']['ops_dossier.id']                               = array_filter(explode(',', $_id_dossiers));

        // jt_dispdos == ops_dispositif_ops_dossier => jointure réalisé dans dossier_geo

        $_SESSION['habilistations_rules']['sql_rules']['jt_dispdos.ops_dispositif_id'] = array_filter(explode(',', $_ids_dispo));

        if (!empty(preg_replace('/\s+/', '', $ids_etape))) {
            $_SESSION['habilistations_rules']['sql_rules']['ops_dossier.ops_etape_id']                 = array_filter(explode(',', $_ids_etape));
        }
    }
}
