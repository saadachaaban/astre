
var customChamps = {

    demandeur: function(champ) {

        champ_html = '';
        var params = isReponseValid( b64_to_utf8(champ.params) );
        var module_name = getInputValue("demandeur_type_choisi");
        
        if (champ && isStringValid(module_name) ) {

            champ_html += '<div style="margin-left: -3%;" class="col-md-8">';
	        champ_html +=    '<div id="display_value_champ_demandeur" data-id-selected="" class="col-md-9" style="border-radius: 3px; height: 47px; background: #c3e7fd none repeat scroll 0 0 !important; border: 1px solid #b8daef; font-size: 14px; font-weight: 400; color: #534d64; padding: 12px!important;"></div>';
	        champ_html +=        '<div class="col-md-3" style="text-align: center;">';
		    champ_html +=            '<span class="id-ff multiple">';
			champ_html +=                '<button type="button" id="btn_statut_initial" title="Sélectionner" onclick="selectRelatedModule(\''+champ.name+'\',\''+module_name+'\',\''+params.type+'\',\''+params.unique+'\',\''+params.condition+'\')" class="button firstChild" value="Sélectionner">';
			champ_html +=	                '<img src="themes/SuiteP/images/id-ff-select.png?v=9PUULME3DHwNvxEgZrpwig">'
			champ_html +=	            '</button>'
            champ_html +=                '<button type="button" id="btn_clr_statut_initial" title="Clear Selection" onclick="deleteRelatedModule(\''+champ.name+'\',\''+params.unique+'\',\''+params.condition+'\')" class="button lastChild" value="Supprimer la selection">'
            champ_html +=                    '<img src="themes/SuiteP/images/id-ff-clear.png?v=9PUULME3DHwNvxEgZrpwig">'
			champ_html +=		        '</button>'
			champ_html +=	        '</span>'
			champ_html +=        '</div>'
			champ_html +=    '<input name="Demandeur" required="true" type="hidden" id="value_champ_demandeur">'
			champ_html += '</div>'
        }

        return champ_html;
    },
    dispositif: function(champ) {

        champ_html = '';

        var params = isReponseValid( b64_to_utf8(champ.params) );
        var dispositif_selected_name = getInputValue("dispositif_choisi_name");
        var dispositif_selected_id = getInputValue("dispositif_choisi_id");
  
        if (champ) {

            champ_html += '<div style="margin-left: -3%;" class="col-md-8">'
            champ_html +=    '<div id="display_value_champ_dispositif" data-id-selected="" class="col-md-9" style="border-radius: 3px; height: 47px; background: #c3e7fd none repeat scroll 0 0 !important; border: 1px solid #b8daef; font-size: 14px; font-weight: 400; color: #534d64; padding: 12px!important;">';
            champ_html +=        dispositif_selected_name + '</div>';
            champ_html +=    '<div class="col-md-3" style="text-align: center;">';
            champ_html +=        '<span class="id-ff multiple">';
            champ_html +=            '<button type="button" id="btn_statut_initial" disabled="disabled" title="Sélectionner" onclick="selectRelatedModule(\''+champ.name+'\',\''+params.module_name+'\',\''+params.type+'\',\''+params.unique+'\',\''+params.condition+'\')" class="button firstChild" value="Sélectionner">';
            champ_html +=                '<img src="themes/SuiteP/images/id-ff-select.png?v=9PUULME3DHwNvxEgZrpwig">';
            champ_html +=            '</button>';
            champ_html +=            '<button type="button" id="btn_clr_statut_initial" disabled="disabled" title="Clear Selection" onclick="deleteRelatedModule(\''+champ.name+'\',\''+params.unique+'\',\''+params.condition+'\')" class="button lastChild" value="Supprimer la selection">';
            champ_html +=                '<img src="themes/SuiteP/images/id-ff-clear.png?v=9PUULME3DHwNvxEgZrpwig">';
            champ_html +=            '</button>';
            champ_html +=        '</span>';
            champ_html +=    '</div>';
			champ_html +=    '<input name="Dispositif" required="true" type="hidden" id="value_champ_dispositif" value="' + dispositif_selected_id + '">';
            champ_html += '</div>';
            
        }

        return champ_html;
    },
    thematique: function(champ) {

        var champ_html = '';
        var liste = isReponseValid( b64_to_utf8( getInputValue("liste_thematique") ) );

        if (liste) {
            champ_html += '<select name="Thèmatique" required="true" class="col-md-8" id="value_champ_thematique">';
            champ_html +=    '<option value=""></option>';
            Object.keys(liste).map(function(option_id) {
                var option_name = liste[option_id]; 
                champ_html +=    '<option value="' + option_id + '">' + option_name + '</option>';
            });
            champ_html += '</select>';
        }       
        
        return champ_html;
       
    },
  
    astre_tiers_appairage: function(champ) {
        return '<div style="padding-top: 15px;" class="col-md-8">Ce champ est indisponible à la création</div>';
    },

    astre_tiers_raison_sociale: function(champ) {
        return '<div style="padding-top: 15px;" class="col-md-8">Ce champ est indisponible à la création</div>';
    },

    astre_domiciliation: function(champ) {
        return '<div style="padding-top: 15px;" class="col-md-8">Ce champ est indisponible à la création</div>';
    },

    astre_domiciliation_appairage: function(champ) {
        return '<div style="padding-top: 15px;" class="col-md-8">Ce champ est indisponible à la création</div>';
    },

};

