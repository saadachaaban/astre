
function utf8_to_b64( str ) {
    return window.btoa(encodeURIComponent( str ));
}
  
function b64_to_utf8( str ) {
    return decodeURIComponent(window.atob( str ));
}

$(document).ready(function() {
    JournalDetail.decodeDescription();
});

var JournalDetail = (function($) {
  
    return {

        decodeDescription: function(){ 

            var description = $('#description').text();
            if ( typeof description === "string" && description !== ''){

                var description_array = [];
                var xmls = [];
                var last_pos = 0;

                var position_debut_xml = JournalDetail.indexesOf(description, /\#\[/g);
                position_debut_xml = position_debut_xml['#['];

                var position_fin_xml = JournalDetail.indexesOf(description, /\]\#/g);
                position_fin_xml = position_fin_xml[']#'];

        
                position_debut_xml.forEach(function(pos_debut,index){

                    var pos_fin = position_fin_xml[index];
            
                    description_array.push( description.substring( last_pos, pos_debut ).replace(/\n/g, '</br>') );

                    var xml = description.substring( pos_debut +2, pos_fin  );
                    if ( typeof xml === "string" && xml !== '' ){
                        var id = JournalDetail.getUniqueId();
                        description_array.push( `<pre id="${id}"></pre>` );
                        xmls.push({
                            id:id,
                            xml:xml
                        });
                    }
                    
                    last_pos = pos_fin + 2;
                });

                if ( last_pos !== description.length ){
                    description_array.push( description.substring( last_pos, description.length ) );
                }
                
                $('#description').html(description_array.join(''))

                xmls.forEach(function(element){  
                    $(`#${element.id}`).text(b64_to_utf8(element.xml));
                });
            }
    
        },

        getUniqueId: function(){ 
            return Math.floor(Date.now() * Math.random() * 100);
        },

        indexesOf: function(string, regex){ 
            var match,
            indexes = {};
    
            regex = new RegExp(regex);
        
            while (match = regex.exec(string)) {
                if (!indexes[match[0]]) indexes[match[0]] = [];
                indexes[match[0]].push(match.index);
            }
        
            return indexes;
        },
    }

})(jQuery);



 


